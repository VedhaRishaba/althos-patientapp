"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router) {
        this.router = router;
        this.profile_data = JSON.parse(appSettings.getString("profile_data"));
        this.name = this.profile_data["patient_name"];
        this.op_code = this.profile_data["op_code"];
        this.isItemVisible = true;
        this.phone = this.profile_data["phone"];
        this.state = this.profile_data["state"];
        this.address = this.profile_data["address"];
        this.city_name = this.profile_data["city_name"];
        this.area_name = this.profile_data["area_name"];
        this.age = 30;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        console.log('sexx==>' + this.profile_data["sex"]);
        if (this.profile_data["sex"] == "F") {
            this.sex = "Female";
        }
        else {
            this.sex = "Male";
        }
        // this.dob = new Date(this.profile_data["dob"]);
        // this.today = new Date();
        // this.age = this.today - this.dob;
        this.address = this.address + ", " + this.area_name + ", " + this.city_name + ", " + this.profile_data["state"] + " - " + this.profile_data["pincode"];
        if (this.profile_data["mail_id"] != null) {
            this.email = this.profile_data["mail_id"];
        }
        else {
            this.isItemVisible = true;
            this.email = this.profile_data["mail_id"];
        }
    };
    ProfileComponent.prototype.logout = function () {
        var _this = this;
        dialogs
            .confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO"
        })
            .then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    ProfileComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1)
            .map(function (key) {
            return (encodeURIComponent(key) +
                "=" +
                encodeURIComponent(object2[key]));
        })
            .join("&");
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                Authorization: "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                charset: "utf-8"
            },
            body: searchParams
        })
            .then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl("/op-code");
            }
            else {
                console.log("failed to logout");
            }
        })
            .catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: "ns-profile",
            templateUrl: "./profile.component.html",
            styleUrls: ["./profile.component.css"],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm9maWxlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxxREFBdUQ7QUFDdkQsMENBQXlDO0FBQ3pDLG1FQUFxRTtBQVFyRTtJQWVJLDBCQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQWRsQyxpQkFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLFNBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3pDLFlBQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBSXZDLGtCQUFhLEdBQUMsSUFBSSxDQUFDO1FBQ25CLFVBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25DLFVBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25DLFlBQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZDLGNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzNDLGNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzNDLFFBQUcsR0FBRyxFQUFFLENBQUM7SUFFNEIsQ0FBQztJQUV0QyxtQ0FBUSxHQUFSO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ2hELElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEVBQUU7WUFDakMsSUFBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUM7U0FDdkI7YUFBTTtZQUNILElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDO1NBQ3JCO1FBQ0QsaURBQWlEO1FBQ2pELDJCQUEyQjtRQUMzQixvQ0FBb0M7UUFFbEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFDLElBQUksR0FBQyxJQUFJLENBQUMsU0FBUyxHQUFDLElBQUksR0FBQyxJQUFJLENBQUMsU0FBUyxHQUFDLElBQUksR0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFDLEtBQUssR0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXZJLElBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLEVBQUM7WUFDdEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzNDO2FBQUk7WUFDSCxJQUFJLENBQUMsYUFBYSxHQUFDLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDM0M7SUFDUCxDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUFBLGlCQVlDO1FBWEcsT0FBTzthQUNGLE9BQU8sQ0FBQztZQUNMLE9BQU8sRUFBRSwwQkFBMEI7WUFDbkMsWUFBWSxFQUFFLEtBQUs7WUFDbkIsZ0JBQWdCLEVBQUUsSUFBSTtTQUN6QixDQUFDO2FBQ0QsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNSLElBQUksTUFBTSxFQUFFO2dCQUNSLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN6QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUNELHlDQUFjLEdBQWQ7UUFBQSxpQkF3Q0M7UUF2Q0csSUFBTSxPQUFPLEdBQUc7WUFDWixPQUFPLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFDRixJQUFNLE9BQU8sR0FBRztZQUNaLE9BQU8sRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQztTQUM1QyxDQUFDO1FBQ0YsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDcEMsR0FBRyxDQUFDLFVBQUEsR0FBRztZQUNKLE9BQU8sQ0FDSCxrQkFBa0IsQ0FBQyxHQUFHLENBQUM7Z0JBQ3ZCLEdBQUc7Z0JBQ0gsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQ25DLENBQUM7UUFDTixDQUFDLENBQUM7YUFDRCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFZixLQUFLLENBQUMsZ0RBQWdELEVBQUU7WUFDcEQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ0wsY0FBYyxFQUFFLG1DQUFtQztnQkFDbkQsYUFBYSxFQUNULHFFQUFxRTtnQkFDekUsT0FBTyxFQUFFLE9BQU87YUFDbkI7WUFDRCxJQUFJLEVBQUUsWUFBWTtTQUNyQixDQUFDO2FBQ0csSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFSLENBQVEsQ0FBQzthQUNuQixJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDOUIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDbEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDekM7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQ25DO1FBQ0wsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUEsQ0FBQztZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUEzRlEsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUsMEJBQTBCO1lBQ3ZDLFNBQVMsRUFBRSxDQUFDLHlCQUF5QixDQUFDO1lBQ3RDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUN0QixDQUFDO3lDQWdCOEIsZUFBTTtPQWZ6QixnQkFBZ0IsQ0E0RjVCO0lBQUQsdUJBQUM7Q0FBQSxBQTVGRCxJQTRGQztBQTVGWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgKiBhcyBkaWFsb2dzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCAqIGFzIGFwcFNldHRpbmdzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIm5zLXByb2ZpbGVcIixcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3Byb2ZpbGUuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFtcIi4vcHJvZmlsZS5jb21wb25lbnQuY3NzXCJdLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWRcbn0pXG5leHBvcnQgY2xhc3MgUHJvZmlsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgcHJvZmlsZV9kYXRhID0gSlNPTi5wYXJzZShhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJwcm9maWxlX2RhdGFcIikpO1xuICAgIG5hbWUgPSB0aGlzLnByb2ZpbGVfZGF0YVtcInBhdGllbnRfbmFtZVwiXTtcbiAgICBvcF9jb2RlID0gdGhpcy5wcm9maWxlX2RhdGFbXCJvcF9jb2RlXCJdO1xuICAgIGVtYWlsO1xuICAgIHNleDtcbiAgICBkb2I7XG4gICAgaXNJdGVtVmlzaWJsZT10cnVlO1xuICAgIHBob25lID0gdGhpcy5wcm9maWxlX2RhdGFbXCJwaG9uZVwiXTtcbiAgICBzdGF0ZSA9IHRoaXMucHJvZmlsZV9kYXRhW1wic3RhdGVcIl07XG4gICAgYWRkcmVzcyA9IHRoaXMucHJvZmlsZV9kYXRhW1wiYWRkcmVzc1wiXTtcbiAgICBjaXR5X25hbWUgPSB0aGlzLnByb2ZpbGVfZGF0YVtcImNpdHlfbmFtZVwiXTtcbiAgICBhcmVhX25hbWUgPSB0aGlzLnByb2ZpbGVfZGF0YVtcImFyZWFfbmFtZVwiXTtcbiAgICBhZ2UgPSAzMDtcbiAgICB0b2RheTtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7fVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdzZXh4PT0+Jyt0aGlzLnByb2ZpbGVfZGF0YVtcInNleFwiXSk7XG4gICAgICAgIGlmICh0aGlzLnByb2ZpbGVfZGF0YVtcInNleFwiXSA9PSBcIkZcIikge1xuICAgICAgICAgICAgdGhpcy5zZXggPSBcIkZlbWFsZVwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zZXggPSBcIk1hbGVcIjtcbiAgICAgICAgfVxuICAgICAgICAvLyB0aGlzLmRvYiA9IG5ldyBEYXRlKHRoaXMucHJvZmlsZV9kYXRhW1wiZG9iXCJdKTtcbiAgICAgICAgLy8gdGhpcy50b2RheSA9IG5ldyBEYXRlKCk7XG4gICAgICAgIC8vIHRoaXMuYWdlID0gdGhpcy50b2RheSAtIHRoaXMuZG9iO1xuXG4gICAgICAgICAgdGhpcy5hZGRyZXNzID0gdGhpcy5hZGRyZXNzK1wiLCBcIit0aGlzLmFyZWFfbmFtZStcIiwgXCIrdGhpcy5jaXR5X25hbWUrXCIsIFwiK3RoaXMucHJvZmlsZV9kYXRhW1wic3RhdGVcIl0rXCIgLSBcIit0aGlzLnByb2ZpbGVfZGF0YVtcInBpbmNvZGVcIl07XG5cbiAgICAgICAgICBpZih0aGlzLnByb2ZpbGVfZGF0YVtcIm1haWxfaWRcIl0gIT0gbnVsbCl7XG4gICAgICAgICAgICB0aGlzLmVtYWlsID0gdGhpcy5wcm9maWxlX2RhdGFbXCJtYWlsX2lkXCJdO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgdGhpcy5pc0l0ZW1WaXNpYmxlPXRydWU7XG4gICAgICAgICAgICB0aGlzLmVtYWlsID0gdGhpcy5wcm9maWxlX2RhdGFbXCJtYWlsX2lkXCJdO1xuICAgICAgICAgIH1cbiAgICB9XG5cbiAgICBsb2dvdXQoKSB7XG4gICAgICAgIGRpYWxvZ3NcbiAgICAgICAgICAgIC5jb25maXJtKHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIkRvIHlvdSB3YW50IHRvIExvZ291dCA/IFwiLFxuICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJZRVNcIixcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCJcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dvdXRhY3Rpdml0eSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH1cbiAgICBsb2dvdXRhY3Rpdml0eSgpIHtcbiAgICAgICAgY29uc3Qgb2JqZWN0MSA9IHtcbiAgICAgICAgICAgIG9wX2NvZGU6IFwiXCJcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgb2JqZWN0MiA9IHtcbiAgICAgICAgICAgIG9wX2NvZGU6IGFwcFNldHRpbmdzLmdldFN0cmluZyhcIm9wLWNvZGVcIilcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSlcbiAgICAgICAgICAgIC5tYXAoa2V5ID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICBlbmNvZGVVUklDb21wb25lbnQoa2V5KSArXG4gICAgICAgICAgICAgICAgICAgIFwiPVwiICtcbiAgICAgICAgICAgICAgICAgICAgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSlcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5qb2luKFwiJlwiKTtcblxuICAgICAgICBmZXRjaChcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9sb2dvdXRcIiwge1xuICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiLFxuICAgICAgICAgICAgICAgIEF1dGhvcml6YXRpb246XG4gICAgICAgICAgICAgICAgICAgIFwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxuICAgICAgICAgICAgICAgIGNoYXJzZXQ6IFwidXRmLThcIlxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJvZHk6IHNlYXJjaFBhcmFtc1xuICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4ociA9PiByLnRleHQoKSlcbiAgICAgICAgICAgIC50aGVuKHRleHQgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwibWVzc2FnZVwiICsgdGV4dCk7XG4gICAgICAgICAgICAgICAgaWYgKHRleHQubGVuZ3RoID09IDQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChcIi9vcC1jb2RlXCIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZmFpbGVkIHRvIGxvZ291dFwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKGUgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3I6IFwiKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==