import { Component, OnInit } from "@angular/core";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Router } from "@angular/router";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "ns-profile",
    templateUrl: "./profile.component.html",
    styleUrls: ["./profile.component.css"],
    moduleId: module.id
})
export class ProfileComponent implements OnInit {
    profile_data = JSON.parse(appSettings.getString("profile_data"));
    name = this.profile_data["patient_name"];
    op_code = this.profile_data["op_code"];
    email;
    sex;
    dob;
    isItemVisible=true;
    phone = this.profile_data["phone"];
    state = this.profile_data["state"];
    address = this.profile_data["address"];
    city_name = this.profile_data["city_name"];
    area_name = this.profile_data["area_name"];
    age = 30;
    today;
    constructor(private router: Router) {}

    ngOnInit() {
        console.log('sexx==>'+this.profile_data["sex"]);
        if (this.profile_data["sex"] == "F") {
            this.sex = "Female";
        } else {
            this.sex = "Male";
        }
        // this.dob = new Date(this.profile_data["dob"]);
        // this.today = new Date();
        // this.age = this.today - this.dob;

          this.address = this.address+", "+this.area_name+", "+this.city_name+", "+this.profile_data["state"]+" - "+this.profile_data["pincode"];

          if(this.profile_data["mail_id"] != null){
            this.email = this.profile_data["mail_id"];
          }else{
            this.isItemVisible=true;
            this.email = this.profile_data["mail_id"];
          }
    }

    logout() {
        dialogs
            .confirm({
                message: "Do you want to Logout ? ",
                okButtonText: "YES",
                cancelButtonText: "NO"
            })
            .then(result => {
                if (result) {
                    this.logoutactivity();
                }
            });
    }
    logoutactivity() {
        const object1 = {
            op_code: ""
        };
        const object2 = {
            op_code: appSettings.getString("op-code")
        };
        const searchParams = Object.keys(object1)
            .map(key => {
                return (
                    encodeURIComponent(key) +
                    "=" +
                    encodeURIComponent(object2[key])
                );
            })
            .join("&");

        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                Authorization:
                    "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                charset: "utf-8"
            },
            body: searchParams
        })
            .then(r => r.text())
            .then(text => {
                console.log("message" + text);
                if (text.length == 4) {
                    this.router.navigateByUrl("/op-code");
                } else {
                    console.log("failed to logout");
                }
            })
            .catch(e => {
                console.log("Error: ");
                console.log(e);
            });
    }
}
