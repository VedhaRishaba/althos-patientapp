import { Injectable, Component, OnInit, Output } from '@angular/core';
import { Page } from "tns-core-modules/ui/page/page";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { DeviceOrientation } from "tns-core-modules/ui/enums";
import { displayedEvent } from 'tns-core-modules/application/application';
import { catchError, map } from "rxjs/operators";
import { Http, Headers, Response } from "@angular/http";
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from "@angular/common/http";
import { disableDebugTools } from "@angular/platform-browser";
import { Router } from '@angular/router';

import * as appSettings from "tns-core-modules/application-settings";
import { password_service } from '~/app/password/passwordservice';
import * as Toast from 'nativescript-toast';

import * as Connectivity from "tns-core-modules/connectivity";

@Component({
  selector: 'ns-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css'],
  providers: [password_service],
  moduleId: module.id,
})
export class PasswordComponent implements OnInit {
  validpassword = true;
  // password:string="";

  public connectionType: string;
  op_code = appSettings.getString("op-code");
  password: "nxC0le9gP2L2mqEUPIIGjw==";
  message: string = "";
  sid: string = "";
  isItemVisible: boolean = false;

  constructor(private http: HttpClient, private page: Page, private router: Router, private myservice: password_service) {
    DeviceOrientation.landscape;
  }

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  getPassword() {
    if (this.password.localeCompare("") != 0) {

      this.getdetails();
    }
    else {
      this.validpassword = false;
    }
  }

  returnPress(arg) {
    if (this.validpassword == false) {
      this.validpassword = true;
    }
  }

  getdetails() {
    const object1 = {
      op_code: "",
      password: "",
    };
    const object2 = {
      op_code: this.op_code,
      password: "nxC0le9gP2L2mqEUPIIGjw==",
    }


    const searchParams = Object.keys(object1).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
    }).join('&');
    this.myservice
      .postData(searchParams)
      .subscribe((response) => {
        if (response.status == 200) {
          this.isItemVisible = true;
          this.message = JSON.stringify(response);
          this.sid = response.headers.get('Set-cookie');
          console.log(this.message);
          console.log("cookie " + this.sid);
          console.log("name" + response.body["patient_name"] );
          appSettings.setString(
            "cookie", this.sid
          );
           appSettings.setString(
            "name",response.body["patient_name"]
          );

          appSettings.setString(
            "profile_data", JSON.stringify(response.body)
          );

          // this.router.navigate(['/home'], { queryParams: { Sessionid:this.sid } });
          this.router.navigate(['/home']);
        } else {
          var toast = Toast.makeText("Invalid password");
          toast.show();
        }
      }, (error) => {
        console.log(error);
        if (error.status == 400) {

          // this.router.navigateByUrl('/home');
          var toast = Toast.makeText("This account is already logged in another device. Please log out and try again later");
          toast.show();

        } else {



          this.connectionType = this.connectionToString(
            Connectivity.getConnectionType()
        );
        if (this.connectionType == "0") {
            var toast = Toast.makeText(
                "Cannot connect to network. Try again later!"
            );
            toast.show();
        } else  {

          var toast = Toast.makeText("Try Again");
          toast.show();
        }



        }
      });


  }


  public connectionToString(connectionType: number): string {
    switch (connectionType) {
      case Connectivity.connectionType.none:
        return "0";
      case Connectivity.connectionType.wifi:
        return "1";
      case Connectivity.connectionType.mobile:
        return "1";
      default:
        return "0";
    }
  }
}

