export interface passwordmodel {
    online_reg_no: string;
    op_code: string;
    patient_name: string;
    patient_pwd: string;
    sex: string;
    dob: number;
    marital_status: string;
    occupation: string;
    address: string;
    area_name: string;
    city_name: string;
    state: string;
    pincode: string;
    phone: number;
    mobile_no: number;
    mail_id: string;
    dependent_type: string;
    dependent_name: string;
    dependent_occupation: string;
    div_code: string;
}

