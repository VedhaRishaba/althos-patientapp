"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var enums_1 = require("tns-core-modules/ui/enums");
var http_1 = require("@angular/common/http");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var passwordservice_1 = require("~/app/password/passwordservice");
var Toast = require("nativescript-toast");
var Connectivity = require("tns-core-modules/connectivity");
var PasswordComponent = /** @class */ (function () {
    function PasswordComponent(http, page, router, myservice) {
        this.http = http;
        this.page = page;
        this.router = router;
        this.myservice = myservice;
        this.validpassword = true;
        this.op_code = appSettings.getString("op-code");
        this.message = "";
        this.sid = "";
        this.isItemVisible = false;
        enums_1.DeviceOrientation.landscape;
    }
    PasswordComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    PasswordComponent.prototype.getPassword = function () {
        if (this.password.localeCompare("") != 0) {
            this.getdetails();
        }
        else {
            this.validpassword = false;
        }
    };
    PasswordComponent.prototype.returnPress = function (arg) {
        if (this.validpassword == false) {
            this.validpassword = true;
        }
    };
    PasswordComponent.prototype.getdetails = function () {
        var _this = this;
        var object1 = {
            op_code: "",
            password: "",
        };
        var object2 = {
            op_code: this.op_code,
            password: "nxC0le9gP2L2mqEUPIIGjw==",
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        this.myservice
            .postData(searchParams)
            .subscribe(function (response) {
            if (response.status == 200) {
                _this.isItemVisible = true;
                _this.message = JSON.stringify(response);
                _this.sid = response.headers.get('Set-cookie');
                console.log(_this.message);
                console.log("cookie " + _this.sid);
                console.log("name" + response.body["patient_name"]);
                appSettings.setString("cookie", _this.sid);
                appSettings.setString("name", response.body["patient_name"]);
                appSettings.setString("profile_data", JSON.stringify(response.body));
                // this.router.navigate(['/home'], { queryParams: { Sessionid:this.sid } });
                _this.router.navigate(['/home']);
            }
            else {
                var toast = Toast.makeText("Invalid password");
                toast.show();
            }
        }, function (error) {
            console.log(error);
            if (error.status == 400) {
                // this.router.navigateByUrl('/home');
                var toast = Toast.makeText("This account is already logged in another device. Please log out and try again later");
                toast.show();
            }
            else {
                _this.connectionType = _this.connectionToString(Connectivity.getConnectionType());
                if (_this.connectionType == "0") {
                    var toast = Toast.makeText("Cannot connect to network. Try again later!");
                    toast.show();
                }
                else {
                    var toast = Toast.makeText("Try Again");
                    toast.show();
                }
            }
        });
    };
    PasswordComponent.prototype.connectionToString = function (connectionType) {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "0";
            case Connectivity.connectionType.wifi:
                return "1";
            case Connectivity.connectionType.mobile:
                return "1";
            default:
                return "0";
        }
    };
    PasswordComponent = __decorate([
        core_1.Component({
            selector: 'ns-password',
            templateUrl: './password.component.html',
            styleUrls: ['./password.component.css'],
            providers: [passwordservice_1.password_service],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, page_1.Page, router_1.Router, passwordservice_1.password_service])
    ], PasswordComponent);
    return PasswordComponent;
}());
exports.PasswordComponent = PasswordComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFzc3dvcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGFzc3dvcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXNFO0FBQ3RFLHNEQUFxRDtBQUVyRCxtREFBOEQ7QUFJOUQsNkNBQStGO0FBRS9GLDBDQUF5QztBQUV6QyxtRUFBcUU7QUFDckUsa0VBQWtFO0FBQ2xFLDBDQUE0QztBQUU1Qyw0REFBOEQ7QUFTOUQ7SUFXRSwyQkFBb0IsSUFBZ0IsRUFBVSxJQUFVLEVBQVUsTUFBYyxFQUFVLFNBQTJCO1FBQWpHLFNBQUksR0FBSixJQUFJLENBQVk7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBVnJILGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBSXJCLFlBQU8sR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRTNDLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFDckIsUUFBRyxHQUFXLEVBQUUsQ0FBQztRQUNqQixrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUc3Qix5QkFBaUIsQ0FBQyxTQUFTLENBQUM7SUFDOUIsQ0FBQztJQUVELG9DQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQUVELHVDQUFXLEdBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUV4QyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDbkI7YUFDSTtZQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1NBQzVCO0lBQ0gsQ0FBQztJQUVELHVDQUFXLEdBQVgsVUFBWSxHQUFHO1FBQ2IsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLEtBQUssRUFBRTtZQUMvQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUMzQjtJQUNILENBQUM7SUFFRCxzQ0FBVSxHQUFWO1FBQUEsaUJBeUVDO1FBeEVDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7WUFDWCxRQUFRLEVBQUUsRUFBRTtTQUNiLENBQUM7UUFDRixJQUFNLE9BQU8sR0FBRztZQUNkLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztZQUNyQixRQUFRLEVBQUUsMEJBQTBCO1NBQ3JDLENBQUE7UUFHRCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7WUFDaEQsT0FBTyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2IsSUFBSSxDQUFDLFNBQVM7YUFDWCxRQUFRLENBQUMsWUFBWSxDQUFDO2FBQ3RCLFNBQVMsQ0FBQyxVQUFDLFFBQVE7WUFDbEIsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQkFDMUIsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEMsS0FBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBRSxDQUFDO2dCQUNyRCxXQUFXLENBQUMsU0FBUyxDQUNuQixRQUFRLEVBQUUsS0FBSSxDQUFDLEdBQUcsQ0FDbkIsQ0FBQztnQkFDRCxXQUFXLENBQUMsU0FBUyxDQUNwQixNQUFNLEVBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FDckMsQ0FBQztnQkFFRixXQUFXLENBQUMsU0FBUyxDQUNuQixjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQzlDLENBQUM7Z0JBRUYsNEVBQTRFO2dCQUM1RSxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDakM7aUJBQU07Z0JBQ0wsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUMvQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDZDtRQUNILENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7Z0JBRXZCLHNDQUFzQztnQkFDdEMsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxzRkFBc0YsQ0FBQyxDQUFDO2dCQUNuSCxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7YUFFZDtpQkFBTTtnQkFJTCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FDM0MsWUFBWSxDQUFDLGlCQUFpQixFQUFFLENBQ25DLENBQUM7Z0JBQ0YsSUFBSSxLQUFJLENBQUMsY0FBYyxJQUFJLEdBQUcsRUFBRTtvQkFDNUIsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FDdEIsNkNBQTZDLENBQ2hELENBQUM7b0JBQ0YsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO2lCQUNoQjtxQkFBTztvQkFFTixJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN4QyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7aUJBQ2Q7YUFJQTtRQUNILENBQUMsQ0FBQyxDQUFDO0lBR1AsQ0FBQztJQUdNLDhDQUFrQixHQUF6QixVQUEwQixjQUFzQjtRQUM5QyxRQUFRLGNBQWMsRUFBRTtZQUN0QixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDbkMsT0FBTyxHQUFHLENBQUM7WUFDYixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDbkMsT0FBTyxHQUFHLENBQUM7WUFDYixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsTUFBTTtnQkFDckMsT0FBTyxHQUFHLENBQUM7WUFDYjtnQkFDRSxPQUFPLEdBQUcsQ0FBQztTQUNkO0lBQ0gsQ0FBQztJQTFIVSxpQkFBaUI7UUFQN0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFdBQVcsRUFBRSwyQkFBMkI7WUFDeEMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLENBQUM7WUFDdkMsU0FBUyxFQUFFLENBQUMsa0NBQWdCLENBQUM7WUFDN0IsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBWTBCLGlCQUFVLEVBQWdCLFdBQUksRUFBa0IsZUFBTSxFQUFxQixrQ0FBZ0I7T0FYMUcsaUJBQWlCLENBMkg3QjtJQUFELHdCQUFDO0NBQUEsQUEzSEQsSUEySEM7QUEzSFksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgQ29tcG9uZW50LCBPbkluaXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG5pbXBvcnQgeyBEZXZpY2VPcmllbnRhdGlvbiB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2VudW1zXCI7XG5pbXBvcnQgeyBkaXNwbGF5ZWRFdmVudCB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24vYXBwbGljYXRpb24nO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFJlc3BvbnNlLCBIdHRwUmVzcG9uc2VCYXNlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQgeyBkaXNhYmxlRGVidWdUb29scyB9IGZyb20gXCJAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyXCI7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xuaW1wb3J0IHsgcGFzc3dvcmRfc2VydmljZSB9IGZyb20gJ34vYXBwL3Bhc3N3b3JkL3Bhc3N3b3Jkc2VydmljZSc7XG5pbXBvcnQgKiBhcyBUb2FzdCBmcm9tICduYXRpdmVzY3JpcHQtdG9hc3QnO1xuXG5pbXBvcnQgKiBhcyBDb25uZWN0aXZpdHkgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvY29ubmVjdGl2aXR5XCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLXBhc3N3b3JkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3Bhc3N3b3JkLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcGFzc3dvcmQuY29tcG9uZW50LmNzcyddLFxuICBwcm92aWRlcnM6IFtwYXNzd29yZF9zZXJ2aWNlXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5leHBvcnQgY2xhc3MgUGFzc3dvcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICB2YWxpZHBhc3N3b3JkID0gdHJ1ZTtcbiAgLy8gcGFzc3dvcmQ6c3RyaW5nPVwiXCI7XG5cbiAgcHVibGljIGNvbm5lY3Rpb25UeXBlOiBzdHJpbmc7XG4gIG9wX2NvZGUgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJvcC1jb2RlXCIpO1xuICBwYXNzd29yZDogXCJueEMwbGU5Z1AyTDJtcUVVUElJR2p3PT1cIjtcbiAgbWVzc2FnZTogc3RyaW5nID0gXCJcIjtcbiAgc2lkOiBzdHJpbmcgPSBcIlwiO1xuICBpc0l0ZW1WaXNpYmxlOiBib29sZWFuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgbXlzZXJ2aWNlOiBwYXNzd29yZF9zZXJ2aWNlKSB7XG4gICAgRGV2aWNlT3JpZW50YXRpb24ubGFuZHNjYXBlO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gIH1cblxuICBnZXRQYXNzd29yZCgpIHtcbiAgICBpZiAodGhpcy5wYXNzd29yZC5sb2NhbGVDb21wYXJlKFwiXCIpICE9IDApIHtcblxuICAgICAgdGhpcy5nZXRkZXRhaWxzKCk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgdGhpcy52YWxpZHBhc3N3b3JkID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuUHJlc3MoYXJnKSB7XG4gICAgaWYgKHRoaXMudmFsaWRwYXNzd29yZCA9PSBmYWxzZSkge1xuICAgICAgdGhpcy52YWxpZHBhc3N3b3JkID0gdHJ1ZTtcbiAgICB9XG4gIH1cblxuICBnZXRkZXRhaWxzKCkge1xuICAgIGNvbnN0IG9iamVjdDEgPSB7XG4gICAgICBvcF9jb2RlOiBcIlwiLFxuICAgICAgcGFzc3dvcmQ6IFwiXCIsXG4gICAgfTtcbiAgICBjb25zdCBvYmplY3QyID0ge1xuICAgICAgb3BfY29kZTogdGhpcy5vcF9jb2RlLFxuICAgICAgcGFzc3dvcmQ6IFwibnhDMGxlOWdQMkwybXFFVVBJSUdqdz09XCIsXG4gICAgfVxuXG5cbiAgICBjb25zdCBzZWFyY2hQYXJhbXMgPSBPYmplY3Qua2V5cyhvYmplY3QxKS5tYXAoKGtleSkgPT4ge1xuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XG4gICAgfSkuam9pbignJicpO1xuICAgIHRoaXMubXlzZXJ2aWNlXG4gICAgICAucG9zdERhdGEoc2VhcmNoUGFyYW1zKVxuICAgICAgLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PSAyMDApIHtcbiAgICAgICAgICB0aGlzLmlzSXRlbVZpc2libGUgPSB0cnVlO1xuICAgICAgICAgIHRoaXMubWVzc2FnZSA9IEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKTtcbiAgICAgICAgICB0aGlzLnNpZCA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdTZXQtY29va2llJyk7XG4gICAgICAgICAgY29uc29sZS5sb2codGhpcy5tZXNzYWdlKTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcImNvb2tpZSBcIiArIHRoaXMuc2lkKTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIm5hbWVcIiArIHJlc3BvbnNlLmJvZHlbXCJwYXRpZW50X25hbWVcIl0gKTtcbiAgICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXG4gICAgICAgICAgICBcImNvb2tpZVwiLCB0aGlzLnNpZFxuICAgICAgICAgICk7XG4gICAgICAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcbiAgICAgICAgICAgIFwibmFtZVwiLHJlc3BvbnNlLmJvZHlbXCJwYXRpZW50X25hbWVcIl1cbiAgICAgICAgICApO1xuXG4gICAgICAgICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFxuICAgICAgICAgICAgXCJwcm9maWxlX2RhdGFcIiwgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UuYm9keSlcbiAgICAgICAgICApO1xuXG4gICAgICAgICAgLy8gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvaG9tZSddLCB7IHF1ZXJ5UGFyYW1zOiB7IFNlc3Npb25pZDp0aGlzLnNpZCB9IH0pO1xuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2hvbWUnXSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJJbnZhbGlkIHBhc3N3b3JkXCIpO1xuICAgICAgICAgIHRvYXN0LnNob3coKTtcbiAgICAgICAgfVxuICAgICAgfSwgKGVycm9yKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgaWYgKGVycm9yLnN0YXR1cyA9PSA0MDApIHtcblxuICAgICAgICAgIC8vIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9ob21lJyk7XG4gICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJUaGlzIGFjY291bnQgaXMgYWxyZWFkeSBsb2dnZWQgaW4gYW5vdGhlciBkZXZpY2UuIFBsZWFzZSBsb2cgb3V0IGFuZCB0cnkgYWdhaW4gbGF0ZXJcIik7XG4gICAgICAgICAgdG9hc3Quc2hvdygpO1xuXG4gICAgICAgIH0gZWxzZSB7XG5cblxuXG4gICAgICAgICAgdGhpcy5jb25uZWN0aW9uVHlwZSA9IHRoaXMuY29ubmVjdGlvblRvU3RyaW5nKFxuICAgICAgICAgICAgQ29ubmVjdGl2aXR5LmdldENvbm5lY3Rpb25UeXBlKClcbiAgICAgICAgKTtcbiAgICAgICAgaWYgKHRoaXMuY29ubmVjdGlvblR5cGUgPT0gXCIwXCIpIHtcbiAgICAgICAgICAgIHZhciB0b2FzdCA9IFRvYXN0Lm1ha2VUZXh0KFxuICAgICAgICAgICAgICAgIFwiQ2Fubm90IGNvbm5lY3QgdG8gbmV0d29yay4gVHJ5IGFnYWluIGxhdGVyIVwiXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgdG9hc3Quc2hvdygpO1xuICAgICAgICB9IGVsc2UgIHtcblxuICAgICAgICAgIHZhciB0b2FzdCA9IFRvYXN0Lm1ha2VUZXh0KFwiVHJ5IEFnYWluXCIpO1xuICAgICAgICAgIHRvYXN0LnNob3coKTtcbiAgICAgICAgfVxuXG5cblxuICAgICAgICB9XG4gICAgICB9KTtcblxuXG4gIH1cblxuXG4gIHB1YmxpYyBjb25uZWN0aW9uVG9TdHJpbmcoY29ubmVjdGlvblR5cGU6IG51bWJlcik6IHN0cmluZyB7XG4gICAgc3dpdGNoIChjb25uZWN0aW9uVHlwZSkge1xuICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUubm9uZTpcbiAgICAgICAgcmV0dXJuIFwiMFwiO1xuICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUud2lmaTpcbiAgICAgICAgcmV0dXJuIFwiMVwiO1xuICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUubW9iaWxlOlxuICAgICAgICByZXR1cm4gXCIxXCI7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gXCIwXCI7XG4gICAgfVxuICB9XG59XG5cbiJdfQ==