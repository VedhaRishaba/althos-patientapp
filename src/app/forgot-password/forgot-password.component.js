"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var fpasswordservice_1 = require("./fpasswordservice");
var router_1 = require("@angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var appSettings = require("tns-core-modules/application-settings");
var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(page, router, myService) {
        this.page = page;
        this.router = router;
        this.myService = myService;
        this.op_code = appSettings.getString("op-code");
        this.mobile = "";
        this.reg = /^\d+$/;
        this.opreg = /^[A-Z]{1}[0-9]{8}/;
        this.validop = true;
        this.opfield = true;
        this.validmobile = true;
        this.mobilefield = true;
    }
    ;
    ForgotPasswordComponent.prototype.ngOnInit = function () {
    };
    ForgotPasswordComponent.prototype.validation = function () {
        this.checkmobnull = this.mobile.match(this.reg);
        this.checkopnull = this.op_code.length == 9;
        console.log("OPcode : " + this.op_code + "Mobile :" + this.mobile);
        if (this.op_code != "" && this.mobile != "") {
            if (this.mobile.length == 10 && this.mobile.match(this.reg) && this.op_code.length == 9 && this.op_code.match(this.opreg)) {
                dialogs.alert("Valid Mobile Number " + this.mobile);
                this.extractData();
            }
            else if (!this.checkopnull || this.op_code.length != 9) {
                this.validop = false;
            }
            else if (!this.checkmobnull || this.mobile.length != 10) {
                this.validmobile = false;
            }
            else {
                this.validop = false;
                this.validmobile = false;
            }
        }
        else if (this.op_code == "" && this.mobile == "") {
            this.opfield = false;
            this.mobilefield = false;
            console.log("both field empty");
        }
        else if (this.op_code == "") {
            this.opfield = false;
            console.log("opcode field is empty");
        }
        else {
            this.mobilefield = false;
            console.log("mobile field is empty");
        }
    };
    ForgotPasswordComponent.prototype.returnOP = function (args) {
        if (this.opfield == false)
            this.opfield = true;
        if (this.validop == false)
            this.validop = true;
    };
    ForgotPasswordComponent.prototype.returnMob = function (args) {
        if (this.mobilefield == false)
            this.mobilefield = true;
        if (this.validmobile == false)
            this.validmobile = true;
    };
    ForgotPasswordComponent.prototype.changeUpper = function () {
        this.op_code.toUpperCase;
    };
    ForgotPasswordComponent.prototype.extractData = function () {
        var _this = this;
        // console.log(this.op_code);
        this.myService.getData(this.op_code, this.mobile)
            .subscribe(function (result) {
            console.log(result);
            if (result == true) {
                _this.onGetDataSuccess(result);
            }
            else {
                dialogs.alert("enter valid credentials");
            }
        }, function (error) {
            //this.router.navigateByUrl('/otp');
            dialogs.alert("Try again later" + _this.op_code);
            console.log(error);
        });
    };
    ForgotPasswordComponent.prototype.onGetDataSuccess = function (res) {
        console.log("inside succ");
        //this.var = res.op_code;
        this.op_code = res.op_code;
        dialogs.alert("otp sent to the mobile number");
        this.router.navigateByUrl('/otp');
    };
    ForgotPasswordComponent = __decorate([
        core_1.Component({
            selector: 'ns-forgot-password',
            templateUrl: './forgot-password.component.html',
            providers: [fpasswordservice_1.Fpasswordservice],
            styleUrls: ['./forgot-password.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [page_1.Page, router_1.Router, fpasswordservice_1.Fpasswordservice])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());
exports.ForgotPasswordComponent = ForgotPasswordComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9yZ290LXBhc3N3b3JkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQscURBQXVEO0FBQ3ZELHVEQUFzRDtBQUV0RCwwQ0FBeUM7QUFDekMsc0RBQXFEO0FBSXJELG1FQUFxRTtBQVNyRTtJQWFFLGlDQUFvQixJQUFVLEVBQVUsTUFBYyxFQUFVLFNBQTJCO1FBQXZFLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFaM0YsWUFBTyxHQUFhLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFckQsV0FBTSxHQUFXLEVBQUUsQ0FBQztRQUNwQixRQUFHLEdBQVEsT0FBTyxDQUFDO1FBQ25CLFVBQUssR0FBUSxtQkFBbUIsQ0FBQztRQUNqQyxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLGdCQUFXLEdBQUcsSUFBSSxDQUFDO0lBSTRFLENBQUM7SUFYaEcsQ0FBQztJQVlELDBDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsNENBQVUsR0FBVjtRQUNFLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO1FBQzVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuRSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFFO1lBRTNDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUN6SCxPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BCO2lCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDdEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7aUJBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFFO2dCQUN2RCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQzthQUMxQjtpQkFDSTtnQkFDSCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7YUFDMUI7U0FDRjthQUNJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLEVBQUU7WUFDaEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQ2pDO2FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsRUFBRTtZQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7U0FDdEM7YUFDSTtZQUNILElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7SUFFRCwwQ0FBUSxHQUFSLFVBQVMsSUFBSTtRQUNYLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFDRCwyQ0FBUyxHQUFULFVBQVUsSUFBSTtRQUNaLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLO1lBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLO1lBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCw2Q0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7SUFDM0IsQ0FBQztJQUdELDZDQUFXLEdBQVg7UUFBQSxpQkFnQkM7UUFmQyw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDO2FBQzlDLFNBQVMsQ0FBQyxVQUFDLE1BQU07WUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwQixJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0JBQ2xCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUMvQjtpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7YUFDMUM7UUFDSCxDQUFDLEVBQUUsVUFBQyxLQUFLO1lBQ1Asb0NBQW9DO1lBQ3BDLE9BQU8sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFckIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8sa0RBQWdCLEdBQXhCLFVBQXlCLEdBQUc7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzQix5QkFBeUI7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBL0ZVLHVCQUF1QjtRQVBuQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixXQUFXLEVBQUUsa0NBQWtDO1lBQy9DLFNBQVMsRUFBRSxDQUFDLG1DQUFnQixDQUFDO1lBQzdCLFNBQVMsRUFBRSxDQUFDLGlDQUFpQyxDQUFDO1lBQzlDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQWMwQixXQUFJLEVBQWtCLGVBQU0sRUFBcUIsbUNBQWdCO09BYmhGLHVCQUF1QixDQWtHbkM7SUFBRCw4QkFBQztDQUFBLEFBbEdELElBa0dDO0FBbEdZLDBEQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgKiBhcyBkaWFsb2dzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcbmltcG9ydCB7IEZwYXNzd29yZHNlcnZpY2UgfSBmcm9tICcuL2ZwYXNzd29yZHNlcnZpY2UnO1xuaW1wb3J0IHsgRXZlbnREYXRhIH0gZnJvbSAndG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcbmltcG9ydCB7IEJ1dHRvbiB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvdWkvYnV0dG9uJztcbmltcG9ydCB7IGdldExvY2FsZURhdGVGb3JtYXQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1mb3Jnb3QtcGFzc3dvcmQnLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9yZ290LXBhc3N3b3JkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbRnBhc3N3b3Jkc2VydmljZV0sXG4gIHN0eWxlVXJsczogWycuL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEZvcmdvdFBhc3N3b3JkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgb3BfY29kZTogc3RyaW5nID0gICBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJvcC1jb2RlXCIpO1xuICA7XG4gIG1vYmlsZTogc3RyaW5nID0gXCJcIjtcbiAgcmVnOiBhbnkgPSAvXlxcZCskLztcbiAgb3ByZWc6IGFueSA9IC9eW0EtWl17MX1bMC05XXs4fS87XG4gIHZhbGlkb3AgPSB0cnVlO1xuICBvcGZpZWxkID0gdHJ1ZTtcbiAgdmFsaWRtb2JpbGUgPSB0cnVlO1xuICBtb2JpbGVmaWVsZCA9IHRydWU7XG4gIGNoZWNrbW9ibnVsbDtcbiAgY2hlY2tvcG51bGw7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIG15U2VydmljZTogRnBhc3N3b3Jkc2VydmljZSkgeyB9XG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgdmFsaWRhdGlvbigpIHtcbiAgICB0aGlzLmNoZWNrbW9ibnVsbCA9IHRoaXMubW9iaWxlLm1hdGNoKHRoaXMucmVnKTtcbiAgICB0aGlzLmNoZWNrb3BudWxsID0gdGhpcy5vcF9jb2RlLmxlbmd0aCA9PSA5O1xuICAgIGNvbnNvbGUubG9nKFwiT1Bjb2RlIDogXCIgKyB0aGlzLm9wX2NvZGUgKyBcIk1vYmlsZSA6XCIgKyB0aGlzLm1vYmlsZSk7XG4gICAgaWYgKHRoaXMub3BfY29kZSAhPSBcIlwiICYmIHRoaXMubW9iaWxlICE9IFwiXCIpIHtcblxuICAgICAgaWYgKHRoaXMubW9iaWxlLmxlbmd0aCA9PSAxMCAmJiB0aGlzLm1vYmlsZS5tYXRjaCh0aGlzLnJlZykgJiYgdGhpcy5vcF9jb2RlLmxlbmd0aCA9PSA5ICYmIHRoaXMub3BfY29kZS5tYXRjaCh0aGlzLm9wcmVnKSkge1xuICAgICAgICBkaWFsb2dzLmFsZXJ0KFwiVmFsaWQgTW9iaWxlIE51bWJlciBcIiArIHRoaXMubW9iaWxlKTtcbiAgICAgICAgdGhpcy5leHRyYWN0RGF0YSgpO1xuICAgICAgfVxuICAgICAgZWxzZSBpZiAoIXRoaXMuY2hlY2tvcG51bGwgfHwgdGhpcy5vcF9jb2RlLmxlbmd0aCAhPSA5KSB7XG4gICAgICAgIHRoaXMudmFsaWRvcCA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgZWxzZSBpZiAoIXRoaXMuY2hlY2ttb2JudWxsIHx8IHRoaXMubW9iaWxlLmxlbmd0aCAhPSAxMCkge1xuICAgICAgICB0aGlzLnZhbGlkbW9iaWxlID0gZmFsc2U7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgdGhpcy52YWxpZG9wID0gZmFsc2U7XG4gICAgICAgIHRoaXMudmFsaWRtb2JpbGUgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5vcF9jb2RlID09IFwiXCIgJiYgdGhpcy5tb2JpbGUgPT0gXCJcIikge1xuICAgICAgdGhpcy5vcGZpZWxkID0gZmFsc2U7XG4gICAgICB0aGlzLm1vYmlsZWZpZWxkID0gZmFsc2U7XG4gICAgICBjb25zb2xlLmxvZyhcImJvdGggZmllbGQgZW1wdHlcIik7XG4gICAgfVxuICAgIGVsc2UgaWYgKHRoaXMub3BfY29kZSA9PSBcIlwiKSB7XG4gICAgICB0aGlzLm9wZmllbGQgPSBmYWxzZTtcbiAgICAgIGNvbnNvbGUubG9nKFwib3Bjb2RlIGZpZWxkIGlzIGVtcHR5XCIpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHRoaXMubW9iaWxlZmllbGQgPSBmYWxzZTtcbiAgICAgIGNvbnNvbGUubG9nKFwibW9iaWxlIGZpZWxkIGlzIGVtcHR5XCIpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybk9QKGFyZ3MpIHtcbiAgICBpZiAodGhpcy5vcGZpZWxkID09IGZhbHNlKVxuICAgICAgdGhpcy5vcGZpZWxkID0gdHJ1ZTtcbiAgICBpZiAodGhpcy52YWxpZG9wID09IGZhbHNlKVxuICAgICAgdGhpcy52YWxpZG9wID0gdHJ1ZTtcbiAgfVxuICByZXR1cm5Nb2IoYXJncykge1xuICAgIGlmICh0aGlzLm1vYmlsZWZpZWxkID09IGZhbHNlKVxuICAgICAgdGhpcy5tb2JpbGVmaWVsZCA9IHRydWU7XG4gICAgaWYgKHRoaXMudmFsaWRtb2JpbGUgPT0gZmFsc2UpXG4gICAgICB0aGlzLnZhbGlkbW9iaWxlID0gdHJ1ZTtcbiAgfVxuXG4gIGNoYW5nZVVwcGVyKCkge1xuICAgIHRoaXMub3BfY29kZS50b1VwcGVyQ2FzZTtcbiAgfVxuXG5cbiAgZXh0cmFjdERhdGEoKSB7XG4gICAgLy8gY29uc29sZS5sb2codGhpcy5vcF9jb2RlKTtcbiAgICB0aGlzLm15U2VydmljZS5nZXREYXRhKHRoaXMub3BfY29kZSwgdGhpcy5tb2JpbGUpXG4gICAgICAuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KTtcbiAgICAgICAgaWYgKHJlc3VsdCA9PSB0cnVlKSB7XG4gICAgICAgICAgdGhpcy5vbkdldERhdGFTdWNjZXNzKHJlc3VsdCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZGlhbG9ncy5hbGVydChcImVudGVyIHZhbGlkIGNyZWRlbnRpYWxzXCIpO1xuICAgICAgICB9XG4gICAgICB9LCAoZXJyb3IpID0+IHtcbiAgICAgICAgLy90aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3RwJyk7XG4gICAgICAgIGRpYWxvZ3MuYWxlcnQoXCJUcnkgYWdhaW4gbGF0ZXJcIiArIHRoaXMub3BfY29kZSk7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcblxuICAgICAgfSk7XG4gIH1cblxuICBwcml2YXRlIG9uR2V0RGF0YVN1Y2Nlc3MocmVzKSB7XG4gICAgY29uc29sZS5sb2coXCJpbnNpZGUgc3VjY1wiKTtcbiAgICAvL3RoaXMudmFyID0gcmVzLm9wX2NvZGU7XG4gICAgdGhpcy5vcF9jb2RlID0gcmVzLm9wX2NvZGU7XG4gICAgZGlhbG9ncy5hbGVydChcIm90cCBzZW50IHRvIHRoZSBtb2JpbGUgbnVtYmVyXCIpO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vdHAnKTtcbiAgfVxuXG5cbn1cbiJdfQ==