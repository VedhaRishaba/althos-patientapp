import { Component, OnInit } from '@angular/core';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Fpasswordservice } from './fpasswordservice';
import { EventData } from 'tns-core-modules/data/observable';
import { Router } from '@angular/router';
import { Page } from "tns-core-modules/ui/page/page";
import { Button } from 'tns-core-modules/ui/button';
import { getLocaleDateFormat } from '@angular/common';

import * as appSettings from "tns-core-modules/application-settings";

@Component({
  selector: 'ns-forgot-password',
  templateUrl: './forgot-password.component.html',
  providers: [Fpasswordservice],
  styleUrls: ['./forgot-password.component.css'],
  moduleId: module.id,
})
export class ForgotPasswordComponent implements OnInit {
  op_code: string =   appSettings.getString("op-code");
  ;
  mobile: string = "";
  reg: any = /^\d+$/;
  opreg: any = /^[A-Z]{1}[0-9]{8}/;
  validop = true;
  opfield = true;
  validmobile = true;
  mobilefield = true;
  checkmobnull;
  checkopnull;

  constructor(private page: Page, private router: Router, private myService: Fpasswordservice) { }
  ngOnInit() {
  }

  validation() {
    this.checkmobnull = this.mobile.match(this.reg);
    this.checkopnull = this.op_code.length == 9;
    console.log("OPcode : " + this.op_code + "Mobile :" + this.mobile);
    if (this.op_code != "" && this.mobile != "") {

      if (this.mobile.length == 10 && this.mobile.match(this.reg) && this.op_code.length == 9 && this.op_code.match(this.opreg)) {
        dialogs.alert("Valid Mobile Number " + this.mobile);
        this.extractData();
      }
      else if (!this.checkopnull || this.op_code.length != 9) {
        this.validop = false;
      }
      else if (!this.checkmobnull || this.mobile.length != 10) {
        this.validmobile = false;
      }
      else {
        this.validop = false;
        this.validmobile = false;
      }
    }
    else if (this.op_code == "" && this.mobile == "") {
      this.opfield = false;
      this.mobilefield = false;
      console.log("both field empty");
    }
    else if (this.op_code == "") {
      this.opfield = false;
      console.log("opcode field is empty");
    }
    else {
      this.mobilefield = false;
      console.log("mobile field is empty");
    }
  }

  returnOP(args) {
    if (this.opfield == false)
      this.opfield = true;
    if (this.validop == false)
      this.validop = true;
  }
  returnMob(args) {
    if (this.mobilefield == false)
      this.mobilefield = true;
    if (this.validmobile == false)
      this.validmobile = true;
  }

  changeUpper() {
    this.op_code.toUpperCase;
  }


  extractData() {
    // console.log(this.op_code);
    this.myService.getData(this.op_code, this.mobile)
      .subscribe((result) => {
        console.log(result);
        if (result == true) {
          this.onGetDataSuccess(result);
        } else {
          dialogs.alert("enter valid credentials");
        }
      }, (error) => {
        //this.router.navigateByUrl('/otp');
        dialogs.alert("Try again later" + this.op_code);
        console.log(error);

      });
  }

  private onGetDataSuccess(res) {
    console.log("inside succ");
    //this.var = res.op_code;
    this.op_code = res.op_code;
    dialogs.alert("otp sent to the mobile number");
    this.router.navigateByUrl('/otp');
  }


}
