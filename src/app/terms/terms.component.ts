import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css'],
  moduleId: module.id,
})

export class TermsComponent {
    loading = false;
  public webViewSrc: string = "https://docs.google.com/gview?embedded=true&url=http://www.psghospitals.com/mob%20app/TERMS%20AND%20CONDITIONS.pdf";
  pageLoaded(){
     this.loading = true;
  }
}
