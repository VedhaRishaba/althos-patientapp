"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TermsComponent = /** @class */ (function () {
    function TermsComponent() {
        this.loading = false;
        this.webViewSrc = "https://docs.google.com/gview?embedded=true&url=http://www.psghospitals.com/mob%20app/TERMS%20AND%20CONDITIONS.pdf";
    }
    TermsComponent.prototype.pageLoaded = function () {
        this.loading = true;
    };
    TermsComponent = __decorate([
        core_1.Component({
            selector: 'ns-terms',
            templateUrl: './terms.component.html',
            styleUrls: ['./terms.component.css'],
            moduleId: module.id,
        })
    ], TermsComponent);
    return TermsComponent;
}());
exports.TermsComponent = TermsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVybXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidGVybXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBU2xEO0lBUEE7UUFRSSxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ1gsZUFBVSxHQUFXLG9IQUFvSCxDQUFDO0lBSW5KLENBQUM7SUFIQyxtQ0FBVSxHQUFWO1FBQ0csSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUxVLGNBQWM7UUFQMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDcEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7T0FFVyxjQUFjLENBTTFCO0lBQUQscUJBQUM7Q0FBQSxBQU5ELElBTUM7QUFOWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLXRlcm1zJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3Rlcm1zLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdGVybXMuY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcblxuZXhwb3J0IGNsYXNzIFRlcm1zQ29tcG9uZW50IHtcbiAgICBsb2FkaW5nID0gZmFsc2U7XG4gIHB1YmxpYyB3ZWJWaWV3U3JjOiBzdHJpbmcgPSBcImh0dHBzOi8vZG9jcy5nb29nbGUuY29tL2d2aWV3P2VtYmVkZGVkPXRydWUmdXJsPWh0dHA6Ly93d3cucHNnaG9zcGl0YWxzLmNvbS9tb2IlMjBhcHAvVEVSTVMlMjBBTkQlMjBDT05ESVRJT05TLnBkZlwiO1xuICBwYWdlTG9hZGVkKCl7XG4gICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gIH1cbn1cbiJdfQ==