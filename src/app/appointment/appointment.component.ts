import { Component, OnInit } from '@angular/core';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Router } from '@angular/router';
import * as appSettings from "tns-core-modules/application-settings";

@Component({
  selector: 'ns-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css'],
  moduleId: module.id,
})
export class AppointmentComponent implements OnInit {
    passData =0;
    one="~/images/one.png";
    two="~/images/two_gray.png";
    three="~/images/three_gray.png";
    four="~/images/four_gray.png";
    five="~/images/five_gray.png";
    currentStep = 1;
    action_title="Book Appointment";
    constructor(private router:Router) { }

    ngOnInit() { }

    step1(){
      this.one="~/images/one.png";
      this.two="~/images/two_gray.png";
      this.three="~/images/three_gray.png";
      this.four="~/images/four_gray.png";
      this.five="~/images/five_gray.png";
      this.currentStep = 1;

    this.action_title="Book Appointment";
    }

    step2(){
      this.currentStep = 2;
      this.one="~/images/checked.png";
      this.two="~/images/two.png";
      this.three="~/images/three_gray.png";
      this.four="~/images/four_gray.png";
      this.five="~/images/five_gray.png";

      this.action_title="Departments";
    }

    step3(){
      this.currentStep = 3;
      this.one="~/images/checked.png";
      this.two="~/images/checked.png";
      this.three="~/images/three.png";
      this.four="~/images/four_gray.png";
      this.five="~/images/five_gray.png";

      this.action_title="Doctors";
    }

    step4(){
      this.currentStep = 4;
      this.one="~/images/checked.png";
      this.two="~/images/checked.png";
      this.three="~/images/checked.png";
      this.four="~/images/four.png";
      this.five="~/images/five_gray.png";

      this.action_title="Payment";
    }

    step5(){
      this.currentStep = 5;
      this.one="~/images/checked.png";
      this.two="~/images/checked.png";
      this.three="~/images/checked.png";
      this.four="~/images/checked.png";
      this.five="~/images/five.png";

      this.action_title="Report";
    }



  logout() {
    dialogs.confirm({
      message: "Do you want to Logout ? ",
      okButtonText: "YES",
      cancelButtonText: "NO",
    }).then(result => {
      if (result) {
        this.logoutactivity();
      }
    });
  }
  logoutactivity() {
    const object1 = {
      op_code: ""
    };
    const object2 = {
      op_code:  appSettings.getString("op-code")
    }
    const searchParams = Object.keys(object1).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
    }).join('&');

    fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
        "charset": "utf-8"
      },
      body: searchParams,
    }).then((r) => r.text())
      .then((text) => {
        console.log("message" + text);
        if (text.length == 4) {
          this.router.navigateByUrl('/op-code');
        }
        else {
          console.log("failed to logout");
        }
      }).catch((e) => {
        console.log("Error: ");
        console.log(e);
      });

  }


}
