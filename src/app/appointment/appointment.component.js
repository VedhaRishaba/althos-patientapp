"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var AppointmentComponent = /** @class */ (function () {
    function AppointmentComponent(router) {
        this.router = router;
        this.passData = 0;
        this.one = "~/images/one.png";
        this.two = "~/images/two_gray.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.currentStep = 1;
        this.action_title = "Book Appointment";
    }
    AppointmentComponent.prototype.ngOnInit = function () { };
    AppointmentComponent.prototype.step1 = function () {
        this.one = "~/images/one.png";
        this.two = "~/images/two_gray.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.currentStep = 1;
        this.action_title = "Book Appointment";
    };
    AppointmentComponent.prototype.step2 = function () {
        this.currentStep = 2;
        this.one = "~/images/checked.png";
        this.two = "~/images/two.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.action_title = "Departments";
    };
    AppointmentComponent.prototype.step3 = function () {
        this.currentStep = 3;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/three.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.action_title = "Doctors";
    };
    AppointmentComponent.prototype.step4 = function () {
        this.currentStep = 4;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/checked.png";
        this.four = "~/images/four.png";
        this.five = "~/images/five_gray.png";
        this.action_title = "Payment";
    };
    AppointmentComponent.prototype.step5 = function () {
        this.currentStep = 5;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/checked.png";
        this.four = "~/images/checked.png";
        this.five = "~/images/five.png";
        this.action_title = "Report";
    };
    AppointmentComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    AppointmentComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    AppointmentComponent = __decorate([
        core_1.Component({
            selector: 'ns-appointment',
            templateUrl: './appointment.component.html',
            styleUrls: ['./appointment.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], AppointmentComponent);
    return AppointmentComponent;
}());
exports.AppointmentComponent = AppointmentComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwb2ludG1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwb2ludG1lbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHFEQUF1RDtBQUN2RCwwQ0FBeUM7QUFDekMsbUVBQXFFO0FBUXJFO0lBU0ksOEJBQW9CLE1BQWE7UUFBYixXQUFNLEdBQU4sTUFBTSxDQUFPO1FBUmpDLGFBQVEsR0FBRSxDQUFDLENBQUM7UUFDWixRQUFHLEdBQUMsa0JBQWtCLENBQUM7UUFDdkIsUUFBRyxHQUFDLHVCQUF1QixDQUFDO1FBQzVCLFVBQUssR0FBQyx5QkFBeUIsQ0FBQztRQUNoQyxTQUFJLEdBQUMsd0JBQXdCLENBQUM7UUFDOUIsU0FBSSxHQUFDLHdCQUF3QixDQUFDO1FBQzlCLGdCQUFXLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLGlCQUFZLEdBQUMsa0JBQWtCLENBQUM7SUFDSyxDQUFDO0lBRXRDLHVDQUFRLEdBQVIsY0FBYSxDQUFDO0lBRWQsb0NBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxHQUFHLEdBQUMsa0JBQWtCLENBQUM7UUFDNUIsSUFBSSxDQUFDLEdBQUcsR0FBQyx1QkFBdUIsQ0FBQztRQUNqQyxJQUFJLENBQUMsS0FBSyxHQUFDLHlCQUF5QixDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7UUFDbkMsSUFBSSxDQUFDLElBQUksR0FBQyx3QkFBd0IsQ0FBQztRQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUV2QixJQUFJLENBQUMsWUFBWSxHQUFDLGtCQUFrQixDQUFDO0lBQ3JDLENBQUM7SUFFRCxvQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBQyxzQkFBc0IsQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxHQUFDLGtCQUFrQixDQUFDO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUMseUJBQXlCLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksR0FBQyx3QkFBd0IsQ0FBQztRQUNuQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBRW5DLElBQUksQ0FBQyxZQUFZLEdBQUMsYUFBYSxDQUFDO0lBQ2xDLENBQUM7SUFFRCxvQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBQyxzQkFBc0IsQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUMsb0JBQW9CLENBQUM7UUFDaEMsSUFBSSxDQUFDLElBQUksR0FBQyx3QkFBd0IsQ0FBQztRQUNuQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBRW5DLElBQUksQ0FBQyxZQUFZLEdBQUMsU0FBUyxDQUFDO0lBQzlCLENBQUM7SUFFRCxvQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBQyxzQkFBc0IsQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUMsc0JBQXNCLENBQUM7UUFDbEMsSUFBSSxDQUFDLElBQUksR0FBQyxtQkFBbUIsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBRW5DLElBQUksQ0FBQyxZQUFZLEdBQUMsU0FBUyxDQUFDO0lBQzlCLENBQUM7SUFFRCxvQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsR0FBQyxzQkFBc0IsQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUMsc0JBQXNCLENBQUM7UUFDbEMsSUFBSSxDQUFDLElBQUksR0FBQyxzQkFBc0IsQ0FBQztRQUNqQyxJQUFJLENBQUMsSUFBSSxHQUFDLG1CQUFtQixDQUFDO1FBRTlCLElBQUksQ0FBQyxZQUFZLEdBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7SUFJSCxxQ0FBTSxHQUFOO1FBQUEsaUJBVUM7UUFUQyxPQUFPLENBQUMsT0FBTyxDQUFDO1lBQ2QsT0FBTyxFQUFFLDBCQUEwQjtZQUNuQyxZQUFZLEVBQUUsS0FBSztZQUNuQixnQkFBZ0IsRUFBRSxJQUFJO1NBQ3ZCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO1lBQ1osSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsNkNBQWMsR0FBZDtRQUFBLGlCQWdDQztRQS9CQyxJQUFNLE9BQU8sR0FBRztZQUNkLE9BQU8sRUFBRSxFQUFFO1NBQ1osQ0FBQztRQUNGLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDO1NBQzNDLENBQUE7UUFDRCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7WUFDaEQsT0FBTyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWIsS0FBSyxDQUFDLGdEQUFnRCxFQUFFO1lBQ3RELE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLGNBQWMsRUFBRSxtQ0FBbUMsRUFBRSxlQUFlLEVBQUUscUVBQXFFO2dCQUMzSSxTQUFTLEVBQUUsT0FBTzthQUNuQjtZQUNELElBQUksRUFBRSxZQUFZO1NBQ25CLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQVIsQ0FBUSxDQUFDO2FBQ3JCLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUM5QixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUNwQixLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN2QztpQkFDSTtnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDakM7UUFDSCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxDQUFDO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQWpIVSxvQkFBb0I7UUFOaEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsV0FBVyxFQUFFLDhCQUE4QjtZQUMzQyxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztZQUMxQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzt5Q0FVNkIsZUFBTTtPQVR4QixvQkFBb0IsQ0FvSGhDO0lBQUQsMkJBQUM7Q0FBQSxBQXBIRCxJQW9IQztBQXBIWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0ICogYXMgYXBwU2V0dGluZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtYXBwb2ludG1lbnQnLFxuICB0ZW1wbGF0ZVVybDogJy4vYXBwb2ludG1lbnQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9hcHBvaW50bWVudC5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEFwcG9pbnRtZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBwYXNzRGF0YSA9MDtcbiAgICBvbmU9XCJ+L2ltYWdlcy9vbmUucG5nXCI7XG4gICAgdHdvPVwifi9pbWFnZXMvdHdvX2dyYXkucG5nXCI7XG4gICAgdGhyZWU9XCJ+L2ltYWdlcy90aHJlZV9ncmF5LnBuZ1wiO1xuICAgIGZvdXI9XCJ+L2ltYWdlcy9mb3VyX2dyYXkucG5nXCI7XG4gICAgZml2ZT1cIn4vaW1hZ2VzL2ZpdmVfZ3JheS5wbmdcIjtcbiAgICBjdXJyZW50U3RlcCA9IDE7XG4gICAgYWN0aW9uX3RpdGxlPVwiQm9vayBBcHBvaW50bWVudFwiO1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOlJvdXRlcikgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHsgfVxuXG4gICAgc3RlcDEoKXtcbiAgICAgIHRoaXMub25lPVwifi9pbWFnZXMvb25lLnBuZ1wiO1xuICAgICAgdGhpcy50d289XCJ+L2ltYWdlcy90d29fZ3JheS5wbmdcIjtcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy90aHJlZV9ncmF5LnBuZ1wiO1xuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvZm91cl9ncmF5LnBuZ1wiO1xuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDE7XG5cbiAgICB0aGlzLmFjdGlvbl90aXRsZT1cIkJvb2sgQXBwb2ludG1lbnRcIjtcbiAgICB9XG5cbiAgICBzdGVwMigpe1xuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDI7XG4gICAgICB0aGlzLm9uZT1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XG4gICAgICB0aGlzLnR3bz1cIn4vaW1hZ2VzL3R3by5wbmdcIjtcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy90aHJlZV9ncmF5LnBuZ1wiO1xuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvZm91cl9ncmF5LnBuZ1wiO1xuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xuXG4gICAgICB0aGlzLmFjdGlvbl90aXRsZT1cIkRlcGFydG1lbnRzXCI7XG4gICAgfVxuXG4gICAgc3RlcDMoKXtcbiAgICAgIHRoaXMuY3VycmVudFN0ZXAgPSAzO1xuICAgICAgdGhpcy5vbmU9XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xuICAgICAgdGhpcy50d289XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xuICAgICAgdGhpcy50aHJlZT1cIn4vaW1hZ2VzL3RocmVlLnBuZ1wiO1xuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvZm91cl9ncmF5LnBuZ1wiO1xuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xuXG4gICAgICB0aGlzLmFjdGlvbl90aXRsZT1cIkRvY3RvcnNcIjtcbiAgICB9XG5cbiAgICBzdGVwNCgpe1xuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDQ7XG4gICAgICB0aGlzLm9uZT1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XG4gICAgICB0aGlzLnR3bz1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XG4gICAgICB0aGlzLnRocmVlPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcbiAgICAgIHRoaXMuZm91cj1cIn4vaW1hZ2VzL2ZvdXIucG5nXCI7XG4gICAgICB0aGlzLmZpdmU9XCJ+L2ltYWdlcy9maXZlX2dyYXkucG5nXCI7XG5cbiAgICAgIHRoaXMuYWN0aW9uX3RpdGxlPVwiUGF5bWVudFwiO1xuICAgIH1cblxuICAgIHN0ZXA1KCl7XG4gICAgICB0aGlzLmN1cnJlbnRTdGVwID0gNTtcbiAgICAgIHRoaXMub25lPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcbiAgICAgIHRoaXMudHdvPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcbiAgICAgIHRoaXMuZml2ZT1cIn4vaW1hZ2VzL2ZpdmUucG5nXCI7XG5cbiAgICAgIHRoaXMuYWN0aW9uX3RpdGxlPVwiUmVwb3J0XCI7XG4gICAgfVxuXG5cblxuICBsb2dvdXQoKSB7XG4gICAgZGlhbG9ncy5jb25maXJtKHtcbiAgICAgIG1lc3NhZ2U6IFwiRG8geW91IHdhbnQgdG8gTG9nb3V0ID8gXCIsXG4gICAgICBva0J1dHRvblRleHQ6IFwiWUVTXCIsXG4gICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCIsXG4gICAgfSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgbG9nb3V0YWN0aXZpdHkoKSB7XG4gICAgY29uc3Qgb2JqZWN0MSA9IHtcbiAgICAgIG9wX2NvZGU6IFwiXCJcbiAgICB9O1xuICAgIGNvbnN0IG9iamVjdDIgPSB7XG4gICAgICBvcF9jb2RlOiAgYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwib3AtY29kZVwiKVxuICAgIH1cbiAgICBjb25zdCBzZWFyY2hQYXJhbXMgPSBPYmplY3Qua2V5cyhvYmplY3QxKS5tYXAoKGtleSkgPT4ge1xuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XG4gICAgfSkuam9pbignJicpO1xuXG4gICAgZmV0Y2goXCJodHRwczovL3BzZ2ltc3IuYWMuaW4vYWx0aG9zL2loc3Jlc3QvdjEvbG9nb3V0XCIsIHtcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsIFwiQXV0aG9yaXphdGlvblwiOiBcIkJlYXJlciBVMU5uY0Zka2R6VlRVMmR3VjJSM05sOUpSRHBUVTJkd1YyUjNOVk5UWjNCWFpIYzJYMU5GUzFKRlZBPT1cIixcbiAgICAgICAgXCJjaGFyc2V0XCI6IFwidXRmLThcIlxuICAgICAgfSxcbiAgICAgIGJvZHk6IHNlYXJjaFBhcmFtcyxcbiAgICB9KS50aGVuKChyKSA9PiByLnRleHQoKSlcbiAgICAgIC50aGVuKCh0ZXh0KSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwibWVzc2FnZVwiICsgdGV4dCk7XG4gICAgICAgIGlmICh0ZXh0Lmxlbmd0aCA9PSA0KSB7XG4gICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL29wLWNvZGUnKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcImZhaWxlZCB0byBsb2dvdXRcIik7XG4gICAgICAgIH1cbiAgICAgIH0pLmNhdGNoKChlKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3I6IFwiKTtcbiAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICB9KTtcblxuICB9XG5cblxufVxuIl19