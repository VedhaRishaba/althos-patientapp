import { Component, OnInit } from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field";
import { Fopcodepageservice } from "./fopcodepageservice";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import * as Toast from "nativescript-toast";
import {send_opcode_service } from "./send-opcode-service";

import { OpCodeComponent } from "../op-code/op-code.component";
@Component({
    selector: "ns-forgot-op",
    templateUrl: "./forgot-op.component.html",
    styleUrls: ["./forgot-op.component.css"],
    providers: [Fopcodepageservice,send_opcode_service],
    moduleId: module.id
})
export class ForgotOPComponent implements OnInit {
    mobile: string = "";
    //regex:any=/^[0-9]/;
    valid = true;
    flag = true;
    reg: any = /^\d+$/;
    forgetopcode = [];
    public firstTx: string = "";
    op_code = new Array();
    patient_name = new Array();
    i;
    list_user_name = false;
    selectpatientname = true;
    loading = true;
    data = [];
    constructor(
        private page: Page,
        private myService: Fopcodepageservice,
        private router: Router,
private sendopcodeService :send_opcode_service
    ) {
        this.op_code = new Array();
        this.patient_name = new Array();
    }

    ngOnInit() { }

    onItemTap(args) {
        for (this.i = 0; this.i < Object.keys(this.data).length; this.i++) {
            if (this.data[args.index].op_code == this.data[this.i].op_code) {
                if (this.data[args.index].select == true) this.selectpatientname = false;
                else this.data[args.index].select = true;

            } else {

                this.data[this.i].select = false;
            }
        }

      

    }
    public onTextChange(args) {
        let textField = <TextField>args.object;
        console.log("onTextChange");
        this.firstTx = textField.text;
    }

    getMobileNumber() {
        if (this.mobile.localeCompare("") != 0) {
            if (this.mobile.length == 10 && this.mobile.match(this.reg)) {
              
              
                this.list_user_name = false;
                this.extractData();
                
               

            } else {
                this.flag = false;
                console.log("invalid mobile");
                //dialogs.alert("Invalid Mobile Number");
            }
        } else {
            this.valid = false;
            console.log("no blank");
            //dialogs.alert("Mobile Number cannot be blank");
        }
    }
    returnPress(args) {
        console.log("Entered : " + this.mobile);
        if (this.valid == false) {
            this.valid = true;
            console.log("valid : " + this.valid);
        } else if (this.flag == false) {
            this.flag = true;
            console.log("valid : " + this.flag);
        }
    }


    extractData() {
       
        this.loading = false;
        this.myService.getData(this.mobile).subscribe(
            result => {
               
                if (result != null && result != "") {
                    let val = Object.keys(result).length;
                    this.data=[];
        
                    for (this.i = 0; this.i < val; this.i++) {
                        this.data.push({ op_code: result[this.i]["op_code"], patient_name: result[this.i]["patient_name"], select: false });
                    }

                   
                this.loading = true;
                this.list_user_name = true;
                } else {
                    
                this.loading = true;
                    var toast = Toast.makeText("No patients found!");
                    toast.show();
                }
                
            },
            error => {
                dialogs.alert("inValid mobile" + this.mobile);
                console.log(error);
            }
        );
    }

    private onGetDataSuccess(res) {
        this.mobile = res.mobile;
        dialogs.alert("Valid mobile" + this.mobile);
        this.router.navigateByUrl("/op-code");
    }



    send_opcode(){

        for(this.i=0;this.i <Object.keys(this.data).length;this.i++){
            if(this.data[this.i].select == true){
              
console.log('op ===>'+this.data[this.i].op_code);

console.log('mobile ===>'+this.mobile);
                const object1={
                    opCode:"",
                    mobile:"",
                    };
                   const object2={
                    opCode: this.data[this.i].op_code,
                   mobile:this.mobile
                   }
                
                  const searchParams = Object.keys(object1).map((key) => {
                  return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
                  }).join('&');
              
                  this.sendopcodeService.getData(searchParams)
                    .subscribe((result) => {
                        
                      var toast = Toast.makeText("OP code sent to mobile number");
                      toast.show();
                      this.router.navigateByUrl('/op-code');
                    }, (error) => {
                      console.log(error);
                    });
            }
        }

        
    }























    

}


