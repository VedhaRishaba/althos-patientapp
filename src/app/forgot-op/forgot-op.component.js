"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var fopcodepageservice_1 = require("./fopcodepageservice");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var page_1 = require("tns-core-modules/ui/page");
var Toast = require("nativescript-toast");
var send_opcode_service_1 = require("./send-opcode-service");
var ForgotOPComponent = /** @class */ (function () {
    function ForgotOPComponent(page, myService, router, sendopcodeService) {
        this.page = page;
        this.myService = myService;
        this.router = router;
        this.sendopcodeService = sendopcodeService;
        this.mobile = "";
        //regex:any=/^[0-9]/;
        this.valid = true;
        this.flag = true;
        this.reg = /^\d+$/;
        this.forgetopcode = [];
        this.firstTx = "";
        this.op_code = new Array();
        this.patient_name = new Array();
        this.list_user_name = false;
        this.selectpatientname = true;
        this.loading = true;
        this.data = [];
        this.op_code = new Array();
        this.patient_name = new Array();
    }
    ForgotOPComponent.prototype.ngOnInit = function () { };
    ForgotOPComponent.prototype.onItemTap = function (args) {
        for (this.i = 0; this.i < Object.keys(this.data).length; this.i++) {
            if (this.data[args.index].op_code == this.data[this.i].op_code) {
                if (this.data[args.index].select == true)
                    this.selectpatientname = false;
                else
                    this.data[args.index].select = true;
            }
            else {
                this.data[this.i].select = false;
            }
        }
    };
    ForgotOPComponent.prototype.onTextChange = function (args) {
        var textField = args.object;
        console.log("onTextChange");
        this.firstTx = textField.text;
    };
    ForgotOPComponent.prototype.getMobileNumber = function () {
        if (this.mobile.localeCompare("") != 0) {
            if (this.mobile.length == 10 && this.mobile.match(this.reg)) {
                this.list_user_name = false;
                this.extractData();
            }
            else {
                this.flag = false;
                console.log("invalid mobile");
                //dialogs.alert("Invalid Mobile Number");
            }
        }
        else {
            this.valid = false;
            console.log("no blank");
            //dialogs.alert("Mobile Number cannot be blank");
        }
    };
    ForgotOPComponent.prototype.returnPress = function (args) {
        console.log("Entered : " + this.mobile);
        if (this.valid == false) {
            this.valid = true;
            console.log("valid : " + this.valid);
        }
        else if (this.flag == false) {
            this.flag = true;
            console.log("valid : " + this.flag);
        }
    };
    ForgotOPComponent.prototype.extractData = function () {
        var _this = this;
        this.loading = false;
        this.myService.getData(this.mobile).subscribe(function (result) {
            if (result != null && result != "") {
                var val = Object.keys(result).length;
                _this.data = [];
                for (_this.i = 0; _this.i < val; _this.i++) {
                    _this.data.push({ op_code: result[_this.i]["op_code"], patient_name: result[_this.i]["patient_name"], select: false });
                }
                _this.loading = true;
                _this.list_user_name = true;
            }
            else {
                _this.loading = true;
                var toast = Toast.makeText("No patients found!");
                toast.show();
            }
        }, function (error) {
            dialogs.alert("inValid mobile" + _this.mobile);
            console.log(error);
        });
    };
    ForgotOPComponent.prototype.onGetDataSuccess = function (res) {
        this.mobile = res.mobile;
        dialogs.alert("Valid mobile" + this.mobile);
        this.router.navigateByUrl("/op-code");
    };
    ForgotOPComponent.prototype.send_opcode = function () {
        var _this = this;
        var _loop_1 = function () {
            if (this_1.data[this_1.i].select == true) {
                console.log('op ===>' + this_1.data[this_1.i].op_code);
                console.log('mobile ===>' + this_1.mobile);
                var object1 = {
                    opCode: "",
                    mobile: "",
                };
                var object2_1 = {
                    opCode: this_1.data[this_1.i].op_code,
                    mobile: this_1.mobile
                };
                var searchParams = Object.keys(object1).map(function (key) {
                    return encodeURIComponent(key) + '=' + encodeURIComponent(object2_1[key]);
                }).join('&');
                this_1.sendopcodeService.getData(searchParams)
                    .subscribe(function (result) {
                    var toast = Toast.makeText("OP code sent to mobile number");
                    toast.show();
                    _this.router.navigateByUrl('/op-code');
                }, function (error) {
                    console.log(error);
                });
            }
        };
        var this_1 = this;
        for (this.i = 0; this.i < Object.keys(this.data).length; this.i++) {
            _loop_1();
        }
    };
    ForgotOPComponent = __decorate([
        core_1.Component({
            selector: "ns-forgot-op",
            templateUrl: "./forgot-op.component.html",
            styleUrls: ["./forgot-op.component.css"],
            providers: [fopcodepageservice_1.Fopcodepageservice, send_opcode_service_1.send_opcode_service],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [page_1.Page,
            fopcodepageservice_1.Fopcodepageservice,
            router_1.Router,
            send_opcode_service_1.send_opcode_service])
    ], ForgotOPComponent);
    return ForgotOPComponent;
}());
exports.ForgotOPComponent = ForgotOPComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9yZ290LW9wLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvcmdvdC1vcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFFbEQsMkRBQTBEO0FBQzFELHFEQUF1RDtBQUN2RCwwQ0FBeUM7QUFDekMsaURBQWdEO0FBQ2hELDBDQUE0QztBQUM1Qyw2REFBMkQ7QUFVM0Q7SUFlSSwyQkFDWSxJQUFVLEVBQ1YsU0FBNkIsRUFDN0IsTUFBYyxFQUN0QixpQkFBc0M7UUFIOUIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGNBQVMsR0FBVCxTQUFTLENBQW9CO1FBQzdCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDdEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFxQjtRQWxCMUMsV0FBTSxHQUFXLEVBQUUsQ0FBQztRQUNwQixxQkFBcUI7UUFDckIsVUFBSyxHQUFHLElBQUksQ0FBQztRQUNiLFNBQUksR0FBRyxJQUFJLENBQUM7UUFDWixRQUFHLEdBQVEsT0FBTyxDQUFDO1FBQ25CLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ1gsWUFBTyxHQUFXLEVBQUUsQ0FBQztRQUM1QixZQUFPLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUN0QixpQkFBWSxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFFM0IsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixTQUFJLEdBQUcsRUFBRSxDQUFDO1FBT04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBRUQsb0NBQVEsR0FBUixjQUFhLENBQUM7SUFFZCxxQ0FBUyxHQUFULFVBQVUsSUFBSTtRQUNWLEtBQUssSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQy9ELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRTtnQkFDNUQsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksSUFBSTtvQkFBRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDOztvQkFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzthQUU1QztpQkFBTTtnQkFFSCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2FBQ3BDO1NBQ0o7SUFJTCxDQUFDO0lBQ00sd0NBQVksR0FBbkIsVUFBb0IsSUFBSTtRQUNwQixJQUFJLFNBQVMsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFFRCwyQ0FBZSxHQUFmO1FBQ0ksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDcEMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUd6RCxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztnQkFDNUIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBSXRCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2dCQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlCLHlDQUF5QzthQUM1QztTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hCLGlEQUFpRDtTQUNwRDtJQUNMLENBQUM7SUFDRCx1Q0FBVyxHQUFYLFVBQVksSUFBSTtRQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN4QyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN4QzthQUFNLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxLQUFLLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUdELHVDQUFXLEdBQVg7UUFBQSxpQkE4QkM7UUE1QkcsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDekMsVUFBQSxNQUFNO1lBRUYsSUFBSSxNQUFNLElBQUksSUFBSSxJQUFJLE1BQU0sSUFBSSxFQUFFLEVBQUU7Z0JBQ2hDLElBQUksR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUNyQyxLQUFJLENBQUMsSUFBSSxHQUFDLEVBQUUsQ0FBQztnQkFFYixLQUFLLEtBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFLEtBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDckMsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBRSxZQUFZLEVBQUUsTUFBTSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztpQkFDdkg7Z0JBR0wsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQzFCO2lCQUFNO2dCQUVQLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7Z0JBQ2pELEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNoQjtRQUVMLENBQUMsRUFDRCxVQUFBLEtBQUs7WUFDRCxPQUFPLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZCLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUVPLDRDQUFnQixHQUF4QixVQUF5QixHQUFHO1FBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUN6QixPQUFPLENBQUMsS0FBSyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUlELHVDQUFXLEdBQVg7UUFBQSxpQkFrQ0M7O1lBL0JPLElBQUcsT0FBSyxJQUFJLENBQUMsT0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFDO2dCQUVoRCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBQyxPQUFLLElBQUksQ0FBQyxPQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUVqRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDO2dCQUN2QixJQUFNLE9BQU8sR0FBQztvQkFDVixNQUFNLEVBQUMsRUFBRTtvQkFDVCxNQUFNLEVBQUMsRUFBRTtpQkFDUixDQUFDO2dCQUNILElBQU0sU0FBTyxHQUFDO29CQUNiLE1BQU0sRUFBRSxPQUFLLElBQUksQ0FBQyxPQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU87b0JBQ2xDLE1BQU0sRUFBQyxPQUFLLE1BQU07aUJBQ2pCLENBQUE7Z0JBRUYsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHO29CQUNsRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxTQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUViLE9BQUssaUJBQWlCLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztxQkFDekMsU0FBUyxDQUFDLFVBQUMsTUFBTTtvQkFFaEIsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO29CQUM1RCxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQ2IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hDLENBQUMsRUFBRSxVQUFDLEtBQUs7b0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsQ0FBQyxDQUFDLENBQUM7YUFDVjtRQUNMLENBQUM7O1FBN0JELEtBQUksSUFBSSxDQUFDLENBQUMsR0FBQyxDQUFDLEVBQUMsSUFBSSxDQUFDLENBQUMsR0FBRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTs7U0E2QjFEO0lBR0wsQ0FBQztJQTNKUSxpQkFBaUI7UUFQN0IsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFdBQVcsRUFBRSw0QkFBNEI7WUFDekMsU0FBUyxFQUFFLENBQUMsMkJBQTJCLENBQUM7WUFDeEMsU0FBUyxFQUFFLENBQUMsdUNBQWtCLEVBQUMseUNBQW1CLENBQUM7WUFDbkQsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3RCLENBQUM7eUNBaUJvQixXQUFJO1lBQ0MsdUNBQWtCO1lBQ3JCLGVBQU07WUFDSCx5Q0FBbUI7T0FuQmpDLGlCQUFpQixDQXFMN0I7SUFBRCx3QkFBQztDQUFBLEFBckxELElBcUxDO0FBckxZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFRleHRGaWVsZCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3RleHQtZmllbGRcIjtcbmltcG9ydCB7IEZvcGNvZGVwYWdlc2VydmljZSB9IGZyb20gXCIuL2ZvcGNvZGVwYWdlc2VydmljZVwiO1xuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xuaW1wb3J0ICogYXMgVG9hc3QgZnJvbSBcIm5hdGl2ZXNjcmlwdC10b2FzdFwiO1xuaW1wb3J0IHtzZW5kX29wY29kZV9zZXJ2aWNlIH0gZnJvbSBcIi4vc2VuZC1vcGNvZGUtc2VydmljZVwiO1xuXG5pbXBvcnQgeyBPcENvZGVDb21wb25lbnQgfSBmcm9tIFwiLi4vb3AtY29kZS9vcC1jb2RlLmNvbXBvbmVudFwiO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtZm9yZ290LW9wXCIsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9mb3Jnb3Qtb3AuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFtcIi4vZm9yZ290LW9wLmNvbXBvbmVudC5jc3NcIl0sXG4gICAgcHJvdmlkZXJzOiBbRm9wY29kZXBhZ2VzZXJ2aWNlLHNlbmRfb3Bjb2RlX3NlcnZpY2VdLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWRcbn0pXG5leHBvcnQgY2xhc3MgRm9yZ290T1BDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIG1vYmlsZTogc3RyaW5nID0gXCJcIjtcbiAgICAvL3JlZ2V4OmFueT0vXlswLTldLztcbiAgICB2YWxpZCA9IHRydWU7XG4gICAgZmxhZyA9IHRydWU7XG4gICAgcmVnOiBhbnkgPSAvXlxcZCskLztcbiAgICBmb3JnZXRvcGNvZGUgPSBbXTtcbiAgICBwdWJsaWMgZmlyc3RUeDogc3RyaW5nID0gXCJcIjtcbiAgICBvcF9jb2RlID0gbmV3IEFycmF5KCk7XG4gICAgcGF0aWVudF9uYW1lID0gbmV3IEFycmF5KCk7XG4gICAgaTtcbiAgICBsaXN0X3VzZXJfbmFtZSA9IGZhbHNlO1xuICAgIHNlbGVjdHBhdGllbnRuYW1lID0gdHJ1ZTtcbiAgICBsb2FkaW5nID0gdHJ1ZTtcbiAgICBkYXRhID0gW107XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgcGFnZTogUGFnZSxcbiAgICAgICAgcHJpdmF0ZSBteVNlcnZpY2U6IEZvcGNvZGVwYWdlc2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbnByaXZhdGUgc2VuZG9wY29kZVNlcnZpY2UgOnNlbmRfb3Bjb2RlX3NlcnZpY2VcbiAgICApIHtcbiAgICAgICAgdGhpcy5vcF9jb2RlID0gbmV3IEFycmF5KCk7XG4gICAgICAgIHRoaXMucGF0aWVudF9uYW1lID0gbmV3IEFycmF5KCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxuICAgIG9uSXRlbVRhcChhcmdzKSB7XG4gICAgICAgIGZvciAodGhpcy5pID0gMDsgdGhpcy5pIDwgT2JqZWN0LmtleXModGhpcy5kYXRhKS5sZW5ndGg7IHRoaXMuaSsrKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5kYXRhW2FyZ3MuaW5kZXhdLm9wX2NvZGUgPT0gdGhpcy5kYXRhW3RoaXMuaV0ub3BfY29kZSkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRhdGFbYXJncy5pbmRleF0uc2VsZWN0ID09IHRydWUpIHRoaXMuc2VsZWN0cGF0aWVudG5hbWUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBlbHNlIHRoaXMuZGF0YVthcmdzLmluZGV4XS5zZWxlY3QgPSB0cnVlO1xuXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhW3RoaXMuaV0uc2VsZWN0ID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgXG5cbiAgICB9XG4gICAgcHVibGljIG9uVGV4dENoYW5nZShhcmdzKSB7XG4gICAgICAgIGxldCB0ZXh0RmllbGQgPSA8VGV4dEZpZWxkPmFyZ3Mub2JqZWN0O1xuICAgICAgICBjb25zb2xlLmxvZyhcIm9uVGV4dENoYW5nZVwiKTtcbiAgICAgICAgdGhpcy5maXJzdFR4ID0gdGV4dEZpZWxkLnRleHQ7XG4gICAgfVxuXG4gICAgZ2V0TW9iaWxlTnVtYmVyKCkge1xuICAgICAgICBpZiAodGhpcy5tb2JpbGUubG9jYWxlQ29tcGFyZShcIlwiKSAhPSAwKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5tb2JpbGUubGVuZ3RoID09IDEwICYmIHRoaXMubW9iaWxlLm1hdGNoKHRoaXMucmVnKSkge1xuICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5saXN0X3VzZXJfbmFtZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHRoaXMuZXh0cmFjdERhdGEoKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgIFxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuZmxhZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiaW52YWxpZCBtb2JpbGVcIik7XG4gICAgICAgICAgICAgICAgLy9kaWFsb2dzLmFsZXJ0KFwiSW52YWxpZCBNb2JpbGUgTnVtYmVyXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy52YWxpZCA9IGZhbHNlO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJubyBibGFua1wiKTtcbiAgICAgICAgICAgIC8vZGlhbG9ncy5hbGVydChcIk1vYmlsZSBOdW1iZXIgY2Fubm90IGJlIGJsYW5rXCIpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVyblByZXNzKGFyZ3MpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJFbnRlcmVkIDogXCIgKyB0aGlzLm1vYmlsZSk7XG4gICAgICAgIGlmICh0aGlzLnZhbGlkID09IGZhbHNlKSB7XG4gICAgICAgICAgICB0aGlzLnZhbGlkID0gdHJ1ZTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidmFsaWQgOiBcIiArIHRoaXMudmFsaWQpO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZmxhZyA9PSBmYWxzZSkge1xuICAgICAgICAgICAgdGhpcy5mbGFnID0gdHJ1ZTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidmFsaWQgOiBcIiArIHRoaXMuZmxhZyk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIGV4dHJhY3REYXRhKCkge1xuICAgICAgIFxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5teVNlcnZpY2UuZ2V0RGF0YSh0aGlzLm1vYmlsZSkuc3Vic2NyaWJlKFxuICAgICAgICAgICAgcmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgIT0gbnVsbCAmJiByZXN1bHQgIT0gXCJcIikge1xuICAgICAgICAgICAgICAgICAgICBsZXQgdmFsID0gT2JqZWN0LmtleXMocmVzdWx0KS5sZW5ndGg7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YT1bXTtcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIGZvciAodGhpcy5pID0gMDsgdGhpcy5pIDwgdmFsOyB0aGlzLmkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLnB1c2goeyBvcF9jb2RlOiByZXN1bHRbdGhpcy5pXVtcIm9wX2NvZGVcIl0sIHBhdGllbnRfbmFtZTogcmVzdWx0W3RoaXMuaV1bXCJwYXRpZW50X25hbWVcIl0sIHNlbGVjdDogZmFsc2UgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0X3VzZXJfbmFtZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJObyBwYXRpZW50cyBmb3VuZCFcIik7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0LnNob3coKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIGRpYWxvZ3MuYWxlcnQoXCJpblZhbGlkIG1vYmlsZVwiICsgdGhpcy5tb2JpbGUpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uR2V0RGF0YVN1Y2Nlc3MocmVzKSB7XG4gICAgICAgIHRoaXMubW9iaWxlID0gcmVzLm1vYmlsZTtcbiAgICAgICAgZGlhbG9ncy5hbGVydChcIlZhbGlkIG1vYmlsZVwiICsgdGhpcy5tb2JpbGUpO1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKFwiL29wLWNvZGVcIik7XG4gICAgfVxuXG5cblxuICAgIHNlbmRfb3Bjb2RlKCl7XG5cbiAgICAgICAgZm9yKHRoaXMuaT0wO3RoaXMuaSA8T2JqZWN0LmtleXModGhpcy5kYXRhKS5sZW5ndGg7dGhpcy5pKyspe1xuICAgICAgICAgICAgaWYodGhpcy5kYXRhW3RoaXMuaV0uc2VsZWN0ID09IHRydWUpe1xuICAgICAgICAgICAgICBcbmNvbnNvbGUubG9nKCdvcCA9PT0+Jyt0aGlzLmRhdGFbdGhpcy5pXS5vcF9jb2RlKTtcblxuY29uc29sZS5sb2coJ21vYmlsZSA9PT0+Jyt0aGlzLm1vYmlsZSk7XG4gICAgICAgICAgICAgICAgY29uc3Qgb2JqZWN0MT17XG4gICAgICAgICAgICAgICAgICAgIG9wQ29kZTpcIlwiLFxuICAgICAgICAgICAgICAgICAgICBtb2JpbGU6XCJcIixcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICBjb25zdCBvYmplY3QyPXtcbiAgICAgICAgICAgICAgICAgICAgb3BDb2RlOiB0aGlzLmRhdGFbdGhpcy5pXS5vcF9jb2RlLFxuICAgICAgICAgICAgICAgICAgIG1vYmlsZTp0aGlzLm1vYmlsZVxuICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaFBhcmFtcyA9IE9iamVjdC5rZXlzKG9iamVjdDEpLm1hcCgoa2V5KSA9PiB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQob2JqZWN0MltrZXldKTtcbiAgICAgICAgICAgICAgICAgIH0pLmpvaW4oJyYnKTtcbiAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICB0aGlzLnNlbmRvcGNvZGVTZXJ2aWNlLmdldERhdGEoc2VhcmNoUGFyYW1zKVxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgIHZhciB0b2FzdCA9IFRvYXN0Lm1ha2VUZXh0KFwiT1AgY29kZSBzZW50IHRvIG1vYmlsZSBudW1iZXJcIik7XG4gICAgICAgICAgICAgICAgICAgICAgdG9hc3Quc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vcC1jb2RlJyk7XG4gICAgICAgICAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBcbiAgICB9XG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuICAgIFxuXG59XG5cblxuIl19