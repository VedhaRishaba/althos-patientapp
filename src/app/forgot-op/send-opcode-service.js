"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var send_opcode_service = /** @class */ (function () {
    function send_opcode_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    }
    send_opcode_service.prototype.getData = function (SearchParams) {
        var headers = this.createRequestHeader();
        return this.http.get(this.serverUrl + "sendOPCode?" + SearchParams, { headers: headers });
    };
    send_opcode_service.prototype.createRequestHeader = function () {
        console.log("inside create");
        var headers = new http_1.HttpHeaders({
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json"
        });
        return headers;
    };
    send_opcode_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], send_opcode_service);
    return send_opcode_service;
}());
exports.send_opcode_service = send_opcode_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VuZC1vcGNvZGUtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNlbmQtb3Bjb2RlLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBTS9EO0lBSUksNkJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGMUIsY0FBUyxHQUFHLDBDQUEwQyxDQUFDO0lBRXpCLENBQUM7SUFFekMscUNBQU8sR0FBUCxVQUFRLFlBQVk7UUFDaEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFDLGFBQWEsR0FBQyxZQUFZLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUMxRixDQUFDO0lBQ08saURBQW1CLEdBQTNCO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3QixJQUFJLE9BQU8sR0FBRyxJQUFJLGtCQUFXLENBQUM7WUFDMUIsZUFBZSxFQUFFLHFFQUFxRTtZQUN0RixjQUFjLEVBQUUsa0JBQWtCO1NBQ3BDLENBQUMsQ0FBQztRQUVKLE9BQU8sT0FBTyxDQUFDO0lBQ2YsQ0FBQztJQWxCSSxtQkFBbUI7UUFIL0IsaUJBQVUsRUFFVjt5Q0FLNkIsaUJBQVU7T0FKM0IsbUJBQW1CLENBbUI5QjtJQUFELDBCQUFDO0NBQUEsQUFuQkYsSUFtQkU7QUFuQlcsa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKFxyXG5cclxuKVxyXG5leHBvcnQgY2xhc3Mgc2VuZF9vcGNvZGVfc2VydmljZXtcclxuICAgXHJcbiAgICAgIHByaXZhdGUgc2VydmVyVXJsID0gXCJodHRwczovL3BzZ2ltc3IuYWMuaW4vYWx0aG9zL2loc3Jlc3QvdjEvXCI7XHJcbiAgXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxyXG5cclxuICAgIGdldERhdGEoU2VhcmNoUGFyYW1zKSB7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB0aGlzLmNyZWF0ZVJlcXVlc3RIZWFkZXIoKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLnNlcnZlclVybCtcInNlbmRPUENvZGU/XCIrU2VhcmNoUGFyYW1zLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XHJcbiAgICB9XHJcbiAgICBwcml2YXRlIGNyZWF0ZVJlcXVlc3RIZWFkZXIoKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJpbnNpZGUgY3JlYXRlXCIpO1xyXG4gICAgICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCIgOlwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGhlYWRlcnM7XHJcbiAgICAgICAgfVxyXG4gfVxyXG5cclxuIl19