"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("tns-core-modules/ui/page");
var dialogs = require("tns-core-modules/ui/dialogs");
var otp_service_1 = require("~/app/otp/otp_service");
var OtpComponent = /** @class */ (function () {
    function OtpComponent(page, router, myservice) {
        this.page = page;
        this.router = router;
        this.myservice = myservice;
        this.otp = "";
        this.valid = true;
        this.flag = true;
        this.reg = /^\d+$/;
        this.firstTx = "";
    }
    OtpComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    OtpComponent.prototype.onTextChange = function (args) {
        var textField = args.object;
        console.log("onTextChange");
        this.firstTx = textField.text;
    };
    OtpComponent.prototype.validate = function () {
        if (this.otp.localeCompare("") != 0) {
            if (this.otp.length == 10 && this.otp.match(this.reg)) {
                dialogs.alert("Valid Mobile Number " + this.otp);
                // this.extractData();
            }
            else {
                this.flag = false;
                console.log("invalid mobile");
                //dialogs.alert("Invalid Mobile Number");
            }
        }
        else {
            this.valid = false;
            console.log("no blank");
            //dialogs.alert("Mobile Number cannot be blank");
        }
    };
    OtpComponent.prototype.returnPress = function (args) {
        console.log("Entered : " + this.otp);
        if (this.valid == false) {
            this.valid = true;
            console.log("valid : " + this.valid);
        }
        else if (this.flag == false) {
            this.flag = true;
            console.log("valid : " + this.flag);
        }
    };
    OtpComponent.prototype.getdetails = function () {
        var object1 = {
            opCode: "",
            otp: "",
        };
        var object2 = {
            opCode: "O19032823",
            otp: "2160",
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        this.myservice
            .postData(searchParams)
            .subscribe(function (response) {
            console.log('response===>' + response);
            console.log('body===>' + response.body);
        }, function (error) {
            console.log('error===>' + error.status);
        });
    };
    OtpComponent = __decorate([
        core_1.Component({
            selector: 'ns-otp',
            templateUrl: './otp.component.html',
            providers: [otp_service_1.otp_service],
            styleUrls: ['./otp.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [page_1.Page, router_1.Router, otp_service_1.otp_service])
    ], OtpComponent);
    return OtpComponent;
}());
exports.OtpComponent = OtpComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm90cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsMENBQXVDO0FBQ3ZDLGlEQUFnRDtBQUVoRCxxREFBdUQ7QUFFdkQscURBQW1EO0FBVW5EO0lBT0ksc0JBQW9CLElBQVMsRUFBUyxNQUFjLEVBQVMsU0FBcUI7UUFBOUQsU0FBSSxHQUFKLElBQUksQ0FBSztRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxjQUFTLEdBQVQsU0FBUyxDQUFZO1FBTmxGLFFBQUcsR0FBUSxFQUFFLENBQUM7UUFDZCxVQUFLLEdBQUMsSUFBSSxDQUFDO1FBQ1gsU0FBSSxHQUFDLElBQUksQ0FBQztRQUNWLFFBQUcsR0FBSyxPQUFPLENBQUM7UUFDVCxZQUFPLEdBQVcsRUFBRSxDQUFDO0lBRTBELENBQUM7SUFFdkYsK0JBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztJQUNuQyxDQUFDO0lBQ00sbUNBQVksR0FBbkIsVUFBb0IsSUFBSTtRQUN0QixJQUFJLFNBQVMsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFFRCwrQkFBUSxHQUFSO1FBRVEsSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsSUFBRSxDQUFDLEVBQ2hDO1lBQ0ssSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBRSxFQUFFLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUNsRDtnQkFDQSxPQUFPLENBQUMsS0FBSyxDQUFDLHNCQUFzQixHQUFFLElBQUksQ0FBQyxHQUFHLENBQUUsQ0FBQztnQkFDbEQsc0JBQXNCO2FBQ3BCO2lCQUVEO2dCQUNBLElBQUksQ0FBQyxJQUFJLEdBQUMsS0FBSyxDQUFDO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlCLHlDQUF5QzthQUN4QztTQUNMO2FBRUQ7WUFDSSxJQUFJLENBQUMsS0FBSyxHQUFDLEtBQUssQ0FBQztZQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hCLGlEQUFpRDtTQUNwRDtJQUdULENBQUM7SUFDRCxrQ0FBVyxHQUFYLFVBQVksSUFBSTtRQUVkLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQyxJQUFHLElBQUksQ0FBQyxLQUFLLElBQUUsS0FBSyxFQUNwQjtZQUNLLElBQUksQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDO1lBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN2QzthQUNJLElBQUcsSUFBSSxDQUFDLElBQUksSUFBRSxLQUFLLEVBQUM7WUFDdEIsSUFBSSxDQUFDLElBQUksR0FBQyxJQUFJLENBQUM7WUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7SUFFSCxDQUFDO0lBS0gsaUNBQVUsR0FBVjtRQUNFLElBQU0sT0FBTyxHQUFDO1lBQ1osTUFBTSxFQUFDLEVBQUU7WUFDVCxHQUFHLEVBQUMsRUFBRTtTQUNMLENBQUM7UUFDSCxJQUFNLE9BQU8sR0FBQztZQUNiLE1BQU0sRUFBQyxXQUFXO1lBQ25CLEdBQUcsRUFBQyxNQUFNO1NBQ1QsQ0FBQTtRQUVGLElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNsRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4RSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDYixJQUFJLENBQUMsU0FBUzthQUNILFFBQVEsQ0FBRSxZQUFZLENBQUM7YUFDdkIsU0FBUyxDQUFDLFVBQUMsUUFBUTtZQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBQyxRQUFRLENBQUMsQ0FBQztZQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxFQUFDLFVBQUMsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV4QyxDQUFDLENBQUMsQ0FBQztJQUdmLENBQUM7SUFyRlksWUFBWTtRQVB4QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUMsQ0FBQyx5QkFBVyxDQUFDO1lBQ3ZCLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ2xDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQVEyQixXQUFJLEVBQWlCLGVBQU0sRUFBbUIseUJBQVc7T0FQekUsWUFBWSxDQXlGeEI7SUFBRCxtQkFBQztDQUFBLEFBekZELElBeUZDO0FBekZZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcbmltcG9ydCB7IFRleHRGaWVsZCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3RleHQtZmllbGRcIjtcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuXG5pbXBvcnQgeyBvdHBfc2VydmljZX0gZnJvbSAnfi9hcHAvb3RwL290cF9zZXJ2aWNlJztcbmltcG9ydCAqIGFzIFRvYXN0IGZyb20gJ25hdGl2ZXNjcmlwdC10b2FzdCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLW90cCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9vdHAuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6W290cF9zZXJ2aWNlXSxcbiAgc3R5bGVVcmxzOiBbJy4vb3RwLmNvbXBvbmVudC5jc3MnXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5leHBvcnQgY2xhc3MgT3RwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBvdHA6c3RyaW5nPVwiXCI7XG4gICAgdmFsaWQ9dHJ1ZTtcbiAgICBmbGFnPXRydWU7XG4gICAgcmVnOmFueT0vXlxcZCskLztcbiAgICBwdWJsaWMgZmlyc3RUeDogc3RyaW5nID0gXCJcIjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTpQYWdlLHByaXZhdGUgcm91dGVyOiBSb3V0ZXIscHJpdmF0ZSBteXNlcnZpY2U6b3RwX3NlcnZpY2UpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcbiAgICB9XG4gICAgcHVibGljIG9uVGV4dENoYW5nZShhcmdzKSB7XG4gICAgICBsZXQgdGV4dEZpZWxkID0gPFRleHRGaWVsZD5hcmdzLm9iamVjdDtcbiAgICAgIGNvbnNvbGUubG9nKFwib25UZXh0Q2hhbmdlXCIpO1xuICAgICAgdGhpcy5maXJzdFR4ID0gdGV4dEZpZWxkLnRleHQ7XG4gIH1cblxuICB2YWxpZGF0ZSgpXG4gICAgICB7XG4gICAgICAgICAgaWYodGhpcy5vdHAubG9jYWxlQ29tcGFyZShcIlwiKSE9MClcbiAgICAgICAgICB7XG4gICAgICAgICAgICAgICBpZih0aGlzLm90cC5sZW5ndGg9PTEwICYmIHRoaXMub3RwLm1hdGNoKHRoaXMucmVnKSlcbiAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgIGRpYWxvZ3MuYWxlcnQoXCJWYWxpZCBNb2JpbGUgTnVtYmVyIFwiKyB0aGlzLm90cCApO1xuICAgICAgICAgICAgICAvLyB0aGlzLmV4dHJhY3REYXRhKCk7XG4gICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICB0aGlzLmZsYWc9ZmFsc2U7XG4gICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImludmFsaWQgbW9iaWxlXCIpO1xuICAgICAgICAgICAgICAgLy9kaWFsb2dzLmFsZXJ0KFwiSW52YWxpZCBNb2JpbGUgTnVtYmVyXCIpO1xuICAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlXG4gICAgICAgICAge1xuICAgICAgICAgICAgICB0aGlzLnZhbGlkPWZhbHNlO1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIm5vIGJsYW5rXCIpO1xuICAgICAgICAgICAgICAvL2RpYWxvZ3MuYWxlcnQoXCJNb2JpbGUgTnVtYmVyIGNhbm5vdCBiZSBibGFua1wiKTtcbiAgICAgICAgICB9XG5cblxuICB9XG4gIHJldHVyblByZXNzKGFyZ3MpXG4gIHtcbiAgICBjb25zb2xlLmxvZyhcIkVudGVyZWQgOiBcIit0aGlzLm90cCk7XG4gICAgaWYodGhpcy52YWxpZD09ZmFsc2UpXG4gICAge1xuICAgICAgICAgdGhpcy52YWxpZD10cnVlO1xuICAgICAgICAgY29uc29sZS5sb2coXCJ2YWxpZCA6IFwiK3RoaXMudmFsaWQpO1xuICAgIH1cbiAgICBlbHNlIGlmKHRoaXMuZmxhZz09ZmFsc2Upe1xuICAgICAgIHRoaXMuZmxhZz10cnVlO1xuICAgICAgIGNvbnNvbGUubG9nKFwidmFsaWQgOiBcIit0aGlzLmZsYWcpO1xuICAgIH1cblxuICB9XG5cblxuXG4gIFxuZ2V0ZGV0YWlscygpe1xuICBjb25zdCBvYmplY3QxPXtcbiAgICBvcENvZGU6XCJcIixcbiAgICBvdHA6XCJcIixcbiAgICB9O1xuICAgY29uc3Qgb2JqZWN0Mj17XG4gICAgb3BDb2RlOlwiTzE5MDMyODIzXCIsXG4gICBvdHA6XCIyMTYwXCIsXG4gICB9XG5cbiAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcbiAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XG4gIH0pLmpvaW4oJyYnKTtcbiAgdGhpcy5teXNlcnZpY2VcbiAgICAgICAgICAgIC5wb3N0RGF0YSggc2VhcmNoUGFyYW1zKVxuICAgICAgICAgICAgLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Jlc3BvbnNlPT09PicrcmVzcG9uc2UpOyAgXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdib2R5PT09PicrcmVzcG9uc2UuYm9keSk7ICAgXG4gICAgICAgICAgICB9LChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3I9PT0+JytlcnJvci5zdGF0dXMpO1xuICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0pO1xuXG5cbn1cbiAgIFxuXG5cbn1cbiJdfQ==