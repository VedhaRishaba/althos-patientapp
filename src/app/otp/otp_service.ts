import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieXSRFStrategy } from "@angular/http";
import { Observable } from "tns-core-modules/data/observable/observable";


@Injectable()
export class otp_service {
    private serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/verifyOTP?";
    private ResponseData: any;


    constructor(private http: HttpClient) { }

    postData(searchParams){
        console.log(""+searchParams);
        let options = this.createRequestOptions();
        return this.http.post(this.serverUrl,searchParams, { headers: options, observe:'response'});
    }

    private createRequestOptions() {
        let headers = new HttpHeaders({
          
            "Authorization":"Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
       
        });
        return headers;
    }
}