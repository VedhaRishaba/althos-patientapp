"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var otp_service = /** @class */ (function () {
    function otp_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/verifyOTP?";
    }
    otp_service.prototype.postData = function (searchParams) {
        console.log("" + searchParams);
        var options = this.createRequestOptions();
        return this.http.post(this.serverUrl, searchParams, { headers: options, observe: 'response' });
    };
    otp_service.prototype.createRequestOptions = function () {
        var headers = new http_1.HttpHeaders({
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
        });
        return headers;
    };
    otp_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], otp_service);
    return otp_service;
}());
exports.otp_service = otp_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RwX3NlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvdHBfc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBK0Q7QUFNL0Q7SUFLSSxxQkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUo1QixjQUFTLEdBQUcsb0RBQW9ELENBQUM7SUFJakMsQ0FBQztJQUV6Qyw4QkFBUSxHQUFSLFVBQVMsWUFBWTtRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBQyxZQUFZLENBQUMsQ0FBQztRQUM3QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUMxQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUMsWUFBWSxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUMsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRU8sMENBQW9CLEdBQTVCO1FBQ0ksSUFBSSxPQUFPLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBRTFCLGVBQWUsRUFBQyxxRUFBcUU7U0FFeEYsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQXBCUSxXQUFXO1FBRHZCLGlCQUFVLEVBQUU7eUNBTWlCLGlCQUFVO09BTDNCLFdBQVcsQ0FxQnZCO0lBQUQsa0JBQUM7Q0FBQSxBQXJCRCxJQXFCQztBQXJCWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBDb29raWVYU1JGU3RyYXRlZ3kgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlL29ic2VydmFibGVcIjtcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBvdHBfc2VydmljZSB7XHJcbiAgICBwcml2YXRlIHNlcnZlclVybCA9IFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL3ZlcmlmeU9UUD9cIjtcclxuICAgIHByaXZhdGUgUmVzcG9uc2VEYXRhOiBhbnk7XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XHJcblxyXG4gICAgcG9zdERhdGEoc2VhcmNoUGFyYW1zKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlwiK3NlYXJjaFBhcmFtcyk7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB0aGlzLmNyZWF0ZVJlcXVlc3RPcHRpb25zKCk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHRoaXMuc2VydmVyVXJsLHNlYXJjaFBhcmFtcywgeyBoZWFkZXJzOiBvcHRpb25zLCBvYnNlcnZlOidyZXNwb25zZSd9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNyZWF0ZVJlcXVlc3RPcHRpb25zKCkge1xyXG4gICAgICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgICBcIkF1dGhvcml6YXRpb25cIjpcIkJlYXJlciBVMU5uY0Zka2R6VlRVMmR3VjJSM05sOUpSRHBUVTJkd1YyUjNOVk5UWjNCWFpIYzJYMU5GUzFKRlZBPT1cIixcclxuICAgICAgIFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBoZWFkZXJzO1xyXG4gICAgfVxyXG59Il19