import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Page } from "tns-core-modules/ui/page";
import { TextField } from "tns-core-modules/ui/text-field";
import * as dialogs from "tns-core-modules/ui/dialogs";

import { otp_service} from '~/app/otp/otp_service';
import * as Toast from 'nativescript-toast';

@Component({
  selector: 'ns-otp',
  templateUrl: './otp.component.html',
  providers:[otp_service],
  styleUrls: ['./otp.component.css'],
  moduleId: module.id,
})
export class OtpComponent implements OnInit {
    otp:string="";
    valid=true;
    flag=true;
    reg:any=/^\d+$/;
    public firstTx: string = "";

    constructor(private page:Page,private router: Router,private myservice:otp_service) { }

    ngOnInit() {
      this.page.actionBarHidden = true;
    }
    public onTextChange(args) {
      let textField = <TextField>args.object;
      console.log("onTextChange");
      this.firstTx = textField.text;
  }

  validate()
      {
          if(this.otp.localeCompare("")!=0)
          {
               if(this.otp.length==10 && this.otp.match(this.reg))
               {
               dialogs.alert("Valid Mobile Number "+ this.otp );
              // this.extractData();
               }
               else
               {
               this.flag=false;
               console.log("invalid mobile");
               //dialogs.alert("Invalid Mobile Number");
               }
          }
          else
          {
              this.valid=false;
              console.log("no blank");
              //dialogs.alert("Mobile Number cannot be blank");
          }


  }
  returnPress(args)
  {
    console.log("Entered : "+this.otp);
    if(this.valid==false)
    {
         this.valid=true;
         console.log("valid : "+this.valid);
    }
    else if(this.flag==false){
       this.flag=true;
       console.log("valid : "+this.flag);
    }

  }



  
getdetails(){
  const object1={
    opCode:"",
    otp:"",
    };
   const object2={
    opCode:"O19032823",
   otp:"2160",
   }

  const searchParams = Object.keys(object1).map((key) => {
  return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
  }).join('&');
  this.myservice
            .postData( searchParams)
            .subscribe((response) => {
              console.log('response===>'+response);  
              console.log('body===>'+response.body);   
            },(error) => {
              console.log('error===>'+error.status);
              
            });


}
   


}
