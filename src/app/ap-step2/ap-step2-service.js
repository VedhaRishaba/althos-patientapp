"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var ap_step2_service = /** @class */ (function () {
    function ap_step2_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    }
    ap_step2_service.prototype.getData = function () {
        var headers = this.createRequestHeader();
        return this.http.get(this.serverUrl + "getAllDepartments?", {
            headers: headers
        });
    };
    ap_step2_service.prototype.createRequestHeader = function () {
        console.log("inside create");
        var headers = new http_1.HttpHeaders({
            Authorization: "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json"
        });
        return headers;
    };
    ap_step2_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ap_step2_service);
    return ap_step2_service;
}());
exports.ap_step2_service = ap_step2_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXAtc3RlcDItc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwLXN0ZXAyLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBRy9EO0lBR0ksMEJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGNUIsY0FBUyxHQUFHLDBDQUEwQyxDQUFDO0lBRXhCLENBQUM7SUFFeEMsa0NBQU8sR0FBUDtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBb0IsRUFBRTtZQUN4RCxPQUFPLEVBQUUsT0FBTztTQUNuQixDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ08sOENBQW1CLEdBQTNCO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3QixJQUFJLE9BQU8sR0FBRyxJQUFJLGtCQUFXLENBQUM7WUFDMUIsYUFBYSxFQUNULHFFQUFxRTtZQUN6RSxjQUFjLEVBQUUsa0JBQWtCO1NBQ3JDLENBQUMsQ0FBQztRQUVILE9BQU8sT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFyQlEsZ0JBQWdCO1FBRDVCLGlCQUFVLEVBQUU7eUNBSWlCLGlCQUFVO09BSDNCLGdCQUFnQixDQXNCNUI7SUFBRCx1QkFBQztDQUFBLEFBdEJELElBc0JDO0FBdEJZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBhcF9zdGVwMl9zZXJ2aWNlIHtcbiAgICBwcml2YXRlIHNlcnZlclVybCA9IFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL1wiO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7fVxuXG4gICAgZ2V0RGF0YSgpIHtcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB0aGlzLmNyZWF0ZVJlcXVlc3RIZWFkZXIoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy5zZXJ2ZXJVcmwgKyBcImdldEFsbERlcGFydG1lbnRzP1wiLCB7XG4gICAgICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBwcml2YXRlIGNyZWF0ZVJlcXVlc3RIZWFkZXIoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiaW5zaWRlIGNyZWF0ZVwiKTtcbiAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgICAgICAgQXV0aG9yaXphdGlvbjpcbiAgICAgICAgICAgICAgICBcIkJlYXJlciBVMU5uY0Zka2R6VlRVMmR3VjJSM05sOUpSRHBUVTJkd1YyUjNOVk5UWjNCWFpIYzJYMU5GUzFKRlZBPT1cIixcbiAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBoZWFkZXJzO1xuICAgICAgICBcbiAgICB9XG59XG4iXX0=