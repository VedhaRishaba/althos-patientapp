"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var appSettings = require("tns-core-modules/application-settings");
var ap_doctor_service = /** @class */ (function () {
    function ap_doctor_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    }
    ap_doctor_service.prototype.getData = function (sid) {
        this.appointment_date = appSettings.getString("appointment_date");
        this.dept_no = JSON.parse(appSettings.getString("dept_no"));
        this.sid = sid;
        var headers = this.createRequestHeader();
        return this.http.get("https://psgimsr.ac.in/althos/ihsrest/v1/getAllDoctors?deptno=" + this.dept_no + "&app_date=" + this.appointment_date, { headers: headers });
    };
    ap_doctor_service.prototype.createRequestHeader = function () {
        console.log("inside create");
        var headers = new http_1.HttpHeaders({
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json",
            "Set-Cookie": this.sid
        });
        return headers;
    };
    ap_doctor_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ap_doctor_service);
    return ap_doctor_service;
}());
exports.ap_doctor_service = ap_doctor_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXAtZG9jdG9yLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhcC1kb2N0b3Itc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBK0Q7QUFHL0QsbUVBQXFFO0FBS3JFO0lBS0ksMkJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFKNUIsY0FBUyxHQUFHLDBDQUEwQyxDQUFDO0lBSXZCLENBQUM7SUFFekMsbUNBQU8sR0FBUCxVQUFTLEdBQUc7UUFFUixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFFL0QsSUFBSSxDQUFDLEdBQUcsR0FBRSxHQUFHLENBQUM7UUFDWCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUN6QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLCtEQUErRCxHQUFDLElBQUksQ0FBQyxPQUFPLEdBQUMsWUFBWSxHQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ2hLLENBQUM7SUFDTywrQ0FBbUIsR0FBM0I7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdCLElBQUksT0FBTyxHQUFHLElBQUksa0JBQVcsQ0FBQztZQUMxQixlQUFlLEVBQUUscUVBQXFFO1lBQ3RGLGNBQWMsRUFBRSxrQkFBa0I7WUFDbEMsWUFBWSxFQUFFLElBQUksQ0FBQyxHQUFHO1NBQ3pCLENBQUMsQ0FBQztRQUVILE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUF6QlEsaUJBQWlCO1FBSjdCLGlCQUFVLEVBRVY7eUNBTzZCLGlCQUFVO09BTDNCLGlCQUFpQixDQTBCN0I7SUFBRCx3QkFBQztDQUFBLEFBMUJELElBMEJDO0FBMUJZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlXCI7XHJcblxyXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xyXG5ASW5qZWN0YWJsZShcclxuXHJcbilcclxuXHJcbmV4cG9ydCBjbGFzcyBhcF9kb2N0b3Jfc2VydmljZSB7XHJcbiAgICBwcml2YXRlIHNlcnZlclVybCA9IFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL1wiO1xyXG4gICAgYXBwb2ludG1lbnRfZGF0ZTtcclxuICAgIGRlcHRfbm87XHJcbiAgICBzaWQ7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxyXG5cclxuICAgIGdldERhdGEoIHNpZCkge1xyXG5cclxuICAgICAgICB0aGlzLmFwcG9pbnRtZW50X2RhdGUgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJhcHBvaW50bWVudF9kYXRlXCIpO1xyXG4gICAgICAgIHRoaXMuZGVwdF9ubyA9IEpTT04ucGFyc2UoYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZGVwdF9ub1wiKSk7XHJcblxyXG4gICAgIHRoaXMuc2lkID1zaWQ7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB0aGlzLmNyZWF0ZVJlcXVlc3RIZWFkZXIoKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldChcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9nZXRBbGxEb2N0b3JzP2RlcHRubz1cIit0aGlzLmRlcHRfbm8rXCImYXBwX2RhdGU9XCIrdGhpcy5hcHBvaW50bWVudF9kYXRlLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XHJcbiAgICB9XHJcbiAgICBwcml2YXRlIGNyZWF0ZVJlcXVlc3RIZWFkZXIoKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJpbnNpZGUgY3JlYXRlXCIpO1xyXG4gICAgICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6IFwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICAgICAgXCJTZXQtQ29va2llXCI6IHRoaXMuc2lkXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBoZWFkZXJzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==