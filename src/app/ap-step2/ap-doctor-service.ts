import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "tns-core-modules/ui/page/page";

import * as appSettings from "tns-core-modules/application-settings";
@Injectable(

)

export class ap_doctor_service {
    private serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    appointment_date;
    dept_no;
    sid;
    constructor(private http: HttpClient) { }

    getData( sid) {

        this.appointment_date = appSettings.getString("appointment_date");
        this.dept_no = JSON.parse(appSettings.getString("dept_no"));

     this.sid =sid;
        let headers = this.createRequestHeader();
        return this.http.get("https://psgimsr.ac.in/althos/ihsrest/v1/getAllDoctors?deptno="+this.dept_no+"&app_date="+this.appointment_date, { headers: headers });
    }
    private createRequestHeader() {
        console.log("inside create");
        let headers = new HttpHeaders({
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json",
            "Set-Cookie": this.sid
        });

        return headers;
    }
}
