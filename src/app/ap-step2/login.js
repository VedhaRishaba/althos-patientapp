"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var Login = /** @class */ (function () {
    function Login(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/login";
    }
    Login.prototype.postData = function (data) {
        var options = this.createRequestOptions();
        return this.http.post(this.serverUrl, data, { headers: options, observe: 'response', withCredentials: true });
    };
    Login.prototype.createRequestOptions = function () {
        var headers = new http_1.HttpHeaders({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA=="
        });
        return headers;
    };
    Login = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], Login);
    return Login;
}());
exports.Login = Login;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsb2dpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBK0Q7QUFNL0Q7SUFLSSxlQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBSjVCLGNBQVMsR0FBRywrQ0FBK0MsQ0FBQztJQUk1QixDQUFDO0lBRXpDLHdCQUFRLEdBQVIsVUFBUyxJQUFTO1FBRWxCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRyxJQUFJLEVBQUcsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUMsZUFBZSxFQUFDLElBQUksRUFBQyxDQUFDLENBQUM7SUFFakgsQ0FBQztJQUVPLG9DQUFvQixHQUE1QjtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksa0JBQVcsQ0FBQztZQUMxQixjQUFjLEVBQUUsbUNBQW1DO1lBQ25ELGVBQWUsRUFBQyxxRUFBcUU7U0FDeEYsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQXBCUSxLQUFLO1FBRGpCLGlCQUFVLEVBQUU7eUNBTWlCLGlCQUFVO09BTDNCLEtBQUssQ0FxQmpCO0lBQUQsWUFBQztDQUFBLEFBckJELElBcUJDO0FBckJZLHNCQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuaW1wb3J0IHsgQ29va2llWFNSRlN0cmF0ZWd5IH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUvb2JzZXJ2YWJsZVwiO1xuaW1wb3J0IHsgcGFzc3dvcmRtb2RlbH0gZnJvbSBcIn4vYXBwL3Bhc3N3b3JkL3Bhc3N3b3JkLW1vZGVsXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb2dpbiB7XG4gICAgcHJpdmF0ZSBzZXJ2ZXJVcmwgPSBcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9sb2dpblwiO1xuICAgIHByaXZhdGUgUmVzcG9uc2VEYXRhOiBhbnk7XG5cblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XG5cbiAgICBwb3N0RGF0YShkYXRhOiBhbnkpe1xuXG4gICAgbGV0IG9wdGlvbnMgPSB0aGlzLmNyZWF0ZVJlcXVlc3RPcHRpb25zKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLnNlcnZlclVybCwgIGRhdGEgLCB7IGhlYWRlcnM6IG9wdGlvbnMsIG9ic2VydmU6ICdyZXNwb25zZScsd2l0aENyZWRlbnRpYWxzOnRydWV9KTtcbiAgICAgICAgXG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVSZXF1ZXN0T3B0aW9ucygpIHtcbiAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWRcIixcbiAgICAgICAgICAgIFwiQXV0aG9yaXphdGlvblwiOlwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gaGVhZGVycztcbiAgICB9XG59Il19