"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var appSettings = require("tns-core-modules/application-settings");
var appointment_component_1 = require("../appointment/appointment.component");
var Toast = require("nativescript-toast");
var Connectivity = require("tns-core-modules/connectivity");
var ap_doctor_service_1 = require("./ap-doctor-service");
var login_1 = require("./login");
var ApStep2Component = /** @class */ (function () {
    function ApStep2Component(appointmentComponent, doctorService, loginService) {
        this.appointmentComponent = appointmentComponent;
        this.doctorService = doctorService;
        this.loginService = loginService;
        this.data = [];
        this.appointment_date = appSettings.getString("appointment_date");
        this.department_data = JSON.parse(appSettings.getString("department_data"));
        this.sid = "";
        this.op = "O18013660";
        this.password = "nxC0le9gP2L2mqEUPIIGjw==";
    }
    ApStep2Component.prototype.ngOnInit = function () {
        console.log("ngoninit");
        this.GetDepartmentData();
    };
    ApStep2Component.prototype.onItemTap = function (args) {
        console.log("You tapped: " + this.data[args.index].dept_no);
        appSettings.setString("dept_no", this.data[args.index].dept_no.toString());
        this.logoutactivity();
        this.appointmentComponent.step3();
    };
    ApStep2Component.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text == "true") {
                console.log("Logout success");
                _this.login();
                //this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    ApStep2Component.prototype.GetDepartmentData = function () {
        for (this.i = 0; this.i < Object.keys(this.department_data).length; this.i++) {
            this.data.push(this.department_data[this.i]);
        }
    };
    ApStep2Component.prototype.login = function () {
        var _this = this;
        var object1 = {
            op_code: "",
            password: "",
        };
        var object2 = {
            op_code: this.op,
            password: this.password,
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        this.loginService
            .postData(searchParams)
            .subscribe(function (response) {
            if (response.status == 200) {
                _this.sid = response.headers.get('Set-cookie');
                console.log("cookie" + _this.sid);
                appSettings.setString("cookie", _this.sid);
                _this.extractDoctor();
                // this.router.navigate(['/home'], { queryParams: { Sessionid:this.sid } });
                //this.router.navigate(['/home']);
            }
            else {
                var toast = Toast.makeText("Invalid password");
                toast.show();
            }
        }, function (error) {
            console.log(error);
            if (error.status == 400) {
                // this.router.navigateByUrl('/home');
                var toast = Toast.makeText("This account is already logged in another device. Please log out and try again later");
                toast.show();
            }
            else {
                _this.connectionType = _this.connectionToString(Connectivity.getConnectionType());
                if (_this.connectionType == "0") {
                    var toast = Toast.makeText("Cannot connect to network. Try again later!");
                    toast.show();
                }
                else {
                    var toast = Toast.makeText("Try Again");
                    toast.show();
                }
            }
        });
    };
    ApStep2Component.prototype.extractDoctor = function () {
        var _this = this;
        this.doctorService.getData(this.sid)
            .subscribe(function (result) {
            console.log(result);
            console.log(Object.keys(result).length);
            _this.doctorData = result;
            appSettings.setString("doctor_data", JSON.stringify(_this.doctorData));
        }, function (error) {
            console.log(error);
        });
    };
    ApStep2Component.prototype.connectionToString = function (connectionType) {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "0";
            case Connectivity.connectionType.wifi:
                return "1";
            case Connectivity.connectionType.mobile:
                return "1";
            default:
                return "0";
        }
    };
    ApStep2Component = __decorate([
        core_1.Component({
            selector: "ns-ap-step2",
            templateUrl: "./ap-step2.component.html",
            providers: [ap_doctor_service_1.ap_doctor_service, login_1.Login],
            styleUrls: ["./ap-step2.component.css"],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [appointment_component_1.AppointmentComponent, ap_doctor_service_1.ap_doctor_service, login_1.Login])
    ], ApStep2Component);
    return ApStep2Component;
}());
exports.ApStep2Component = ApStep2Component;
/*
*
*    Remaining Work
*    ~~~~~~~~~~~~~~
*
*    Set ttf image to relevant Departments
*
*
*/
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXAtc3RlcDIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXAtc3RlcDIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELG1FQUFxRTtBQUNyRSw4RUFBNEU7QUFFNUUsMENBQTRDO0FBRTVDLDREQUE4RDtBQUM5RCx5REFBd0Q7QUFDeEQsaUNBQWdDO0FBU2hDO0lBZUksMEJBQW9CLG9CQUEwQyxFQUFTLGFBQWdDLEVBQVMsWUFBbUI7UUFBL0cseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUFTLGtCQUFhLEdBQWIsYUFBYSxDQUFtQjtRQUFTLGlCQUFZLEdBQVosWUFBWSxDQUFPO1FBZG5JLFNBQUksR0FBRyxFQUFFLENBQUM7UUFFVixxQkFBZ0IsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDN0Qsb0JBQWUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1FBSXpFLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFFZixPQUFFLEdBQUcsV0FBVyxDQUFDO1FBQ2pCLGFBQVEsR0FBRywwQkFBMEIsQ0FBQztJQUlnRyxDQUFDO0lBRXZJLG1DQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFDRCxvQ0FBUyxHQUFULFVBQVUsSUFBSTtRQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzVELFdBQVcsQ0FBQyxTQUFTLENBQ2pCLFNBQVMsRUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQzNDLENBQUM7UUFFRixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFHdEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFHQyx5Q0FBYyxHQUFkO1FBQUEsaUJBd0NDO1FBdkNDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBQ0YsSUFBTSxPQUFPLEdBQUc7WUFFZCxPQUFPLEVBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7U0FDM0MsQ0FBQTtRQUNELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNoRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFYixLQUFLLENBQUMsZ0RBQWdELEVBQUU7WUFDdEQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsY0FBYyxFQUFFLG1DQUFtQyxFQUFFLGVBQWUsRUFBRSxxRUFBcUU7Z0JBQzNJLFNBQVMsRUFBRSxPQUFPO2FBQ25CO1lBQ0QsSUFBSSxFQUFFLFlBQVk7U0FDbkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBUixDQUFRLENBQUM7YUFDckIsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxJQUFJLE1BQU0sRUFBRTtnQkFHaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUc5QixLQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBRWYsd0NBQXdDO2FBQ3pDO2lCQUNJO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBR0gsNENBQWlCLEdBQWpCO1FBQ0ksS0FDSSxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFDVixJQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLE1BQU0sRUFDakQsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUNWO1lBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNoRDtJQUNMLENBQUM7SUFJRCxnQ0FBSyxHQUFMO1FBQUEsaUJBaUVHO1FBaEVDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7WUFDWCxRQUFRLEVBQUUsRUFBRTtTQUNiLENBQUM7UUFDRixJQUFNLE9BQU8sR0FBRztZQUNkLE9BQU8sRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNoQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDeEIsQ0FBQTtRQUdELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNoRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDYixJQUFJLENBQUMsWUFBWTthQUNkLFFBQVEsQ0FBQyxZQUFZLENBQUM7YUFDdEIsU0FBUyxDQUFDLFVBQUMsUUFBUTtZQUNsQixJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO2dCQUUxQixLQUFJLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUU5QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2pDLFdBQVcsQ0FBQyxTQUFTLENBQ25CLFFBQVEsRUFBRSxLQUFJLENBQUMsR0FBRyxDQUNuQixDQUFDO2dCQUVGLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsNEVBQTRFO2dCQUM1RSxrQ0FBa0M7YUFDbkM7aUJBQU07Z0JBQ0wsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUMvQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDZDtRQUNILENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7Z0JBRXZCLHNDQUFzQztnQkFDdEMsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxzRkFBc0YsQ0FBQyxDQUFDO2dCQUNuSCxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7YUFFZDtpQkFBTTtnQkFJTCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FDM0MsWUFBWSxDQUFDLGlCQUFpQixFQUFFLENBQ25DLENBQUM7Z0JBQ0YsSUFBSSxLQUFJLENBQUMsY0FBYyxJQUFJLEdBQUcsRUFBRTtvQkFDNUIsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FDdEIsNkNBQTZDLENBQ2hELENBQUM7b0JBQ0YsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO2lCQUNoQjtxQkFBTztvQkFFTixJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN4QyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7aUJBQ2Q7YUFJQTtRQUNILENBQUMsQ0FBQyxDQUFDO0lBR1AsQ0FBQztJQUlILHdDQUFhLEdBQWI7UUFBQSxpQkFtQkc7UUFoQkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQzthQUNqQyxTQUFTLENBQUMsVUFBQyxNQUFNO1lBRWhCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRXhDLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO1lBRXpCLFdBQVcsQ0FBQyxTQUFTLENBQ2pCLGFBQWEsRUFDYixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FDbEMsQ0FBQztRQUVKLENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLDZDQUFrQixHQUF6QixVQUEwQixjQUFzQjtRQUM5QyxRQUFRLGNBQWMsRUFBRTtZQUN0QixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDbkMsT0FBTyxHQUFHLENBQUM7WUFDYixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDbkMsT0FBTyxHQUFHLENBQUM7WUFDYixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsTUFBTTtnQkFDckMsT0FBTyxHQUFHLENBQUM7WUFDYjtnQkFDRSxPQUFPLEdBQUcsQ0FBQztTQUNkO0lBQ0gsQ0FBQztJQTlMTSxnQkFBZ0I7UUFSNUIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFdBQVcsRUFBRSwyQkFBMkI7WUFFMUMsU0FBUyxFQUFDLENBQUMscUNBQWlCLEVBQUMsYUFBSyxDQUFDO1lBQ2pDLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO1lBQ3ZDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUN0QixDQUFDO3lDQWdCNEMsNENBQW9CLEVBQXdCLHFDQUFpQixFQUF1QixhQUFLO09BZjFILGdCQUFnQixDQStMNUI7SUFBRCx1QkFBQztDQUFBLEFBL0xELElBK0xDO0FBL0xZLDRDQUFnQjtBQWlNN0I7Ozs7Ozs7O0VBUUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xuaW1wb3J0IHsgQXBwb2ludG1lbnRDb21wb25lbnQgfSBmcm9tIFwiLi4vYXBwb2ludG1lbnQvYXBwb2ludG1lbnQuY29tcG9uZW50XCI7XG5cbmltcG9ydCAqIGFzIFRvYXN0IGZyb20gJ25hdGl2ZXNjcmlwdC10b2FzdCc7XG5cbmltcG9ydCAqIGFzIENvbm5lY3Rpdml0eSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9jb25uZWN0aXZpdHlcIjtcbmltcG9ydCB7IGFwX2RvY3Rvcl9zZXJ2aWNlIH0gZnJvbSAnLi9hcC1kb2N0b3Itc2VydmljZSc7XG5pbXBvcnQgeyBMb2dpbiB9IGZyb20gJy4vbG9naW4nO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtYXAtc3RlcDJcIixcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2FwLXN0ZXAyLmNvbXBvbmVudC5odG1sXCIsXG5cbiAgcHJvdmlkZXJzOlthcF9kb2N0b3Jfc2VydmljZSxMb2dpbl0sXG4gICAgc3R5bGVVcmxzOiBbXCIuL2FwLXN0ZXAyLmNvbXBvbmVudC5jc3NcIl0sXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZFxufSlcbmV4cG9ydCBjbGFzcyBBcFN0ZXAyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBkYXRhID0gW107XG4gICAgaTogbnVtYmVyO1xuICAgIGFwcG9pbnRtZW50X2RhdGUgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJhcHBvaW50bWVudF9kYXRlXCIpO1xuICAgIGRlcGFydG1lbnRfZGF0YSA9IEpTT04ucGFyc2UoYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZGVwYXJ0bWVudF9kYXRhXCIpKTtcbiAgICBkb2N0b3JEYXRhO1xuXG5cbiAgc2lkOiBzdHJpbmcgPSBcIlwiO1xuXG4gICAgb3AgPSBcIk8xODAxMzY2MFwiO1xuICAgIHBhc3N3b3JkID0gXCJueEMwbGU5Z1AyTDJtcUVVUElJR2p3PT1cIjtcblxuXG4gICAgcHVibGljIGNvbm5lY3Rpb25UeXBlOiBzdHJpbmc7XG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcHBvaW50bWVudENvbXBvbmVudDogQXBwb2ludG1lbnRDb21wb25lbnQscHJpdmF0ZSBkb2N0b3JTZXJ2aWNlOiBhcF9kb2N0b3Jfc2VydmljZSxwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW4pIHt9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJuZ29uaW5pdFwiKTtcbiAgICAgICAgdGhpcy5HZXREZXBhcnRtZW50RGF0YSgpO1xuICAgIH1cbiAgICBvbkl0ZW1UYXAoYXJncykge1xuICAgICAgICBjb25zb2xlLmxvZyhcIllvdSB0YXBwZWQ6IFwiICsgdGhpcy5kYXRhW2FyZ3MuaW5kZXhdLmRlcHRfbm8pO1xuICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXG4gICAgICAgICAgICBcImRlcHRfbm9cIixcbiAgICAgICAgICAgIHRoaXMuZGF0YVthcmdzLmluZGV4XS5kZXB0X25vLnRvU3RyaW5nKClcbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XG5cblxuICAgICAgICB0aGlzLmFwcG9pbnRtZW50Q29tcG9uZW50LnN0ZXAzKCk7XG4gICAgfVxuXG5cbiAgICAgIGxvZ291dGFjdGl2aXR5KCkge1xuICAgICAgICBjb25zdCBvYmplY3QxID0ge1xuICAgICAgICAgIG9wX2NvZGU6IFwiXCJcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgb2JqZWN0MiA9IHtcblxuICAgICAgICAgIG9wX2NvZGU6ICBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJvcC1jb2RlXCIpXG4gICAgICAgIH1cbiAgICAgICAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcbiAgICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQob2JqZWN0MltrZXldKTtcbiAgICAgICAgfSkuam9pbignJicpO1xuXG4gICAgICAgIGZldGNoKFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL2xvZ291dFwiLCB7XG4gICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiLCBcIkF1dGhvcml6YXRpb25cIjogXCJCZWFyZXIgVTFObmNGZGtkelZUVTJkd1YyUjNObDlKUkRwVFUyZHdWMlIzTlZOVFozQlhaSGMyWDFORlMxSkZWQT09XCIsXG4gICAgICAgICAgICBcImNoYXJzZXRcIjogXCJ1dGYtOFwiXG4gICAgICAgICAgfSxcbiAgICAgICAgICBib2R5OiBzZWFyY2hQYXJhbXMsXG4gICAgICAgIH0pLnRoZW4oKHIpID0+IHIudGV4dCgpKVxuICAgICAgICAgIC50aGVuKCh0ZXh0KSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIm1lc3NhZ2VcIiArIHRleHQpO1xuICAgICAgICAgICAgaWYgKHRleHQgPT0gXCJ0cnVlXCIpIHtcblxuXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJMb2dvdXQgc3VjY2Vzc1wiKTtcblxuXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dpbigpO1xuXG4gICAgICAgICAgICAgIC8vdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL29wLWNvZGUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImZhaWxlZCB0byBsb2dvdXRcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSkuY2F0Y2goKGUpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3I6IFwiKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICB9XG5cblxuICAgIEdldERlcGFydG1lbnREYXRhKCkge1xuICAgICAgICBmb3IgKFxuICAgICAgICAgICAgdGhpcy5pID0gMDtcbiAgICAgICAgICAgIHRoaXMuaSA8IE9iamVjdC5rZXlzKHRoaXMuZGVwYXJ0bWVudF9kYXRhKS5sZW5ndGg7XG4gICAgICAgICAgICB0aGlzLmkrK1xuICAgICAgICApIHtcbiAgICAgICAgICAgIHRoaXMuZGF0YS5wdXNoKHRoaXMuZGVwYXJ0bWVudF9kYXRhW3RoaXMuaV0pO1xuICAgICAgICB9XG4gICAgfVxuXG5cblxuICAgIGxvZ2luKCkge1xuICAgICAgICBjb25zdCBvYmplY3QxID0ge1xuICAgICAgICAgIG9wX2NvZGU6IFwiXCIsXG4gICAgICAgICAgcGFzc3dvcmQ6IFwiXCIsXG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IG9iamVjdDIgPSB7XG4gICAgICAgICAgb3BfY29kZTogdGhpcy5vcCxcbiAgICAgICAgICBwYXNzd29yZDogdGhpcy5wYXNzd29yZCxcbiAgICAgICAgfVxuXG5cbiAgICAgICAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcbiAgICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQob2JqZWN0MltrZXldKTtcbiAgICAgICAgfSkuam9pbignJicpO1xuICAgICAgICB0aGlzLmxvZ2luU2VydmljZVxuICAgICAgICAgIC5wb3N0RGF0YShzZWFyY2hQYXJhbXMpXG4gICAgICAgICAgLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT0gMjAwKSB7XG5cbiAgICAgICAgICAgICAgdGhpcy5zaWQgPSByZXNwb25zZS5oZWFkZXJzLmdldCgnU2V0LWNvb2tpZScpO1xuXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiY29va2llXCIgKyB0aGlzLnNpZCk7XG4gICAgICAgICAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcbiAgICAgICAgICAgICAgICBcImNvb2tpZVwiLCB0aGlzLnNpZFxuICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgIHRoaXMuZXh0cmFjdERvY3RvcigpO1xuICAgICAgICAgICAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9ob21lJ10sIHsgcXVlcnlQYXJhbXM6IHsgU2Vzc2lvbmlkOnRoaXMuc2lkIH0gfSk7XG4gICAgICAgICAgICAgIC8vdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvaG9tZSddKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHZhciB0b2FzdCA9IFRvYXN0Lm1ha2VUZXh0KFwiSW52YWxpZCBwYXNzd29yZFwiKTtcbiAgICAgICAgICAgICAgdG9hc3Quc2hvdygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgICAgaWYgKGVycm9yLnN0YXR1cyA9PSA0MDApIHtcblxuICAgICAgICAgICAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvaG9tZScpO1xuICAgICAgICAgICAgICB2YXIgdG9hc3QgPSBUb2FzdC5tYWtlVGV4dChcIlRoaXMgYWNjb3VudCBpcyBhbHJlYWR5IGxvZ2dlZCBpbiBhbm90aGVyIGRldmljZS4gUGxlYXNlIGxvZyBvdXQgYW5kIHRyeSBhZ2FpbiBsYXRlclwiKTtcbiAgICAgICAgICAgICAgdG9hc3Quc2hvdygpO1xuXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG5cblxuICAgICAgICAgICAgICB0aGlzLmNvbm5lY3Rpb25UeXBlID0gdGhpcy5jb25uZWN0aW9uVG9TdHJpbmcoXG4gICAgICAgICAgICAgICAgQ29ubmVjdGl2aXR5LmdldENvbm5lY3Rpb25UeXBlKClcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBpZiAodGhpcy5jb25uZWN0aW9uVHlwZSA9PSBcIjBcIikge1xuICAgICAgICAgICAgICAgIHZhciB0b2FzdCA9IFRvYXN0Lm1ha2VUZXh0KFxuICAgICAgICAgICAgICAgICAgICBcIkNhbm5vdCBjb25uZWN0IHRvIG5ldHdvcmsuIFRyeSBhZ2FpbiBsYXRlciFcIlxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgdG9hc3Quc2hvdygpO1xuICAgICAgICAgICAgfSBlbHNlICB7XG5cbiAgICAgICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJUcnkgQWdhaW5cIik7XG4gICAgICAgICAgICAgIHRvYXN0LnNob3coKTtcbiAgICAgICAgICAgIH1cblxuXG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcblxuXG4gICAgICB9XG5cblxuXG4gICAgZXh0cmFjdERvY3RvcigpIHtcblxuXG4gICAgICAgIHRoaXMuZG9jdG9yU2VydmljZS5nZXREYXRhKHRoaXMuc2lkKVxuICAgICAgICAgIC5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coT2JqZWN0LmtleXMocmVzdWx0KS5sZW5ndGgpO1xuXG4gICAgICAgICAgICB0aGlzLmRvY3RvckRhdGEgPSByZXN1bHQ7XG5cbiAgICAgICAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcbiAgICAgICAgICAgICAgICBcImRvY3Rvcl9kYXRhXCIsXG4gICAgICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkodGhpcy5kb2N0b3JEYXRhKVxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgcHVibGljIGNvbm5lY3Rpb25Ub1N0cmluZyhjb25uZWN0aW9uVHlwZTogbnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgc3dpdGNoIChjb25uZWN0aW9uVHlwZSkge1xuICAgICAgICAgIGNhc2UgQ29ubmVjdGl2aXR5LmNvbm5lY3Rpb25UeXBlLm5vbmU6XG4gICAgICAgICAgICByZXR1cm4gXCIwXCI7XG4gICAgICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUud2lmaTpcbiAgICAgICAgICAgIHJldHVybiBcIjFcIjtcbiAgICAgICAgICBjYXNlIENvbm5lY3Rpdml0eS5jb25uZWN0aW9uVHlwZS5tb2JpbGU6XG4gICAgICAgICAgICByZXR1cm4gXCIxXCI7XG4gICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHJldHVybiBcIjBcIjtcbiAgICAgICAgfVxuICAgICAgfVxufVxuXG4vKlxuKlxuKiAgICBSZW1haW5pbmcgV29ya1xuKiAgICB+fn5+fn5+fn5+fn5+flxuKlxuKiAgICBTZXQgdHRmIGltYWdlIHRvIHJlbGV2YW50IERlcGFydG1lbnRzXG4qXG4qXG4qL1xuIl19