import { Component, OnInit } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import { AppointmentComponent } from "../appointment/appointment.component";

import * as Toast from 'nativescript-toast';

import * as Connectivity from "tns-core-modules/connectivity";
import { ap_doctor_service } from './ap-doctor-service';
import { Login } from './login';
@Component({
    selector: "ns-ap-step2",
    templateUrl: "./ap-step2.component.html",

  providers:[ap_doctor_service,Login],
    styleUrls: ["./ap-step2.component.css"],
    moduleId: module.id
})
export class ApStep2Component implements OnInit {
    data = [];
    i: number;
    appointment_date = appSettings.getString("appointment_date");
    department_data = JSON.parse(appSettings.getString("department_data"));
    doctorData;


  sid: string = "";

    op = "O18013660";
    password = "nxC0le9gP2L2mqEUPIIGjw==";


    public connectionType: string;
    constructor(private appointmentComponent: AppointmentComponent,private doctorService: ap_doctor_service,private loginService: Login) {}

    ngOnInit() {
        console.log("ngoninit");
        this.GetDepartmentData();
    }
    onItemTap(args) {
        console.log("You tapped: " + this.data[args.index].dept_no);
        appSettings.setString(
            "dept_no",
            this.data[args.index].dept_no.toString()
        );

        this.logoutactivity();


        this.appointmentComponent.step3();
    }


      logoutactivity() {
        const object1 = {
          op_code: ""
        };
        const object2 = {

          op_code:  appSettings.getString("op-code")
        }
        const searchParams = Object.keys(object1).map((key) => {
          return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');

        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "charset": "utf-8"
          },
          body: searchParams,
        }).then((r) => r.text())
          .then((text) => {
            console.log("message" + text);
            if (text == "true") {


                console.log("Logout success");


                this.login();

              //this.router.navigateByUrl('/op-code');
            }
            else {
              console.log("failed to logout");
            }
          }).catch((e) => {
            console.log("Error: ");
            console.log(e);
          });

      }


    GetDepartmentData() {
        for (
            this.i = 0;
            this.i < Object.keys(this.department_data).length;
            this.i++
        ) {
            this.data.push(this.department_data[this.i]);
        }
    }



    login() {
        const object1 = {
          op_code: "",
          password: "",
        };
        const object2 = {
          op_code: this.op,
          password: this.password,
        }


        const searchParams = Object.keys(object1).map((key) => {
          return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        this.loginService
          .postData(searchParams)
          .subscribe((response) => {
            if (response.status == 200) {

              this.sid = response.headers.get('Set-cookie');

              console.log("cookie" + this.sid);
              appSettings.setString(
                "cookie", this.sid
              );

              this.extractDoctor();
              // this.router.navigate(['/home'], { queryParams: { Sessionid:this.sid } });
              //this.router.navigate(['/home']);
            } else {
              var toast = Toast.makeText("Invalid password");
              toast.show();
            }
          }, (error) => {
            console.log(error);
            if (error.status == 400) {

              // this.router.navigateByUrl('/home');
              var toast = Toast.makeText("This account is already logged in another device. Please log out and try again later");
              toast.show();

            } else {



              this.connectionType = this.connectionToString(
                Connectivity.getConnectionType()
            );
            if (this.connectionType == "0") {
                var toast = Toast.makeText(
                    "Cannot connect to network. Try again later!"
                );
                toast.show();
            } else  {

              var toast = Toast.makeText("Try Again");
              toast.show();
            }



            }
          });


      }



    extractDoctor() {


        this.doctorService.getData(this.sid)
          .subscribe((result) => {

            console.log(result);
            console.log(Object.keys(result).length);

            this.doctorData = result;

            appSettings.setString(
                "doctor_data",
                JSON.stringify(this.doctorData)
            );

          }, (error) => {
            console.log(error);
          });
      }
      public connectionToString(connectionType: number): string {
        switch (connectionType) {
          case Connectivity.connectionType.none:
            return "0";
          case Connectivity.connectionType.wifi:
            return "1";
          case Connectivity.connectionType.mobile:
            return "1";
          default:
            return "0";
        }
      }
}

/*
*
*    Remaining Work
*    ~~~~~~~~~~~~~~
*
*    Set ttf image to relevant Departments
*
*
*/
