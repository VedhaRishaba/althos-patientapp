"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ap_step3_service_1 = require("./ap-step3-service");
var appSettings = require("tns-core-modules/application-settings");
var ApStep3Component = /** @class */ (function () {
    function ApStep3Component(myService) {
        this.myService = myService;
        this.data = [];
        this.doctor_data = JSON.parse(appSettings.getString("doctor_data"));
        this.appointment_date = appSettings.getString("appointment_date");
        this.dept_no = appSettings.getString("dept_no");
    }
    ApStep3Component.prototype.ngOnInit = function () {
        console.log('vedha ====>' + this.appointment_date);
        console.log('vedha ====>' + this.dept_no);
        this.GetDoctorData();
    };
    ApStep3Component.prototype.GetDoctorData = function () {
        for (this.i = 0; this.i < Object.keys(this.doctor_data).length; this.i++) {
            this.data.push(this.doctor_data[this.i]);
            console.log('vedha ====>' + this.doctor_data[this.i]);
        }
    };
    ApStep3Component = __decorate([
        core_1.Component({
            selector: 'ns-ap-step3',
            templateUrl: './ap-step3.component.html',
            providers: [ap_step3_service_1.ap_step3_service],
            styleUrls: ['./ap-step3.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [ap_step3_service_1.ap_step3_service])
    ], ApStep3Component);
    return ApStep3Component;
}());
exports.ApStep3Component = ApStep3Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXAtc3RlcDMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXAtc3RlcDMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELHVEQUFzRDtBQUN0RCxtRUFBcUU7QUFRckU7SUFVRSwwQkFBb0IsU0FBMkI7UUFBM0IsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFSL0MsU0FBSSxHQUFHLEVBQUUsQ0FBQztRQUVWLGdCQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDL0QscUJBQWdCLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzdELFlBQU8sR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBSVEsQ0FBQztJQUVwRCxtQ0FBUSxHQUFSO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDbkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUd2QixDQUFDO0lBR0Qsd0NBQWEsR0FBYjtRQUNFLEtBQ0ksSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQ1YsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLEVBQzdDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFDVjtZQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyRDtJQUNMLENBQUM7SUEvQlksZ0JBQWdCO1FBUDVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsYUFBYTtZQUN2QixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBQyxDQUFDLG1DQUFnQixDQUFDO1lBQzVCLFNBQVMsRUFBRSxDQUFDLDBCQUEwQixDQUFDO1lBQ3ZDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQVcrQixtQ0FBZ0I7T0FWcEMsZ0JBQWdCLENBZ0M1QjtJQUFELHVCQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7QUFoQ1ksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgYXBfc3RlcDNfc2VydmljZSB9IGZyb20gJy4vYXAtc3RlcDMtc2VydmljZSc7XG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtYXAtc3RlcDMnLFxuICB0ZW1wbGF0ZVVybDogJy4vYXAtc3RlcDMuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6W2FwX3N0ZXAzX3NlcnZpY2VdLFxuICBzdHlsZVVybHM6IFsnLi9hcC1zdGVwMy5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEFwU3RlcDNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGRhdGEgPSBbXTtcblxuICBkb2N0b3JfZGF0YSA9IEpTT04ucGFyc2UoYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZG9jdG9yX2RhdGFcIikpO1xuICBhcHBvaW50bWVudF9kYXRlID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiYXBwb2ludG1lbnRfZGF0ZVwiKTtcbiAgZGVwdF9ubyA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcImRlcHRfbm9cIik7XG4gIGk6IG51bWJlcjtcbiAgajogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbXlTZXJ2aWNlOiBhcF9zdGVwM19zZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBjb25zb2xlLmxvZygndmVkaGEgPT09PT4nICsgdGhpcy5hcHBvaW50bWVudF9kYXRlKTtcbiAgICBjb25zb2xlLmxvZygndmVkaGEgPT09PT4nICsgdGhpcy5kZXB0X25vKTtcbiAgICB0aGlzLkdldERvY3RvckRhdGEoKTtcblxuXG4gIH1cblxuXG4gIEdldERvY3RvckRhdGEoKSB7XG4gICAgZm9yIChcbiAgICAgICAgdGhpcy5pID0gMDtcbiAgICAgICAgdGhpcy5pIDwgT2JqZWN0LmtleXModGhpcy5kb2N0b3JfZGF0YSkubGVuZ3RoO1xuICAgICAgICB0aGlzLmkrK1xuICAgICkge1xuICAgICAgICB0aGlzLmRhdGEucHVzaCh0aGlzLmRvY3Rvcl9kYXRhW3RoaXMuaV0pO1xuXG4gICAgY29uc29sZS5sb2coJ3ZlZGhhID09PT0+JyArIHRoaXMuZG9jdG9yX2RhdGFbdGhpcy5pXSk7XG4gICAgfVxufVxufVxuIl19