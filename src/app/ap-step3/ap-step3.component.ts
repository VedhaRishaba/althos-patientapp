import { Component, OnInit } from '@angular/core';

import { ap_step3_service } from './ap-step3-service';
import * as appSettings from "tns-core-modules/application-settings";
@Component({
  selector: 'ns-ap-step3',
  templateUrl: './ap-step3.component.html',
  providers:[ap_step3_service],
  styleUrls: ['./ap-step3.component.css'],
  moduleId: module.id,
})
export class ApStep3Component implements OnInit {

  data = [];

  doctor_data = JSON.parse(appSettings.getString("doctor_data"));
  appointment_date = appSettings.getString("appointment_date");
  dept_no = appSettings.getString("dept_no");
  i: number;
  j: number;

  constructor(private myService: ap_step3_service) { }

  ngOnInit() {
    console.log('vedha ====>' + this.appointment_date);
    console.log('vedha ====>' + this.dept_no);
    this.GetDoctorData();


  }


  GetDoctorData() {
    for (
        this.i = 0;
        this.i < Object.keys(this.doctor_data).length;
        this.i++
    ) {
        this.data.push(this.doctor_data[this.i]);

    console.log('vedha ====>' + this.doctor_data[this.i]);
    }
}
}
