"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var ap_step3_service = /** @class */ (function () {
    function ap_step3_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    }
    ap_step3_service.prototype.getData = function () {
        var headers = this.createRequestHeader();
        return this.http.get("https://psgimsr.ac.in/althos/ihsrest/v1/getAllDoctors?deptno=48&app_date=13-06-2019", { headers: headers });
    };
    ap_step3_service.prototype.createRequestHeader = function () {
        console.log("inside create");
        var headers = new http_1.HttpHeaders({
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json",
            "Set-Cookie": "sessionID=8H0CUEL231N4NNGUT4C5BEILCK  ; expires=2147483647000"
        });
        return headers;
    };
    ap_step3_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ap_step3_service);
    return ap_step3_service;
}());
exports.ap_step3_service = ap_step3_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXAtc3RlcDMtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwLXN0ZXAzLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBTy9EO0lBR0ksMEJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGNUIsY0FBUyxHQUFHLDBDQUEwQyxDQUFDO0lBRXZCLENBQUM7SUFFekMsa0NBQU8sR0FBUDtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMscUZBQXFGLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUN0SSxDQUFDO0lBQ08sOENBQW1CLEdBQTNCO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3QixJQUFJLE9BQU8sR0FBRyxJQUFJLGtCQUFXLENBQUM7WUFDMUIsZUFBZSxFQUFFLHFFQUFxRTtZQUN0RixjQUFjLEVBQUUsa0JBQWtCO1lBQ2xDLFlBQVksRUFBRSwrREFBK0Q7U0FDaEYsQ0FBQyxDQUFDO1FBRUgsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQWxCUSxnQkFBZ0I7UUFKNUIsaUJBQVUsRUFFVjt5Q0FLNkIsaUJBQVU7T0FIM0IsZ0JBQWdCLENBbUI1QjtJQUFELHVCQUFDO0NBQUEsQUFuQkQsSUFtQkM7QUFuQlksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKFxyXG5cclxuKVxyXG5cclxuZXhwb3J0IGNsYXNzIGFwX3N0ZXAzX3NlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSBzZXJ2ZXJVcmwgPSBcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9cIjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxyXG5cclxuICAgIGdldERhdGEoKSB7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSB0aGlzLmNyZWF0ZVJlcXVlc3RIZWFkZXIoKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldChcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9nZXRBbGxEb2N0b3JzP2RlcHRubz00OCZhcHBfZGF0ZT0xMy0wNi0yMDE5XCIsIHsgaGVhZGVyczogaGVhZGVycyB9KTtcclxuICAgIH1cclxuICAgIHByaXZhdGUgY3JlYXRlUmVxdWVzdEhlYWRlcigpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcImluc2lkZSBjcmVhdGVcIik7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICBcIkF1dGhvcml6YXRpb25cIjogXCJCZWFyZXIgVTFObmNGZGtkelZUVTJkd1YyUjNObDlKUkRwVFUyZHdWMlIzTlZOVFozQlhaSGMyWDFORlMxSkZWQT09XCIsXHJcbiAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICBcIlNldC1Db29raWVcIjogXCJzZXNzaW9uSUQ9OEgwQ1VFTDIzMU40Tk5HVVQ0QzVCRUlMQ0sgIDsgZXhwaXJlcz0yMTQ3NDgzNjQ3MDAwXCJcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGhlYWRlcnM7XHJcbiAgICB9XHJcbn1cclxuIl19