"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var FaqComponent = /** @class */ (function () {
    function FaqComponent(router) {
        this.router = router;
        this.webViewSrc = "https://docs.google.com/gview?embedded=true&url=www.psghospitals.com/mob%20app/FAQ.pdf";
        this.loading = false;
    }
    FaqComponent.prototype.ngOnInit = function () {
    };
    FaqComponent.prototype.pageLoaded = function () {
        this.loading = true;
    };
    FaqComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    FaqComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    FaqComponent = __decorate([
        core_1.Component({
            selector: 'ns-faq',
            templateUrl: './faq.component.html',
            styleUrls: ['./faq.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], FaqComponent);
    return FaqComponent;
}());
exports.FaqComponent = FaqComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFxLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZhcS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQscURBQXVEO0FBQ3ZELDBDQUF5QztBQUN6QyxtRUFBcUU7QUFRckU7SUFFRSxzQkFBb0IsTUFBYTtRQUFiLFdBQU0sR0FBTixNQUFNLENBQU87UUFDMUIsZUFBVSxHQUFXLHdGQUF3RixDQUFDO1FBQ3JILFlBQU8sR0FBRyxLQUFLLENBQUM7SUFGcUIsQ0FBQztJQUt0QywrQkFBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELGlDQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN2QixDQUFDO0lBRUQsNkJBQU0sR0FBTjtRQUFBLGlCQVVBO1FBVEMsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUNkLE9BQU8sRUFBRSwwQkFBMEI7WUFDbkMsWUFBWSxFQUFFLEtBQUs7WUFDbkIsZ0JBQWdCLEVBQUUsSUFBSTtTQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNaLElBQUksTUFBTSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELHFDQUFjLEdBQWQ7UUFBQSxpQkFpQ0M7UUFoQ0MsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsRUFBRTtTQUNaLENBQUM7UUFDRixJQUFNLE9BQU8sR0FBRztZQUVsQixPQUFPLEVBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7U0FDdkMsQ0FBQTtRQUNELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNoRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFYixLQUFLLENBQUMsZ0RBQWdELEVBQUU7WUFDdEQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsY0FBYyxFQUFFLG1DQUFtQyxFQUFFLGVBQWUsRUFBRSxxRUFBcUU7Z0JBQzNJLFNBQVMsRUFBRSxPQUFPO2FBQ25CO1lBQ0QsSUFBSSxFQUFFLFlBQVk7U0FDbkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBUixDQUFRLENBQUM7YUFDckIsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUNJO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBMURZLFlBQVk7UUFOeEIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFdBQVcsRUFBRSxzQkFBc0I7WUFDbkMsU0FBUyxFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDbEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBRzJCLGVBQU07T0FGdEIsWUFBWSxDQTREeEI7SUFBRCxtQkFBQztDQUFBLEFBNURELElBNERDO0FBNURZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCAqIGFzIGFwcFNldHRpbmdzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLWZhcScsXG4gIHRlbXBsYXRlVXJsOiAnLi9mYXEuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9mYXEuY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBGYXFDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOlJvdXRlcikgeyB9XG4gIHB1YmxpYyB3ZWJWaWV3U3JjOiBzdHJpbmcgPSBcImh0dHBzOi8vZG9jcy5nb29nbGUuY29tL2d2aWV3P2VtYmVkZGVkPXRydWUmdXJsPXd3dy5wc2dob3NwaXRhbHMuY29tL21vYiUyMGFwcC9GQVEucGRmXCI7XG4gIGxvYWRpbmcgPSBmYWxzZTtcblxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgcGFnZUxvYWRlZCgpe1xuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gfVxuICBcbiBsb2dvdXQoKSB7XG4gIGRpYWxvZ3MuY29uZmlybSh7XG4gICAgbWVzc2FnZTogXCJEbyB5b3Ugd2FudCB0byBMb2dvdXQgPyBcIixcbiAgICBva0J1dHRvblRleHQ6IFwiWUVTXCIsXG4gICAgY2FuY2VsQnV0dG9uVGV4dDogXCJOT1wiLFxuICB9KS50aGVuKHJlc3VsdCA9PiB7XG4gICAgaWYgKHJlc3VsdCkge1xuICAgICAgdGhpcy5sb2dvdXRhY3Rpdml0eSgpO1xuICAgIH1cbiAgfSk7XG59XG5sb2dvdXRhY3Rpdml0eSgpIHtcbiAgY29uc3Qgb2JqZWN0MSA9IHtcbiAgICBvcF9jb2RlOiBcIlwiXG4gIH07XG4gIGNvbnN0IG9iamVjdDIgPSB7XG4gICAgXG5vcF9jb2RlOiAgYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwib3AtY29kZVwiKVxuICB9XG4gIGNvbnN0IHNlYXJjaFBhcmFtcyA9IE9iamVjdC5rZXlzKG9iamVjdDEpLm1hcCgoa2V5KSA9PiB7XG4gICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XG4gIH0pLmpvaW4oJyYnKTtcblxuICBmZXRjaChcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9sb2dvdXRcIiwge1xuICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgaGVhZGVyczoge1xuICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWRcIiwgXCJBdXRob3JpemF0aW9uXCI6IFwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxuICAgICAgXCJjaGFyc2V0XCI6IFwidXRmLThcIlxuICAgIH0sXG4gICAgYm9keTogc2VhcmNoUGFyYW1zLFxuICB9KS50aGVuKChyKSA9PiByLnRleHQoKSlcbiAgICAudGhlbigodGV4dCkgPT4ge1xuICAgICAgY29uc29sZS5sb2coXCJtZXNzYWdlXCIgKyB0ZXh0KTtcbiAgICAgIGlmICh0ZXh0Lmxlbmd0aCA9PSA0KSB7XG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vcC1jb2RlJyk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJmYWlsZWQgdG8gbG9nb3V0XCIpO1xuICAgICAgfVxuICAgIH0pLmNhdGNoKChlKSA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhcIkVycm9yOiBcIik7XG4gICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICB9KTtcblxufVxuXG59XG4iXX0=