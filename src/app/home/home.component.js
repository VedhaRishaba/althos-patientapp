"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
op_code: appSettings.getString("op-code");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(router) {
        this.router = router;
        this.profile_data = JSON.parse(appSettings.getString("profile_data"));
        this.name = this.profile_data["patient_name"];
        // Use the component constructor to inject providers.
    }
    HomeComponent.prototype.ngOnInit = function () {
        // Init your component properties here.
    };
    HomeComponent.prototype.appointmentClick = function () {
        this.router.navigate(["/appointment"]);
        /* dialogs.confirm({
           message: "Appointment booking coming soon",
           okButtonText: "OK",
    
       }).then(result => {
         if(result){
           this.router.navigate(["/appointment"]);
         }
       }); */
    };
    HomeComponent.prototype.labClick = function () {
        /* dialogs.confirm({
           message: "Lab booking coming soon",
           okButtonText: "OK",
    
       }).then(result => {
         if(result){
           this.router.navigate(["/lab"]);
         }
       }); */
        this.router.navigate(["/lab"]);
    };
    HomeComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    HomeComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['./home.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxxREFBdUQ7QUFDdkQsMENBQXlDO0FBQ3pDLG1FQUFxRTtBQUNyRSxPQUFPLEVBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQVExQztJQUtFLHVCQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUhoQyxpQkFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLFNBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBSXpDLHFEQUFxRDtJQUN2RCxDQUFDO0lBRUQsZ0NBQVEsR0FBUjtRQUNFLHVDQUF1QztJQUV6QyxDQUFDO0lBRUQsd0NBQWdCLEdBQWhCO1FBR0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ3ZDOzs7Ozs7OzthQVFLO0lBSVAsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDRTs7Ozs7Ozs7YUFRSztRQUlMLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsOEJBQU0sR0FBTjtRQUFBLGlCQVVDO1FBVEMsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUNkLE9BQU8sRUFBRSwwQkFBMEI7WUFDbkMsWUFBWSxFQUFFLEtBQUs7WUFDbkIsZ0JBQWdCLEVBQUUsSUFBSTtTQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNaLElBQUksTUFBTSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELHNDQUFjLEdBQWQ7UUFBQSxpQkFpQ0M7UUFoQ0MsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsRUFBRTtTQUNaLENBQUM7UUFDRixJQUFNLE9BQU8sR0FBRztZQUVkLE9BQU8sRUFBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQztTQUMzQyxDQUFBO1FBQ0QsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHO1lBQ2hELE9BQU8sa0JBQWtCLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUViLEtBQUssQ0FBQyxnREFBZ0QsRUFBRTtZQUN0RCxNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRTtnQkFDUCxjQUFjLEVBQUUsbUNBQW1DLEVBQUUsZUFBZSxFQUFFLHFFQUFxRTtnQkFDM0ksU0FBUyxFQUFFLE9BQU87YUFDbkI7WUFDRCxJQUFJLEVBQUUsWUFBWTtTQUNuQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFSLENBQVEsQ0FBQzthQUNyQixJQUFJLENBQUMsVUFBQyxJQUFJO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDOUIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDdkM7aUJBQ0k7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQ2pDO1FBQ0gsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUE3RlUsYUFBYTtRQU56QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLE1BQU07WUFDaEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7U0FDcEMsQ0FBQzt5Q0FNNEIsZUFBTTtPQUx2QixhQUFhLENBOEZ6QjtJQUFELG9CQUFDO0NBQUEsQUE5RkQsSUE4RkM7QUE5Rlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgKiBhcyBkaWFsb2dzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xub3BfY29kZTogIGFwcFNldHRpbmdzLmdldFN0cmluZyhcIm9wLWNvZGVcIilcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIkhvbWVcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9ob21lLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogWycuL2hvbWUuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgcHJvZmlsZV9kYXRhID0gSlNPTi5wYXJzZShhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJwcm9maWxlX2RhdGFcIikpO1xuICAgIG5hbWUgPSB0aGlzLnByb2ZpbGVfZGF0YVtcInBhdGllbnRfbmFtZVwiXTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XG5cbiAgICAvLyBVc2UgdGhlIGNvbXBvbmVudCBjb25zdHJ1Y3RvciB0byBpbmplY3QgcHJvdmlkZXJzLlxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgLy8gSW5pdCB5b3VyIGNvbXBvbmVudCBwcm9wZXJ0aWVzIGhlcmUuXG5cbiAgfVxuXG4gIGFwcG9pbnRtZW50Q2xpY2soKSB7XG5cblxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9hcHBvaW50bWVudFwiXSk7XG4gICAgLyogZGlhbG9ncy5jb25maXJtKHtcbiAgICAgICBtZXNzYWdlOiBcIkFwcG9pbnRtZW50IGJvb2tpbmcgY29taW5nIHNvb25cIixcbiAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIixcblxuICAgfSkudGhlbihyZXN1bHQgPT4ge1xuICAgICBpZihyZXN1bHQpe1xuICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIi9hcHBvaW50bWVudFwiXSk7XG4gICAgIH1cbiAgIH0pOyAqL1xuXG5cblxuICB9XG5cbiAgbGFiQ2xpY2soKSB7XG4gICAgLyogZGlhbG9ncy5jb25maXJtKHtcbiAgICAgICBtZXNzYWdlOiBcIkxhYiBib29raW5nIGNvbWluZyBzb29uXCIsXG4gICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCIsXG5cbiAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgaWYocmVzdWx0KXtcbiAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvbGFiXCJdKTtcbiAgICAgfVxuICAgfSk7ICovXG5cblxuXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2xhYlwiXSk7XG4gIH1cblxuICBsb2dvdXQoKSB7XG4gICAgZGlhbG9ncy5jb25maXJtKHtcbiAgICAgIG1lc3NhZ2U6IFwiRG8geW91IHdhbnQgdG8gTG9nb3V0ID8gXCIsXG4gICAgICBva0J1dHRvblRleHQ6IFwiWUVTXCIsXG4gICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCIsXG4gICAgfSkudGhlbihyZXN1bHQgPT4ge1xuICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgbG9nb3V0YWN0aXZpdHkoKSB7XG4gICAgY29uc3Qgb2JqZWN0MSA9IHtcbiAgICAgIG9wX2NvZGU6IFwiXCJcbiAgICB9O1xuICAgIGNvbnN0IG9iamVjdDIgPSB7XG5cbiAgICAgIG9wX2NvZGU6ICBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJvcC1jb2RlXCIpXG4gICAgfVxuICAgIGNvbnN0IHNlYXJjaFBhcmFtcyA9IE9iamVjdC5rZXlzKG9iamVjdDEpLm1hcCgoa2V5KSA9PiB7XG4gICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQob2JqZWN0MltrZXldKTtcbiAgICB9KS5qb2luKCcmJyk7XG5cbiAgICBmZXRjaChcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9sb2dvdXRcIiwge1xuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWRcIiwgXCJBdXRob3JpemF0aW9uXCI6IFwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxuICAgICAgICBcImNoYXJzZXRcIjogXCJ1dGYtOFwiXG4gICAgICB9LFxuICAgICAgYm9keTogc2VhcmNoUGFyYW1zLFxuICAgIH0pLnRoZW4oKHIpID0+IHIudGV4dCgpKVxuICAgICAgLnRoZW4oKHRleHQpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coXCJtZXNzYWdlXCIgKyB0ZXh0KTtcbiAgICAgICAgaWYgKHRleHQubGVuZ3RoID09IDQpIHtcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3AtY29kZScpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiZmFpbGVkIHRvIGxvZ291dFwiKTtcbiAgICAgICAgfVxuICAgICAgfSkuY2F0Y2goKGUpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coXCJFcnJvcjogXCIpO1xuICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgIH0pO1xuXG4gIH1cbn1cbiJdfQ==