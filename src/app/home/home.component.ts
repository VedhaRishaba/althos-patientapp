import { Component, OnInit } from "@angular/core";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Router } from '@angular/router';
import * as appSettings from "tns-core-modules/application-settings";
op_code:  appSettings.getString("op-code")

@Component({
  selector: "Home",
  moduleId: module.id,
  templateUrl: "./home.component.html",
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    profile_data = JSON.parse(appSettings.getString("profile_data"));
    name = this.profile_data["patient_name"];

  constructor(private router: Router) {

    // Use the component constructor to inject providers.
  }

  ngOnInit(): void {
    // Init your component properties here.

  }

  appointmentClick() {


    this.router.navigate(["/appointment"]);
    /* dialogs.confirm({
       message: "Appointment booking coming soon",
       okButtonText: "OK",

   }).then(result => {
     if(result){
       this.router.navigate(["/appointment"]);
     }
   }); */



  }

  labClick() {
    /* dialogs.confirm({
       message: "Lab booking coming soon",
       okButtonText: "OK",

   }).then(result => {
     if(result){
       this.router.navigate(["/lab"]);
     }
   }); */



    this.router.navigate(["/lab"]);
  }

  logout() {
    dialogs.confirm({
      message: "Do you want to Logout ? ",
      okButtonText: "YES",
      cancelButtonText: "NO",
    }).then(result => {
      if (result) {
        this.logoutactivity();
      }
    });
  }
  logoutactivity() {
    const object1 = {
      op_code: ""
    };
    const object2 = {

      op_code:  appSettings.getString("op-code")
    }
    const searchParams = Object.keys(object1).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
    }).join('&');

    fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
        "charset": "utf-8"
      },
      body: searchParams,
    }).then((r) => r.text())
      .then((text) => {
        console.log("message" + text);
        if (text.length == 4) {
          this.router.navigateByUrl('/op-code');
        }
        else {
          console.log("failed to logout");
        }
      }).catch((e) => {
        console.log("Error: ");
        console.log(e);
      });

  }
}
