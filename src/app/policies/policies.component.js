"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var PoliciesComponent = /** @class */ (function () {
    function PoliciesComponent(router) {
        this.router = router;
        this.webViewSrc = "https://docs.google.com/gview?embedded=true&url=http://www.psghospitals.com/mob%20app/Privacy%20Policy.pdf";
        this.loading = false;
    }
    PoliciesComponent.prototype.ngOnInit = function () {
    };
    PoliciesComponent.prototype.pageLoaded = function () {
        this.loading = true;
    };
    PoliciesComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    PoliciesComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    PoliciesComponent = __decorate([
        core_1.Component({
            selector: 'ns-policies',
            templateUrl: './policies.component.html',
            styleUrls: ['./policies.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], PoliciesComponent);
    return PoliciesComponent;
}());
exports.PoliciesComponent = PoliciesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9saWNpZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicG9saWNpZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHFEQUF1RDtBQUN2RCwwQ0FBeUM7QUFDekMsbUVBQXFFO0FBUXJFO0lBRUUsMkJBQW9CLE1BQWE7UUFBYixXQUFNLEdBQU4sTUFBTSxDQUFPO1FBQzFCLGVBQVUsR0FBVyw0R0FBNEcsQ0FBQztRQUN6SSxZQUFPLEdBQUcsS0FBSyxDQUFDO0lBRnFCLENBQUM7SUFLdEMsb0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCxzQ0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUdELGtDQUFNLEdBQU47UUFBQSxpQkFVQTtRQVRDLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDZCxPQUFPLEVBQUUsMEJBQTBCO1lBQ25DLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLElBQUk7U0FDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07WUFDWixJQUFJLE1BQU0sRUFBRTtnQkFDVixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCwwQ0FBYyxHQUFkO1FBQUEsaUJBaUNDO1FBaENDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBQ0YsSUFBTSxPQUFPLEdBQUc7WUFFbEIsT0FBTyxFQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDO1NBQ3ZDLENBQUE7UUFDRCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7WUFDaEQsT0FBTyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWIsS0FBSyxDQUFDLGdEQUFnRCxFQUFFO1lBQ3RELE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLGNBQWMsRUFBRSxtQ0FBbUMsRUFBRSxlQUFlLEVBQUUscUVBQXFFO2dCQUMzSSxTQUFTLEVBQUUsT0FBTzthQUNuQjtZQUNELElBQUksRUFBRSxZQUFZO1NBQ25CLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQVIsQ0FBUSxDQUFDO2FBQ3JCLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUM5QixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUNwQixLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN2QztpQkFDSTtnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDakM7UUFDSCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxDQUFDO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQTNEWSxpQkFBaUI7UUFON0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFdBQVcsRUFBRSwyQkFBMkI7WUFDeEMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLENBQUM7WUFDdkMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBRzJCLGVBQU07T0FGdEIsaUJBQWlCLENBNkQ3QjtJQUFELHdCQUFDO0NBQUEsQUE3REQsSUE2REM7QUE3RFksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCAqIGFzIGFwcFNldHRpbmdzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLXBvbGljaWVzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3BvbGljaWVzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcG9saWNpZXMuY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBQb2xpY2llc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6Um91dGVyKSB7IH1cbiAgcHVibGljIHdlYlZpZXdTcmM6IHN0cmluZyA9IFwiaHR0cHM6Ly9kb2NzLmdvb2dsZS5jb20vZ3ZpZXc/ZW1iZWRkZWQ9dHJ1ZSZ1cmw9aHR0cDovL3d3dy5wc2dob3NwaXRhbHMuY29tL21vYiUyMGFwcC9Qcml2YWN5JTIwUG9saWN5LnBkZlwiO1xuICBsb2FkaW5nID0gZmFsc2U7XG5cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIHBhZ2VMb2FkZWQoKXtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuIH1cblxuIFxuIGxvZ291dCgpIHtcbiAgZGlhbG9ncy5jb25maXJtKHtcbiAgICBtZXNzYWdlOiBcIkRvIHlvdSB3YW50IHRvIExvZ291dCA/IFwiLFxuICAgIG9rQnV0dG9uVGV4dDogXCJZRVNcIixcbiAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCIsXG4gIH0pLnRoZW4ocmVzdWx0ID0+IHtcbiAgICBpZiAocmVzdWx0KSB7XG4gICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XG4gICAgfVxuICB9KTtcbn1cbmxvZ291dGFjdGl2aXR5KCkge1xuICBjb25zdCBvYmplY3QxID0ge1xuICAgIG9wX2NvZGU6IFwiXCJcbiAgfTtcbiAgY29uc3Qgb2JqZWN0MiA9IHtcbiAgICBcbm9wX2NvZGU6ICBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJvcC1jb2RlXCIpXG4gIH1cbiAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcbiAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQob2JqZWN0MltrZXldKTtcbiAgfSkuam9pbignJicpO1xuXG4gIGZldGNoKFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL2xvZ291dFwiLCB7XG4gICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICBoZWFkZXJzOiB7XG4gICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiLCBcIkF1dGhvcml6YXRpb25cIjogXCJCZWFyZXIgVTFObmNGZGtkelZUVTJkd1YyUjNObDlKUkRwVFUyZHdWMlIzTlZOVFozQlhaSGMyWDFORlMxSkZWQT09XCIsXG4gICAgICBcImNoYXJzZXRcIjogXCJ1dGYtOFwiXG4gICAgfSxcbiAgICBib2R5OiBzZWFyY2hQYXJhbXMsXG4gIH0pLnRoZW4oKHIpID0+IHIudGV4dCgpKVxuICAgIC50aGVuKCh0ZXh0KSA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhcIm1lc3NhZ2VcIiArIHRleHQpO1xuICAgICAgaWYgKHRleHQubGVuZ3RoID09IDQpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL29wLWNvZGUnKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBjb25zb2xlLmxvZyhcImZhaWxlZCB0byBsb2dvdXRcIik7XG4gICAgICB9XG4gICAgfSkuY2F0Y2goKGUpID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3I6IFwiKTtcbiAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgIH0pO1xuXG59XG5cbn1cbiJdfQ==