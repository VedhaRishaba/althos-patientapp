import { Component, OnInit } from '@angular/core';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Router } from '@angular/router';
import * as appSettings from "tns-core-modules/application-settings";

@Component({
  selector: 'ns-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.css'],
  moduleId: module.id,
})
export class PoliciesComponent implements OnInit {

  constructor(private router:Router) { }
  public webViewSrc: string = "https://docs.google.com/gview?embedded=true&url=http://www.psghospitals.com/mob%20app/Privacy%20Policy.pdf";
  loading = false;


  ngOnInit() {
  }

  pageLoaded(){
    this.loading = true;
 }

 
 logout() {
  dialogs.confirm({
    message: "Do you want to Logout ? ",
    okButtonText: "YES",
    cancelButtonText: "NO",
  }).then(result => {
    if (result) {
      this.logoutactivity();
    }
  });
}
logoutactivity() {
  const object1 = {
    op_code: ""
  };
  const object2 = {
    
op_code:  appSettings.getString("op-code")
  }
  const searchParams = Object.keys(object1).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
  }).join('&');

  fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
      "charset": "utf-8"
    },
    body: searchParams,
  }).then((r) => r.text())
    .then((text) => {
      console.log("message" + text);
      if (text.length == 4) {
        this.router.navigateByUrl('/op-code');
      }
      else {
        console.log("failed to logout");
      }
    }).catch((e) => {
      console.log("Error: ");
      console.log(e);
    });

}

}
