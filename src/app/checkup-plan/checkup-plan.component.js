"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var CheckupPlanComponent = /** @class */ (function () {
    function CheckupPlanComponent(router) {
        this.router = router;
        this.passData = 0;
        this.one = "~/images/one.png";
        this.two = "~/images/two_gray.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.currentStep = 1;
    }
    CheckupPlanComponent.prototype.ngOnInit = function () { };
    CheckupPlanComponent.prototype.step1 = function () {
        this.one = "~/images/one.png";
        this.two = "~/images/two_gray.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.currentStep = 1;
    };
    CheckupPlanComponent.prototype.step2 = function () {
        this.currentStep = 2;
        this.one = "~/images/checked.png";
        this.two = "~/images/two.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
    };
    CheckupPlanComponent.prototype.step3 = function () {
        this.currentStep = 3;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/three.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
    };
    CheckupPlanComponent.prototype.step4 = function () {
        this.currentStep = 4;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/checked.png";
        this.four = "~/images/four.png";
        this.five = "~/images/five_gray.png";
    };
    CheckupPlanComponent.prototype.step5 = function () {
        this.currentStep = 5;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/checked.png";
        this.four = "~/images/checked.png";
        this.five = "~/images/five.png";
    };
    CheckupPlanComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    CheckupPlanComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    CheckupPlanComponent = __decorate([
        core_1.Component({
            selector: 'ns-checkup-plan',
            templateUrl: './checkup-plan.component.html',
            styleUrls: ['./checkup-plan.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], CheckupPlanComponent);
    return CheckupPlanComponent;
}());
exports.CheckupPlanComponent = CheckupPlanComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2t1cC1wbGFuLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNoZWNrdXAtcGxhbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQscURBQXVEO0FBQ3ZELDBDQUF5QztBQUN6QyxtRUFBcUU7QUFRckU7SUFTSSw4QkFBb0IsTUFBYTtRQUFiLFdBQU0sR0FBTixNQUFNLENBQU87UUFSakMsYUFBUSxHQUFFLENBQUMsQ0FBQztRQUNaLFFBQUcsR0FBQyxrQkFBa0IsQ0FBQztRQUN2QixRQUFHLEdBQUMsdUJBQXVCLENBQUM7UUFDNUIsVUFBSyxHQUFDLHlCQUF5QixDQUFDO1FBQ2hDLFNBQUksR0FBQyx3QkFBd0IsQ0FBQztRQUM5QixTQUFJLEdBQUMsd0JBQXdCLENBQUM7UUFDOUIsZ0JBQVcsR0FBRyxDQUFDLENBQUM7SUFFcUIsQ0FBQztJQUV0Qyx1Q0FBUSxHQUFSLGNBQWEsQ0FBQztJQUVkLG9DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsR0FBRyxHQUFDLGtCQUFrQixDQUFDO1FBQzVCLElBQUksQ0FBQyxHQUFHLEdBQUMsdUJBQXVCLENBQUM7UUFDakMsSUFBSSxDQUFDLEtBQUssR0FBQyx5QkFBeUIsQ0FBQztRQUNyQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVELG9DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsa0JBQWtCLENBQUM7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBQyx5QkFBeUIsQ0FBQztRQUNyQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7SUFDckMsQ0FBQztJQUVELG9DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsc0JBQXNCLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBQyxvQkFBb0IsQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7SUFDckMsQ0FBQztJQUVELG9DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsc0JBQXNCLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBQyxzQkFBc0IsQ0FBQztRQUNsQyxJQUFJLENBQUMsSUFBSSxHQUFDLG1CQUFtQixDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7SUFDckMsQ0FBQztJQUVELG9DQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsc0JBQXNCLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBQyxzQkFBc0IsQ0FBQztRQUNsQyxJQUFJLENBQUMsSUFBSSxHQUFDLHNCQUFzQixDQUFDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUMsbUJBQW1CLENBQUM7SUFDaEMsQ0FBQztJQUdILHFDQUFNLEdBQU47UUFBQSxpQkFVQztRQVRDLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDZCxPQUFPLEVBQUUsMEJBQTBCO1lBQ25DLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLElBQUk7U0FDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07WUFDWixJQUFJLE1BQU0sRUFBRTtnQkFDVixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCw2Q0FBYyxHQUFkO1FBQUEsaUJBZ0NDO1FBL0JDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBQ0YsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7U0FDM0MsQ0FBQTtRQUNELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNoRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFYixLQUFLLENBQUMsZ0RBQWdELEVBQUU7WUFDdEQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsY0FBYyxFQUFFLG1DQUFtQyxFQUFFLGVBQWUsRUFBRSxxRUFBcUU7Z0JBQzNJLFNBQVMsRUFBRSxPQUFPO2FBQ25CO1lBQ0QsSUFBSSxFQUFFLFlBQVk7U0FDbkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBUixDQUFRLENBQUM7YUFDckIsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUNJO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBdEdVLG9CQUFvQjtRQU5oQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsK0JBQStCO1lBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO1lBQzNDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQVU2QixlQUFNO09BVHhCLG9CQUFvQixDQXdHaEM7SUFBRCwyQkFBQztDQUFBLEFBeEdELElBd0dDO0FBeEdZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICducy1jaGVja3VwLXBsYW4nLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGVja3VwLXBsYW4uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NoZWNrdXAtcGxhbi5jb21wb25lbnQuY3NzJ10sXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxufSlcclxuZXhwb3J0IGNsYXNzIENoZWNrdXBQbGFuQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHBhc3NEYXRhID0wO1xyXG4gICAgb25lPVwifi9pbWFnZXMvb25lLnBuZ1wiO1xyXG4gICAgdHdvPVwifi9pbWFnZXMvdHdvX2dyYXkucG5nXCI7XHJcbiAgICB0aHJlZT1cIn4vaW1hZ2VzL3RocmVlX2dyYXkucG5nXCI7XHJcbiAgICBmb3VyPVwifi9pbWFnZXMvZm91cl9ncmF5LnBuZ1wiO1xyXG4gICAgZml2ZT1cIn4vaW1hZ2VzL2ZpdmVfZ3JheS5wbmdcIjtcclxuICAgIGN1cnJlbnRTdGVwID0gMTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjpSb3V0ZXIpIHsgfVxyXG5cclxuICAgIG5nT25Jbml0KCkgeyB9XHJcblxyXG4gICAgc3RlcDEoKXtcclxuICAgICAgdGhpcy5vbmU9XCJ+L2ltYWdlcy9vbmUucG5nXCI7XHJcbiAgICAgIHRoaXMudHdvPVwifi9pbWFnZXMvdHdvX2dyYXkucG5nXCI7XHJcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy90aHJlZV9ncmF5LnBuZ1wiO1xyXG4gICAgICB0aGlzLmZvdXI9XCJ+L2ltYWdlcy9mb3VyX2dyYXkucG5nXCI7XHJcbiAgICAgIHRoaXMuZml2ZT1cIn4vaW1hZ2VzL2ZpdmVfZ3JheS5wbmdcIjtcclxuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDE7XHJcbiAgICB9XHJcblxyXG4gICAgc3RlcDIoKXtcclxuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDI7XHJcbiAgICAgIHRoaXMub25lPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy50d289XCJ+L2ltYWdlcy90d28ucG5nXCI7XHJcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy90aHJlZV9ncmF5LnBuZ1wiO1xyXG4gICAgICB0aGlzLmZvdXI9XCJ+L2ltYWdlcy9mb3VyX2dyYXkucG5nXCI7XHJcbiAgICAgIHRoaXMuZml2ZT1cIn4vaW1hZ2VzL2ZpdmVfZ3JheS5wbmdcIjtcclxuICAgIH1cclxuXHJcbiAgICBzdGVwMygpe1xyXG4gICAgICB0aGlzLmN1cnJlbnRTdGVwID0gMztcclxuICAgICAgdGhpcy5vbmU9XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xyXG4gICAgICB0aGlzLnR3bz1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XHJcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy90aHJlZS5wbmdcIjtcclxuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvZm91cl9ncmF5LnBuZ1wiO1xyXG4gICAgICB0aGlzLmZpdmU9XCJ+L2ltYWdlcy9maXZlX2dyYXkucG5nXCI7XHJcbiAgICB9XHJcblxyXG4gICAgc3RlcDQoKXtcclxuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDQ7XHJcbiAgICAgIHRoaXMub25lPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy50d289XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xyXG4gICAgICB0aGlzLnRocmVlPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvZm91ci5wbmdcIjtcclxuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xyXG4gICAgfVxyXG5cclxuICAgIHN0ZXA1KCl7XHJcbiAgICAgIHRoaXMuY3VycmVudFN0ZXAgPSA1O1xyXG4gICAgICB0aGlzLm9uZT1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XHJcbiAgICAgIHRoaXMudHdvPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy50aHJlZT1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XHJcbiAgICAgIHRoaXMuZm91cj1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XHJcbiAgICAgIHRoaXMuZml2ZT1cIn4vaW1hZ2VzL2ZpdmUucG5nXCI7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgbG9nb3V0KCkge1xyXG4gICAgZGlhbG9ncy5jb25maXJtKHtcclxuICAgICAgbWVzc2FnZTogXCJEbyB5b3Ugd2FudCB0byBMb2dvdXQgPyBcIixcclxuICAgICAgb2tCdXR0b25UZXh0OiBcIllFU1wiLFxyXG4gICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCIsXHJcbiAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBsb2dvdXRhY3Rpdml0eSgpIHtcclxuICAgIGNvbnN0IG9iamVjdDEgPSB7XHJcbiAgICAgIG9wX2NvZGU6IFwiXCJcclxuICAgIH07XHJcbiAgICBjb25zdCBvYmplY3QyID0ge1xyXG4gICAgICBvcF9jb2RlOiAgYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwib3AtY29kZVwiKVxyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcclxuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XHJcbiAgICB9KS5qb2luKCcmJyk7XHJcblxyXG4gICAgZmV0Y2goXCJodHRwczovL3BzZ2ltc3IuYWMuaW4vYWx0aG9zL2loc3Jlc3QvdjEvbG9nb3V0XCIsIHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsIFwiQXV0aG9yaXphdGlvblwiOiBcIkJlYXJlciBVMU5uY0Zka2R6VlRVMmR3VjJSM05sOUpSRHBUVTJkd1YyUjNOVk5UWjNCWFpIYzJYMU5GUzFKRlZBPT1cIixcclxuICAgICAgICBcImNoYXJzZXRcIjogXCJ1dGYtOFwiXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IHNlYXJjaFBhcmFtcyxcclxuICAgIH0pLnRoZW4oKHIpID0+IHIudGV4dCgpKVxyXG4gICAgICAudGhlbigodGV4dCkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwibWVzc2FnZVwiICsgdGV4dCk7XHJcbiAgICAgICAgaWYgKHRleHQubGVuZ3RoID09IDQpIHtcclxuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vcC1jb2RlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJmYWlsZWQgdG8gbG9nb3V0XCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yOiBcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICB9XHJcblxyXG59XHJcbiJdfQ==