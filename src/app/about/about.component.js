"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var AboutComponent = /** @class */ (function () {
    function AboutComponent(router) {
        this.router = router;
    }
    AboutComponent.prototype.ngOnInit = function () { };
    AboutComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    AboutComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    AboutComponent = __decorate([
        core_1.Component({
            selector: 'ns-about',
            templateUrl: './about.component.html',
            styleUrls: ['./about.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], AboutComponent);
    return AboutComponent;
}());
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWJvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHFEQUF1RDtBQUN2RCwwQ0FBeUM7QUFFekMsbUVBQXFFO0FBU3JFO0lBRUUsd0JBQW9CLE1BQWE7UUFBYixXQUFNLEdBQU4sTUFBTSxDQUFPO0lBQUksQ0FBQztJQUV0QyxpQ0FBUSxHQUFSLGNBQVksQ0FBQztJQUViLCtCQUFNLEdBQU47UUFBQSxpQkFVQztRQVRDLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDZCxPQUFPLEVBQUUsMEJBQTBCO1lBQ25DLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLElBQUk7U0FDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07WUFDWixJQUFJLE1BQU0sRUFBRTtnQkFDVixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCx1Q0FBYyxHQUFkO1FBQUEsaUJBZ0NDO1FBL0JDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBQ0YsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7U0FDM0MsQ0FBQTtRQUNELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNoRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFYixLQUFLLENBQUMsZ0RBQWdELEVBQUU7WUFDdEQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsY0FBYyxFQUFFLG1DQUFtQyxFQUFFLGVBQWUsRUFBRSxxRUFBcUU7Z0JBQzNJLFNBQVMsRUFBRSxPQUFPO2FBQ25CO1lBQ0QsSUFBSSxFQUFFLFlBQVk7U0FDbkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBUixDQUFRLENBQUM7YUFDckIsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUNJO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUM7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBakRVLGNBQWM7UUFQMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDcEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBSTJCLGVBQU07T0FGdEIsY0FBYyxDQW9EMUI7SUFBRCxxQkFBQztDQUFBLEFBcERELElBb0RDO0FBcERZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICducy1hYm91dCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2Fib3V0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9hYm91dC5jb21wb25lbnQuY3NzJ10sXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBYm91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOlJvdXRlcikgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge31cclxuXHJcbiAgbG9nb3V0KCkge1xyXG4gICAgZGlhbG9ncy5jb25maXJtKHtcclxuICAgICAgbWVzc2FnZTogXCJEbyB5b3Ugd2FudCB0byBMb2dvdXQgPyBcIixcclxuICAgICAgb2tCdXR0b25UZXh0OiBcIllFU1wiLFxyXG4gICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCIsXHJcbiAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBsb2dvdXRhY3Rpdml0eSgpIHtcclxuICAgIGNvbnN0IG9iamVjdDEgPSB7XHJcbiAgICAgIG9wX2NvZGU6IFwiXCJcclxuICAgIH07XHJcbiAgICBjb25zdCBvYmplY3QyID0ge1xyXG4gICAgICBvcF9jb2RlOiAgYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwib3AtY29kZVwiKVxyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcclxuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XHJcbiAgICB9KS5qb2luKCcmJyk7XHJcblxyXG4gICAgZmV0Y2goXCJodHRwczovL3BzZ2ltc3IuYWMuaW4vYWx0aG9zL2loc3Jlc3QvdjEvbG9nb3V0XCIsIHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsIFwiQXV0aG9yaXphdGlvblwiOiBcIkJlYXJlciBVMU5uY0Zka2R6VlRVMmR3VjJSM05sOUpSRHBUVTJkd1YyUjNOVk5UWjNCWFpIYzJYMU5GUzFKRlZBPT1cIixcclxuICAgICAgICBcImNoYXJzZXRcIjogXCJ1dGYtOFwiXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IHNlYXJjaFBhcmFtcyxcclxuICAgIH0pLnRoZW4oKHIpID0+IHIudGV4dCgpKVxyXG4gICAgICAudGhlbigodGV4dCkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwibWVzc2FnZVwiICsgdGV4dCk7XHJcbiAgICAgICAgaWYgKHRleHQubGVuZ3RoID09IDQpIHtcclxuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vcC1jb2RlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJmYWlsZWQgdG8gbG9nb3V0XCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yOiBcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICB9XHJcblxyXG5cclxufVxyXG4iXX0=