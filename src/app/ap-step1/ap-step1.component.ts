import { Component, OnInit } from "@angular/core";

import { ap_step1_service } from "./ap-step1-service";
import { ap_limit_service } from "./ap-limit-service";
import { registerElement } from "nativescript-angular/element-registry";
import {
    Calendar,
    SELECTION_MODE,
    DISPLAY_MODE,
    CalendarEvent,
    Appearance,
    SCROLL_ORIENTATION,
    CalendarSubtitle,
    Settings
} from "nativescript-fancy-calendar";
import * as Toast from "nativescript-toast";
import { AppointmentComponent } from "../appointment/appointment.component";
import * as appSettings from "tns-core-modules/application-settings";
import * as Connectivity from "tns-core-modules/connectivity";
import { DatePipe } from "@angular/common";
registerElement("Calendar", () => Calendar);

@Component({
    selector: "ns-ap-step1",
    templateUrl: "./ap-step1.component.html",
    styleUrls: ["./ap-step1.component.css"],
    providers: [DatePipe, ap_step1_service,ap_limit_service],
    moduleId: module.id
})
export class ApStep1Component implements OnInit {
    selected_date = this.datePipe.transform(new Date(), "EEE, MMM dd yyyy");
    appointment_date;
    today = this.datePipe.transform(new Date(), "EEE, MMM dd yyyy");
    bookParmission = 0;
    data = [];
    settings: any;
    subtitles: CalendarSubtitle[];
    events: CalendarEvent[];
    public appearance: Appearance;
    public connectionType: string;
    res;
    i: number;
    j: number;
    department_data;

    constructor(
        private appointmentComponent: AppointmentComponent,
        private datePipe: DatePipe,
        private myService: ap_step1_service,
        private ApLimitService: ap_limit_service
    ) {}

    ngOnInit() {
        this.oldAppointment();

        this.extractDepartments();
    }

    public calendarLoaded(event) {
        this.settings = <Settings>{
            displayMode: DISPLAY_MODE.MONTH,
            scrollOrientation: SCROLL_ORIENTATION.HORIZONTAL,
            selectionMode: SELECTION_MODE.SINGLE,
            firstWeekday: 0,
            maximumDate: new Date(
                new Date().getFullYear(),
                new Date().getMonth() + 4,
                0
            ),
            minimumDate: new Date(
                new Date().getFullYear(),
                new Date().getMonth()
            )
        };

        this.appearance = <Appearance>{
            weekdayTextColor: "#ffffff",
            headerTitleColor: "#000000",
            eventColor: "white",
            selectionColor: "#B83744",
            hasBorder: true,
            todayColor: "#cccccc",
            todaySelectionColor: "#9e9e9e",
            borderRadius: 20
        };
    }

    extractDepartments() {
        this.myService.getData().subscribe(
            result => {
                
                this.department_data = result;
            },
            error => {
                console.log(error);
            }
        );
    }
    CheckApLimit() {
        this.ApLimitService.getData().subscribe(
            result => {
                console.log(result);

            },
            error => {
                console.log(error);
            }
        );
    }

    onItemTap(args) {
        //console.log("You tapped: old appointment ===> " + this.data[args.index]);
    }

    //  Manual old apointment Data
    oldAppointment() {
        this.data.push({
            day: "Wed",
            date: "NOV 29",
            doctor_name: "Dr. Arun Kumar P",
            dept: "Cardiology"
        });
        this.data.push({
            day: "Mon",
            date: "JUN 03",
            doctor_name: "Dr. Deepak R",
            dept: "Dental"
        });
        this.data.push({
            day: "Fri",
            date: "MAY 11",
            doctor_name: "Dr. Santhosh",
            dept: "Neurology"
        });
    }

    public dateSelected(event) {
        let date: Date;
        date = event.data.date;
        this.selected_date = this.datePipe.transform(date, "EEE, MMM dd yyyy");

        if (this.datePipe.transform(date, "EEE") == "Sun") {
            this.bookParmission = 1;
        } else if (this.today == this.selected_date) {
            this.bookParmission = 2;
        } else if (
            new Date(
                new Date().getFullYear(),
                new Date().getMonth(),
                new Date().getDate() + 8
            ) <= new Date(date.getFullYear(), date.getMonth(), date.getDate())
        ) {
            this.bookParmission = 3;
        } else if (
            new Date(
                new Date().getFullYear(),
                new Date().getMonth(),
                new Date().getDate()
            ) < new Date(date.getFullYear(), date.getMonth(), date.getDate())
        ) {
            this.appointment_date = this.datePipe.transform(date, "dd-MM-yyyy");
            this.bookParmission = 4;
        } else {
            this.bookParmission = 5;
        }

        this.appearance = <Appearance>{
            weekdayTextColor: "#000000",
            headerTitleColor: "#000000",
            eventColor: "white",
            hasBorder: true,
            todayColor: "#cccccc",
            todaySelectionColor: "#9e9e9e",
            borderRadius: 20,
            selectionColor: "#B83744"
        };
    }

    bookAppointment() {
        if (this.bookParmission == 0) {
            var toast = Toast.makeText(
                "Please select a date to book appointment"
            );
            toast.show();
        } else if (this.bookParmission == 1) {
            var toast = Toast.makeText(
                "No appointment booking is allowed on Sunday"
            );
            toast.show();
        } else if (this.bookParmission == 2) {
            var toast = Toast.makeText(
                "Cannot book appointment for current date"
            );
            toast.show();
        } else if (this.bookParmission == 3) {
            var toast = Toast.makeText(
                "Appointment allowed only upto 7 days in advance from current date"
            );
            toast.show();
        } else if (this.bookParmission == 4) {
            this.connectionType = this.connectionToString(
                Connectivity.getConnectionType()
            );
            if (this.connectionType == "0") {
                var toast = Toast.makeText(
                    "Cannot connect to network. Try again later!"
                );
                toast.show();
            } else if (this.connectionType == "1") {


                this.CheckApLimit();
                appSettings.setString(
                    "department_data",
                    JSON.stringify(this.department_data)
                );

                appSettings.setString(
                    "appointment_date",
                    this.appointment_date
                );
                this.appointmentComponent.step2();
            }
        } else if (this.bookParmission == 5) {
            var toast = Toast.makeText(
                "Cannot book appointment for past days. Please choose valid date"
            );
            toast.show();
        } else {
            var toast = Toast.makeText(
                "Please select a date to book appointment"
            );
            toast.show();
        }
    }
    public connectionToString(connectionType: number): string {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "0";
            case Connectivity.connectionType.wifi:
                return "1";
            case Connectivity.connectionType.mobile:
                return "1";
            default:
                return "0";
        }
    }
}

/*
 *
 *    Notes
 *    ~~~~~
 *
 *   0 - select date
 *   1 - sunday
 *   2 - current date
 *   3 - 7 days
 *   4 - ------- 0 ------  Cannot connect to network. Try again later!
 *               1 ------  successful appointment date , next page
 *   5 - past
 */

/*
 *
 *    Remaining Work
 *    ~~~~~~~~~~~~~~
 *
 *   Cannot book more than 3 appointments for the same date (remaining work)
 *   Old Appointment history functionality
 *
 */
