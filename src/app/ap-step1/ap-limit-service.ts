import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "tns-core-modules/ui/page/page";
@Injectable()
export class ap_limit_service {
    private serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";

    constructor(private http: HttpClient) {}

    getData() {
        let headers = this.createRequestHeader();
        return this.http.post("https://psgimsr.ac.in/althos/ihsrest/v1/checkAppointmentLimit?op_code=O18013660&app_date=13-06-2019", {
            headers: headers
        });
    }


    private createRequestHeader() {
        console.log("inside create");
        let headers = new HttpHeaders({
            Authorization :"Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",

            "Set-Cookie":"sessionID=V3KKQMAIQHMO1RBFD4NAP3GTAV  ; expires=1559855383589",
                });

        return headers;
    }
}
