"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var ap_limit_service = /** @class */ (function () {
    function ap_limit_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    }
    ap_limit_service.prototype.getData = function () {
        var headers = this.createRequestHeader();
        return this.http.post("https://psgimsr.ac.in/althos/ihsrest/v1/checkAppointmentLimit?op_code=O18013660&app_date=13-06-2019", {
            headers: headers
        });
    };
    ap_limit_service.prototype.createRequestHeader = function () {
        console.log("inside create");
        var headers = new http_1.HttpHeaders({
            Authorization: "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Set-Cookie": "sessionID=V3KKQMAIQHMO1RBFD4NAP3GTAV  ; expires=1559855383589",
        });
        return headers;
    };
    ap_limit_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ap_limit_service);
    return ap_limit_service;
}());
exports.ap_limit_service = ap_limit_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXAtbGltaXQtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwLWxpbWl0LXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBRy9EO0lBR0ksMEJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGNUIsY0FBUyxHQUFHLDBDQUEwQyxDQUFDO0lBRXhCLENBQUM7SUFFeEMsa0NBQU8sR0FBUDtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUdBQXFHLEVBQUU7WUFDekgsT0FBTyxFQUFFLE9BQU87U0FDbkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUdPLDhDQUFtQixHQUEzQjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDN0IsSUFBSSxPQUFPLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzFCLGFBQWEsRUFBRSxxRUFBcUU7WUFFcEYsWUFBWSxFQUFDLCtEQUErRDtTQUN2RSxDQUFDLENBQUM7UUFFWCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBdEJRLGdCQUFnQjtRQUQ1QixpQkFBVSxFQUFFO3lDQUlpQixpQkFBVTtPQUgzQixnQkFBZ0IsQ0F1QjVCO0lBQUQsdUJBQUM7Q0FBQSxBQXZCRCxJQXVCQztBQXZCWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlXCI7XG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgYXBfbGltaXRfc2VydmljZSB7XG4gICAgcHJpdmF0ZSBzZXJ2ZXJVcmwgPSBcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9cIjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkge31cblxuICAgIGdldERhdGEoKSB7XG4gICAgICAgIGxldCBoZWFkZXJzID0gdGhpcy5jcmVhdGVSZXF1ZXN0SGVhZGVyKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChcImh0dHBzOi8vcHNnaW1zci5hYy5pbi9hbHRob3MvaWhzcmVzdC92MS9jaGVja0FwcG9pbnRtZW50TGltaXQ/b3BfY29kZT1PMTgwMTM2NjAmYXBwX2RhdGU9MTMtMDYtMjAxOVwiLCB7XG4gICAgICAgICAgICBoZWFkZXJzOiBoZWFkZXJzXG4gICAgICAgIH0pO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBjcmVhdGVSZXF1ZXN0SGVhZGVyKCkge1xuICAgICAgICBjb25zb2xlLmxvZyhcImluc2lkZSBjcmVhdGVcIik7XG4gICAgICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICAgICAgIEF1dGhvcml6YXRpb24gOlwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxuXG4gICAgICAgICAgICBcIlNldC1Db29raWVcIjpcInNlc3Npb25JRD1WM0tLUU1BSVFITU8xUkJGRDROQVAzR1RBViAgOyBleHBpcmVzPTE1NTk4NTUzODM1ODlcIixcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gaGVhZGVycztcbiAgICB9XG59XG4iXX0=