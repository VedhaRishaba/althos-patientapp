import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "tns-core-modules/ui/page/page";
@Injectable()
export class ap_step1_service {
    private serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";

    constructor(private http: HttpClient) {}

    getData() {
        let headers = this.createRequestHeader();
        return this.http.get(this.serverUrl + "getAllDepartments?", {
            headers: headers
        });
    }
    private createRequestHeader() {
        console.log("inside create");
        let headers = new HttpHeaders({
            Authorization:
                "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json"
        });

        return headers;
    }
}
