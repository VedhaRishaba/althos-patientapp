"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var hp_service_1 = require("./hp-service");
var HpStep1Component = /** @class */ (function () {
    function HpStep1Component(Hp_service) {
        this.Hp_service = Hp_service;
        this.data = [];
    }
    HpStep1Component.prototype.ngOnInit = function () {
        this.data.push({ code: "MAHC05", name: "Comprehensive Diabetic Check Up" });
        this.data.push({ code: "MAHC02", name: "Executive Health Check Up" });
        this.data.push({ code: "MAHC01", name: "Master Health check Up" });
        this.data.push({ code: "MAHC03", name: "Senior Citizen Health Check Up - For Women" });
        this.data.push({ code: "MAHC11", name: "Senior Citizen Health Check Up - For Men" });
        this.data.push({ code: "MAHC12", name: "Sweet Check Up" });
    };
    HpStep1Component.prototype.onItemTap = function (args) {
        console.log("You tapped: " + this.data[args.index].code);
    };
    HpStep1Component = __decorate([
        core_1.Component({
            selector: 'ns-hp-step1',
            templateUrl: './hp-step1.component.html',
            styleUrls: ['./hp-step1.component.css'],
            providers: [hp_service_1.hp_service],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [hp_service_1.hp_service])
    ], HpStep1Component);
    return HpStep1Component;
}());
exports.HpStep1Component = HpStep1Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHAtc3RlcDEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaHAtc3RlcDEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELDJDQUF5QztBQVF6QztJQUlFLDBCQUFxQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBRjNDLFNBQUksR0FBRyxFQUFFLENBQUM7SUFFcUMsQ0FBQztJQUVoRCxtQ0FBUSxHQUFSO1FBRUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxpQ0FBaUMsRUFBRSxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSwyQkFBMkIsRUFBRSxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSx3QkFBd0IsRUFBRSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSw0Q0FBNEMsRUFBRSxDQUFDLENBQUM7UUFDdkYsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSwwQ0FBMEMsRUFBRSxDQUFDLENBQUM7UUFDckYsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFFN0QsQ0FBQztJQUNELG9DQUFTLEdBQVQsVUFBVSxJQUFJO1FBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFM0QsQ0FBQztJQW5CVSxnQkFBZ0I7UUFQNUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFdBQVcsRUFBRSwyQkFBMkI7WUFDeEMsU0FBUyxFQUFFLENBQUMsMEJBQTBCLENBQUM7WUFDdkMsU0FBUyxFQUFDLENBQUMsdUJBQVUsQ0FBQztZQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzt5Q0FLaUMsdUJBQVU7T0FKaEMsZ0JBQWdCLENBcUI1QjtJQUFELHVCQUFDO0NBQUEsQUFyQkQsSUFxQkM7QUFyQlksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGhwX3NlcnZpY2V9IGZyb20gJy4vaHAtc2VydmljZSc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1ocC1zdGVwMScsXG4gIHRlbXBsYXRlVXJsOiAnLi9ocC1zdGVwMS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2hwLXN0ZXAxLmNvbXBvbmVudC5jc3MnXSxcbiAgcHJvdmlkZXJzOltocF9zZXJ2aWNlXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5leHBvcnQgY2xhc3MgSHBTdGVwMUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgZGF0YSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKCBwcml2YXRlIEhwX3NlcnZpY2UgOmhwX3NlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gICAgdGhpcy5kYXRhLnB1c2goeyBjb2RlOiBcIk1BSEMwNVwiLCBuYW1lOiBcIkNvbXByZWhlbnNpdmUgRGlhYmV0aWMgQ2hlY2sgVXBcIiB9KTtcbiAgICB0aGlzLmRhdGEucHVzaCh7IGNvZGU6IFwiTUFIQzAyXCIsIG5hbWU6IFwiRXhlY3V0aXZlIEhlYWx0aCBDaGVjayBVcFwiIH0pO1xuICAgIHRoaXMuZGF0YS5wdXNoKHsgY29kZTogXCJNQUhDMDFcIiwgbmFtZTogXCJNYXN0ZXIgSGVhbHRoIGNoZWNrIFVwXCIgfSk7XG4gICAgdGhpcy5kYXRhLnB1c2goeyBjb2RlOiBcIk1BSEMwM1wiLCBuYW1lOiBcIlNlbmlvciBDaXRpemVuIEhlYWx0aCBDaGVjayBVcCAtIEZvciBXb21lblwiIH0pO1xuICAgIHRoaXMuZGF0YS5wdXNoKHsgY29kZTogXCJNQUhDMTFcIiwgbmFtZTogXCJTZW5pb3IgQ2l0aXplbiBIZWFsdGggQ2hlY2sgVXAgLSBGb3IgTWVuXCIgfSk7XG4gICAgdGhpcy5kYXRhLnB1c2goeyBjb2RlOiBcIk1BSEMxMlwiLCBuYW1lOiBcIlN3ZWV0IENoZWNrIFVwXCIgfSk7XG5cbiAgfVxuICBvbkl0ZW1UYXAoYXJncykge1xuICAgIGNvbnNvbGUubG9nKFwiWW91IHRhcHBlZDogXCIgKyB0aGlzLmRhdGFbYXJncy5pbmRleF0uY29kZSk7XG5cbiAgfVxuXG59XG4iXX0=