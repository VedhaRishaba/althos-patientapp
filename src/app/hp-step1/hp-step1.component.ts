import { Component, OnInit } from '@angular/core';
import { hp_service} from './hp-service';
@Component({
  selector: 'ns-hp-step1',
  templateUrl: './hp-step1.component.html',
  styleUrls: ['./hp-step1.component.css'],
  providers:[hp_service],
  moduleId: module.id,
})
export class HpStep1Component implements OnInit {

  data = [];

  constructor( private Hp_service :hp_service) { }

  ngOnInit() {

    this.data.push({ code: "MAHC05", name: "Comprehensive Diabetic Check Up" });
    this.data.push({ code: "MAHC02", name: "Executive Health Check Up" });
    this.data.push({ code: "MAHC01", name: "Master Health check Up" });
    this.data.push({ code: "MAHC03", name: "Senior Citizen Health Check Up - For Women" });
    this.data.push({ code: "MAHC11", name: "Senior Citizen Health Check Up - For Men" });
    this.data.push({ code: "MAHC12", name: "Sweet Check Up" });

  }
  onItemTap(args) {
    console.log("You tapped: " + this.data[args.index].code);

  }

}
