"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var hp_service = /** @class */ (function () {
    function hp_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/";
    }
    hp_service.prototype.getData = function () {
        var headers = this.createRequestHeader();
        return this.http.get(this.serverUrl + "getHealthPlans", { headers: headers });
    };
    hp_service.prototype.createRequestHeader = function () {
        console.log("inside create");
        var headers = new http_1.HttpHeaders({
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
            "Content-Type": "application/json",
        });
        return headers;
    };
    hp_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], hp_service);
    return hp_service;
}());
exports.hp_service = hp_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHAtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhwLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBTy9EO0lBS0ksb0JBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGMUIsY0FBUyxHQUFHLDBDQUEwQyxDQUFDO0lBRXpCLENBQUM7SUFFekMsNEJBQU8sR0FBUDtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBQyxnQkFBZ0IsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFDTyx3Q0FBbUIsR0FBM0I7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdCLElBQUksT0FBTyxHQUFHLElBQUksa0JBQVcsQ0FBQztZQUMxQixlQUFlLEVBQUUscUVBQXFFO1lBQ3RGLGNBQWMsRUFBRSxrQkFBa0I7U0FFcEMsQ0FBQyxDQUFDO1FBRUosT0FBTyxPQUFPLENBQUM7SUFDZixDQUFDO0lBcEJJLFVBQVU7UUFIdEIsaUJBQVUsRUFFVjt5Q0FNNkIsaUJBQVU7T0FMM0IsVUFBVSxDQXFCckI7SUFBRCxpQkFBQztDQUFBLEFBckJGLElBcUJFO0FBckJXLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xuXG5cbkBJbmplY3RhYmxlKFxuXG4pXG5leHBvcnQgY2xhc3MgaHBfc2VydmljZXtcblxuXG4gICAgICBwcml2YXRlIHNlcnZlclVybCA9IFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL1wiO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IH1cblxuICAgIGdldERhdGEoKSB7XG4gICAgICAgIGxldCBoZWFkZXJzID0gdGhpcy5jcmVhdGVSZXF1ZXN0SGVhZGVyKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHRoaXMuc2VydmVyVXJsK1wiZ2V0SGVhbHRoUGxhbnNcIiwgeyBoZWFkZXJzOiBoZWFkZXJzIH0pO1xuICAgIH1cbiAgICBwcml2YXRlIGNyZWF0ZVJlcXVlc3RIZWFkZXIoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiaW5zaWRlIGNyZWF0ZVwiKTtcbiAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCIgOlwiQmVhcmVyIFUxTm5jRmRrZHpWVFUyZHdWMlIzTmw5SlJEcFRVMmR3VjJSM05WTlRaM0JYWkhjMlgxTkZTMUpGVkE9PVwiLFxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gICAgICAgICAvLyAgIFwic2V0LWNvb2tpZVwiOlxuICAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIGhlYWRlcnM7XG4gICAgICAgIH1cbiB9XG4iXX0=