"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var appSettings = require("tns-core-modules/application-settings");
var LabComponent = /** @class */ (function () {
    function LabComponent(router) {
        this.router = router;
        this.passData = 0;
        this.one = "~/images/one.png";
        this.two = "~/images/two_gray.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.currentStep = 1;
    }
    LabComponent.prototype.ngOnInit = function () { };
    LabComponent.prototype.step1 = function () {
        this.one = "~/images/one.png";
        this.two = "~/images/two_gray.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
        this.currentStep = 1;
    };
    LabComponent.prototype.step2 = function () {
        this.currentStep = 2;
        this.one = "~/images/checked.png";
        this.two = "~/images/two.png";
        this.three = "~/images/three_gray.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
    };
    LabComponent.prototype.step3 = function () {
        this.currentStep = 3;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/three.png";
        this.four = "~/images/four_gray.png";
        this.five = "~/images/five_gray.png";
    };
    LabComponent.prototype.step4 = function () {
        this.currentStep = 4;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/checked.png";
        this.four = "~/images/four.png";
        this.five = "~/images/five_gray.png";
    };
    LabComponent.prototype.step5 = function () {
        this.currentStep = 5;
        this.one = "~/images/checked.png";
        this.two = "~/images/checked.png";
        this.three = "~/images/checked.png";
        this.four = "~/images/checked.png";
        this.five = "~/images/five.png";
    };
    LabComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    LabComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    LabComponent = __decorate([
        core_1.Component({
            selector: 'ns-lab',
            templateUrl: './lab.component.html',
            styleUrls: ['./lab.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], LabComponent);
    return LabComponent;
}());
exports.LabComponent = LabComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFiLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxhYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQscURBQXVEO0FBQ3ZELDBDQUF5QztBQUN6QyxtRUFBcUU7QUFRckU7SUFVSSxzQkFBb0IsTUFBYTtRQUFiLFdBQU0sR0FBTixNQUFNLENBQU87UUFSakMsYUFBUSxHQUFFLENBQUMsQ0FBQztRQUNaLFFBQUcsR0FBQyxrQkFBa0IsQ0FBQztRQUN2QixRQUFHLEdBQUMsdUJBQXVCLENBQUM7UUFDNUIsVUFBSyxHQUFDLHlCQUF5QixDQUFDO1FBQ2hDLFNBQUksR0FBQyx3QkFBd0IsQ0FBQztRQUM5QixTQUFJLEdBQUMsd0JBQXdCLENBQUM7UUFDOUIsZ0JBQVcsR0FBRyxDQUFDLENBQUM7SUFFcUIsQ0FBQztJQUV0QywrQkFBUSxHQUFSLGNBQWEsQ0FBQztJQUVkLDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsR0FBRyxHQUFDLGtCQUFrQixDQUFDO1FBQzVCLElBQUksQ0FBQyxHQUFHLEdBQUMsdUJBQXVCLENBQUM7UUFDakMsSUFBSSxDQUFDLEtBQUssR0FBQyx5QkFBeUIsQ0FBQztRQUNyQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVELDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsa0JBQWtCLENBQUM7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBQyx5QkFBeUIsQ0FBQztRQUNyQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7SUFDckMsQ0FBQztJQUVELDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsc0JBQXNCLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBQyxvQkFBb0IsQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxHQUFDLHdCQUF3QixDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7SUFDckMsQ0FBQztJQUVELDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsc0JBQXNCLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBQyxzQkFBc0IsQ0FBQztRQUNsQyxJQUFJLENBQUMsSUFBSSxHQUFDLG1CQUFtQixDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUMsd0JBQXdCLENBQUM7SUFDckMsQ0FBQztJQUVELDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsR0FBRyxHQUFDLHNCQUFzQixDQUFDO1FBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUMsc0JBQXNCLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBQyxzQkFBc0IsQ0FBQztRQUNsQyxJQUFJLENBQUMsSUFBSSxHQUFDLHNCQUFzQixDQUFDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUMsbUJBQW1CLENBQUM7SUFDaEMsQ0FBQztJQUdILDZCQUFNLEdBQU47UUFBQSxpQkFVQztRQVRDLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDZCxPQUFPLEVBQUUsMEJBQTBCO1lBQ25DLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLElBQUk7U0FDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07WUFDWixJQUFJLE1BQU0sRUFBRTtnQkFDVixLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxxQ0FBYyxHQUFkO1FBQUEsaUJBZ0NDO1FBL0JDLElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO1FBQ0YsSUFBTSxPQUFPLEdBQUc7WUFDaEIsT0FBTyxFQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDO1NBQ3pDLENBQUE7UUFDRCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7WUFDaEQsT0FBTyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWIsS0FBSyxDQUFDLGdEQUFnRCxFQUFFO1lBQ3RELE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLGNBQWMsRUFBRSxtQ0FBbUMsRUFBRSxlQUFlLEVBQUUscUVBQXFFO2dCQUMzSSxTQUFTLEVBQUUsT0FBTzthQUNuQjtZQUNELElBQUksRUFBRSxZQUFZO1NBQ25CLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQVIsQ0FBUSxDQUFDO2FBQ3JCLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUM5QixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUNwQixLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN2QztpQkFDSTtnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDakM7UUFDSCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxDQUFDO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQXZHVSxZQUFZO1FBTnhCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsc0JBQXNCO1lBQ25DLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ2xDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQVc2QixlQUFNO09BVnhCLFlBQVksQ0F5R3hCO0lBQUQsbUJBQUM7Q0FBQSxBQXpHRCxJQXlHQztBQXpHWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICducy1sYWInLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9sYWIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2xhYi5jb21wb25lbnQuY3NzJ10sXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxufSlcclxuZXhwb3J0IGNsYXNzIExhYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgcGFzc0RhdGEgPTA7XHJcbiAgICBvbmU9XCJ+L2ltYWdlcy9vbmUucG5nXCI7XHJcbiAgICB0d289XCJ+L2ltYWdlcy90d29fZ3JheS5wbmdcIjtcclxuICAgIHRocmVlPVwifi9pbWFnZXMvdGhyZWVfZ3JheS5wbmdcIjtcclxuICAgIGZvdXI9XCJ+L2ltYWdlcy9mb3VyX2dyYXkucG5nXCI7XHJcbiAgICBmaXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xyXG4gICAgY3VycmVudFN0ZXAgPSAxO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOlJvdXRlcikgeyB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7IH1cclxuXHJcbiAgICBzdGVwMSgpe1xyXG4gICAgICB0aGlzLm9uZT1cIn4vaW1hZ2VzL29uZS5wbmdcIjtcclxuICAgICAgdGhpcy50d289XCJ+L2ltYWdlcy90d29fZ3JheS5wbmdcIjtcclxuICAgICAgdGhpcy50aHJlZT1cIn4vaW1hZ2VzL3RocmVlX2dyYXkucG5nXCI7XHJcbiAgICAgIHRoaXMuZm91cj1cIn4vaW1hZ2VzL2ZvdXJfZ3JheS5wbmdcIjtcclxuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xyXG4gICAgICB0aGlzLmN1cnJlbnRTdGVwID0gMTtcclxuICAgIH1cclxuXHJcbiAgICBzdGVwMigpe1xyXG4gICAgICB0aGlzLmN1cnJlbnRTdGVwID0gMjtcclxuICAgICAgdGhpcy5vbmU9XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xyXG4gICAgICB0aGlzLnR3bz1cIn4vaW1hZ2VzL3R3by5wbmdcIjtcclxuICAgICAgdGhpcy50aHJlZT1cIn4vaW1hZ2VzL3RocmVlX2dyYXkucG5nXCI7XHJcbiAgICAgIHRoaXMuZm91cj1cIn4vaW1hZ2VzL2ZvdXJfZ3JheS5wbmdcIjtcclxuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZV9ncmF5LnBuZ1wiO1xyXG4gICAgfVxyXG5cclxuICAgIHN0ZXAzKCl7XHJcbiAgICAgIHRoaXMuY3VycmVudFN0ZXAgPSAzO1xyXG4gICAgICB0aGlzLm9uZT1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XHJcbiAgICAgIHRoaXMudHdvPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy50aHJlZT1cIn4vaW1hZ2VzL3RocmVlLnBuZ1wiO1xyXG4gICAgICB0aGlzLmZvdXI9XCJ+L2ltYWdlcy9mb3VyX2dyYXkucG5nXCI7XHJcbiAgICAgIHRoaXMuZml2ZT1cIn4vaW1hZ2VzL2ZpdmVfZ3JheS5wbmdcIjtcclxuICAgIH1cclxuXHJcbiAgICBzdGVwNCgpe1xyXG4gICAgICB0aGlzLmN1cnJlbnRTdGVwID0gNDtcclxuICAgICAgdGhpcy5vbmU9XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xyXG4gICAgICB0aGlzLnR3bz1cIn4vaW1hZ2VzL2NoZWNrZWQucG5nXCI7XHJcbiAgICAgIHRoaXMudGhyZWU9XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xyXG4gICAgICB0aGlzLmZvdXI9XCJ+L2ltYWdlcy9mb3VyLnBuZ1wiO1xyXG4gICAgICB0aGlzLmZpdmU9XCJ+L2ltYWdlcy9maXZlX2dyYXkucG5nXCI7XHJcbiAgICB9XHJcblxyXG4gICAgc3RlcDUoKXtcclxuICAgICAgdGhpcy5jdXJyZW50U3RlcCA9IDU7XHJcbiAgICAgIHRoaXMub25lPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy50d289XCJ+L2ltYWdlcy9jaGVja2VkLnBuZ1wiO1xyXG4gICAgICB0aGlzLnRocmVlPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy5mb3VyPVwifi9pbWFnZXMvY2hlY2tlZC5wbmdcIjtcclxuICAgICAgdGhpcy5maXZlPVwifi9pbWFnZXMvZml2ZS5wbmdcIjtcclxuICAgIH1cclxuXHJcbiAgICBcclxuICBsb2dvdXQoKSB7XHJcbiAgICBkaWFsb2dzLmNvbmZpcm0oe1xyXG4gICAgICBtZXNzYWdlOiBcIkRvIHlvdSB3YW50IHRvIExvZ291dCA/IFwiLFxyXG4gICAgICBva0J1dHRvblRleHQ6IFwiWUVTXCIsXHJcbiAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiTk9cIixcclxuICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgIHRoaXMubG9nb3V0YWN0aXZpdHkoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIGxvZ291dGFjdGl2aXR5KCkge1xyXG4gICAgY29uc3Qgb2JqZWN0MSA9IHtcclxuICAgICAgb3BfY29kZTogXCJcIlxyXG4gICAgfTtcclxuICAgIGNvbnN0IG9iamVjdDIgPSB7XHJcbiAgICBvcF9jb2RlOiAgYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwib3AtY29kZVwiKVxyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VhcmNoUGFyYW1zID0gT2JqZWN0LmtleXMob2JqZWN0MSkubWFwKChrZXkpID0+IHtcclxuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9iamVjdDJba2V5XSk7XHJcbiAgICB9KS5qb2luKCcmJyk7XHJcblxyXG4gICAgZmV0Y2goXCJodHRwczovL3BzZ2ltc3IuYWMuaW4vYWx0aG9zL2loc3Jlc3QvdjEvbG9nb3V0XCIsIHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsIFwiQXV0aG9yaXphdGlvblwiOiBcIkJlYXJlciBVMU5uY0Zka2R6VlRVMmR3VjJSM05sOUpSRHBUVTJkd1YyUjNOVk5UWjNCWFpIYzJYMU5GUzFKRlZBPT1cIixcclxuICAgICAgICBcImNoYXJzZXRcIjogXCJ1dGYtOFwiXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IHNlYXJjaFBhcmFtcyxcclxuICAgIH0pLnRoZW4oKHIpID0+IHIudGV4dCgpKVxyXG4gICAgICAudGhlbigodGV4dCkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwibWVzc2FnZVwiICsgdGV4dCk7XHJcbiAgICAgICAgaWYgKHRleHQubGVuZ3RoID09IDQpIHtcclxuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9vcC1jb2RlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJmYWlsZWQgdG8gbG9nb3V0XCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSkuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yOiBcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICB9XHJcblxyXG59XHJcbiJdfQ==