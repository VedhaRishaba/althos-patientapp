import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule ,HttpClient} from "@angular/common/http";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptFormsModule} from "nativescript-angular/forms";
import { AppComponent } from "./app.component";


import { OpCodeComponent } from './op-code/op-code.component';
import { ForgotOPComponent } from './forgot-op/forgot-op.component';
import { PasswordComponent } from './password/password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { OtpComponent } from './otp/otp.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { FaqComponent } from './faq/faq.component';
import { PoliciesComponent } from './policies/policies.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ProfileComponent } from './profile/profile.component';
import { CheckupPlanComponent } from './checkup-plan/checkup-plan.component';
import { HpStep1Component } from './hp-step1/hp-step1.component';
import { HpStep2Component } from './hp-step2/hp-step2.component';
import { HpStep3Component } from './hp-step3/hp-step3.component';
import { HpStep4Component } from './hp-step4/hp-step4.component';
import { HpStep5Component } from './hp-step5/hp-step5.component';
import { RecordsComponent } from './records/records.component';
import { LabComponent } from './lab/lab.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { ApStep1Component } from './ap-step1/ap-step1.component';
import { ApStep2Component } from './ap-step2/ap-step2.component';
import { ApStep3Component } from './ap-step3/ap-step3.component';
import { ApStep4Component } from './ap-step4/ap-step4.component';
import { ApStep5Component } from './ap-step5/ap-step5.component';
import { LabStep1Component } from './lab-step1/lab-step1.component';
import { LabStep2Component } from './lab-step2/lab-step2.component';
import { LabStep3Component } from './lab-step3/lab-step3.component';
import { LabStep4Component } from './lab-step4/lab-step4.component';
import { LabStep5Component } from './lab-step5/lab-step5.component';
import { TermsComponent } from './terms/terms.component';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        HttpClientModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        AppComponent,
        OpCodeComponent,
        ForgotOPComponent,
        PasswordComponent,
        ForgotPasswordComponent,
        OtpComponent,
        ResetPasswordComponent,
        FaqComponent,
        PoliciesComponent,
        AboutComponent,
        ContactComponent,
        ProfileComponent,
        CheckupPlanComponent,
        HpStep1Component,
        HpStep2Component,
        HpStep3Component,
        HpStep4Component,
        HpStep5Component,
        RecordsComponent,
        LabComponent,
        AppointmentComponent,
        ApStep1Component,
        ApStep2Component,
        ApStep3Component,
        ApStep4Component,
        ApStep5Component,
        LabStep1Component,
        LabStep2Component,
        LabStep3Component,
        LabStep4Component,
        LabStep5Component,
        TermsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
