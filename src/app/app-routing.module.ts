import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AboutComponent } from './about/about.component';
import { ApStep1Component } from './ap-step1/ap-step1.component';
import { ApStep2Component } from './ap-step2/ap-step2.component';
import { ApStep3Component } from './ap-step3/ap-step3.component';
import { ApStep4Component } from './ap-step4/ap-step4.component';
import { ApStep5Component} from './ap-step5/ap-step5.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { CheckupPlanComponent } from './checkup-plan/checkup-plan.component';
import { ContactComponent } from './contact/contact.component';
import { FaqComponent } from './faq/faq.component';
import { ForgotOPComponent } from './forgot-op/forgot-op.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HomeComponent } from './home/home.component';
import { HpStep1Component } from './hp-step1/hp-step1.component';
import { HpStep2Component } from './hp-step2/hp-step2.component';
import { HpStep3Component } from './hp-step3/hp-step3.component';
import { HpStep4Component } from './hp-step4/hp-step4.component';
import { HpStep5Component } from './hp-step5/hp-step5.component';
import { LabComponent } from './lab/lab.component';
import { LabStep1Component } from './lab-step1/lab-step1.component';
import { LabStep2Component} from './lab-step2/lab-step2.component';
import { LabStep3Component } from './lab-step3/lab-step3.component';
import { LabStep4Component } from './lab-step4/lab-step4.component';
import { LabStep5Component } from './lab-step5/lab-step5.component';
import { OpCodeComponent} from './op-code/op-code.component';
import { OtpComponent } from './otp/otp.component';
import { PasswordComponent } from './password/password.component';
import { PoliciesComponent } from './policies/policies.component';
import { ProfileComponent } from './profile/profile.component';
import { RecordsComponent } from './records/records.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TermsComponent } from './terms/terms.component';
import { from } from "rxjs";



const routes: Routes = [
    { path: "", redirectTo: "/op-code", pathMatch: "full" },


    { path: "about", component: AboutComponent },
    { path: "ap-step1", component: ApStep1Component },
    { path: "ap-step2", component: ApStep2Component },
    { path: "ap-step3", component: ApStep3Component },
    { path: "ap-step4", component: ApStep4Component },
    { path: "ap-step5", component: ApStep5Component },
    { path: "appointment", component: AppointmentComponent},
    { path: "checkup", component: CheckupPlanComponent},
    { path: "contact", component: ContactComponent },
    { path: "faq", component: FaqComponent },
    { path: "forgot-op", component: ForgotOPComponent },
    { path: "forgot-password", component: ForgotPasswordComponent },

    { path: "hp-step-1", component: HpStep1Component},
    { path: "hp-step-2", component: HpStep2Component},
    { path: "hp-step-3", component: HpStep3Component},
    { path: "hp-step-4", component: HpStep4Component},
    { path: "hp-step-5", component: HpStep5Component},
    { path: "lab", component: LabComponent},
    { path: "lab-step1", component: LabStep1Component },
    { path: "lab-step2", component: LabStep2Component },
    { path: "lab-step3", component: LabStep3Component },
    { path: "lab-step4", component: LabStep4Component },
    { path: "lab-step5", component: LabStep5Component },
    { path: "op-code", component: OpCodeComponent },
    { path: "otp", component: OtpComponent },
    { path: "password", component: PasswordComponent },
    { path: "policies", component: PoliciesComponent },
    { path: "profile", component: ProfileComponent },
    { path: "record", component: RecordsComponent},
    { path: "reset-password", component: ResetPasswordComponent},
    { path: "terms", component: TermsComponent},

    { path: "home", loadChildren: "~/app/home/home.module#HomeModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
