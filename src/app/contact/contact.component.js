"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var router_1 = require("@angular/router");
var email = require("nativescript-email");
var appSettings = require("tns-core-modules/application-settings");
var ContactComponent = /** @class */ (function () {
    function ContactComponent(router) {
        this.router = router;
        this.toEmail = "support@psgsoftwaretechnologies.com";
        this.subject = "Feedback-PSG Hospitals App";
    }
    ContactComponent.prototype.ngOnInit = function () { };
    ContactComponent.prototype.share = function () {
        console.log("click as been done");
        email.compose({
            subject: this.subject,
            to: [this.toEmail],
        }).then(function () {
        }, function (err) {
            alert("Error: " + err);
        });
    };
    ContactComponent.prototype.logout = function () {
        var _this = this;
        dialogs.confirm({
            message: "Do you want to Logout ? ",
            okButtonText: "YES",
            cancelButtonText: "NO",
        }).then(function (result) {
            if (result) {
                _this.logoutactivity();
            }
        });
    };
    ContactComponent.prototype.logoutactivity = function () {
        var _this = this;
        var object1 = {
            op_code: ""
        };
        var object2 = {
            op_code: appSettings.getString("op-code")
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        fetch("https://psgimsr.ac.in/althos/ihsrest/v1/logout", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA==",
                "charset": "utf-8"
            },
            body: searchParams,
        }).then(function (r) { return r.text(); })
            .then(function (text) {
            console.log("message" + text);
            if (text.length == 4) {
                _this.router.navigateByUrl('/op-code');
            }
            else {
                console.log("failed to logout");
            }
        }).catch(function (e) {
            console.log("Error: ");
            console.log(e);
        });
    };
    ContactComponent = __decorate([
        core_1.Component({
            selector: 'ns-contact',
            templateUrl: './contact.component.html',
            styleUrls: ['./contact.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], ContactComponent);
    return ContactComponent;
}());
exports.ContactComponent = ContactComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjb250YWN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxxREFBdUQ7QUFDdkQsMENBQXlDO0FBQ3pDLDBDQUE0QztBQUM1QyxtRUFBcUU7QUFRckU7SUFLRSwwQkFBb0IsTUFBYTtRQUFiLFdBQU0sR0FBTixNQUFNLENBQU87UUFIaEMsWUFBTyxHQUFDLHFDQUFxQyxDQUFDO1FBQzlDLFlBQU8sR0FBQyw0QkFBNEIsQ0FBQztJQUVELENBQUM7SUFFdEMsbUNBQVEsR0FBUixjQUFhLENBQUM7SUFFZCxnQ0FBSyxHQUFMO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2xDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDWixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUNuQixDQUFDLENBQUMsSUFBSSxDQUFDO1FBRVIsQ0FBQyxFQUFFLFVBQUEsR0FBRztZQUNKLEtBQUssQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUFBLGlCQVVDO1FBVEMsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUNkLE9BQU8sRUFBRSwwQkFBMEI7WUFDbkMsWUFBWSxFQUFFLEtBQUs7WUFDbkIsZ0JBQWdCLEVBQUUsSUFBSTtTQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNaLElBQUksTUFBTSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELHlDQUFjLEdBQWQ7UUFBQSxpQkFpQ0M7UUFoQ0MsSUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUUsRUFBRTtTQUNaLENBQUM7UUFDRixJQUFNLE9BQU8sR0FBRztZQUVkLE9BQU8sRUFBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQztTQUMzQyxDQUFBO1FBQ0QsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHO1lBQ2hELE9BQU8sa0JBQWtCLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUViLEtBQUssQ0FBQyxnREFBZ0QsRUFBRTtZQUN0RCxNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRTtnQkFDUCxjQUFjLEVBQUUsbUNBQW1DLEVBQUUsZUFBZSxFQUFFLHFFQUFxRTtnQkFDM0ksU0FBUyxFQUFFLE9BQU87YUFDbkI7WUFDRCxJQUFJLEVBQUUsWUFBWTtTQUNuQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFSLENBQVEsQ0FBQzthQUNyQixJQUFJLENBQUMsVUFBQyxJQUFJO1lBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDOUIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDdkM7aUJBQ0k7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQ2pDO1FBQ0gsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQztZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFqRVUsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUsMEJBQTBCO1lBQ3ZDLFNBQVMsRUFBRSxDQUFDLHlCQUF5QixDQUFDO1lBQ3RDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQU0yQixlQUFNO09BTHRCLGdCQUFnQixDQW1FNUI7SUFBRCx1QkFBQztDQUFBLEFBbkVELElBbUVDO0FBbkVZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgKiBhcyBlbWFpbCBmcm9tIFwibmF0aXZlc2NyaXB0LWVtYWlsXCI7XHJcbmltcG9ydCAqIGFzIGFwcFNldHRpbmdzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ25zLWNvbnRhY3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb250YWN0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb250YWN0LmNvbXBvbmVudC5jc3MnXSxcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGFjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICB0b0VtYWlsPVwic3VwcG9ydEBwc2dzb2Z0d2FyZXRlY2hub2xvZ2llcy5jb21cIjtcclxuICAgc3ViamVjdD1cIkZlZWRiYWNrLVBTRyBIb3NwaXRhbHMgQXBwXCI7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOlJvdXRlcikgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkgeyB9XHJcblxyXG4gIHNoYXJlKCl7XHJcbiAgICBjb25zb2xlLmxvZyhcImNsaWNrIGFzIGJlZW4gZG9uZVwiKTtcclxuICAgIGVtYWlsLmNvbXBvc2Uoe1xyXG4gICAgICBzdWJqZWN0OiB0aGlzLnN1YmplY3QsXHJcbiAgICAgIHRvOiBbdGhpcy50b0VtYWlsXSxcclxuICAgIH0pLnRoZW4oKCkgPT4ge1xyXG5cclxuICAgIH0sIGVyciA9PiB7XHJcbiAgICAgIGFsZXJ0KFwiRXJyb3I6IFwiICsgZXJyKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbG9nb3V0KCkge1xyXG4gICAgZGlhbG9ncy5jb25maXJtKHtcclxuICAgICAgbWVzc2FnZTogXCJEbyB5b3Ugd2FudCB0byBMb2dvdXQgPyBcIixcclxuICAgICAgb2tCdXR0b25UZXh0OiBcIllFU1wiLFxyXG4gICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIk5PXCIsXHJcbiAgICB9KS50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICB0aGlzLmxvZ291dGFjdGl2aXR5KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBsb2dvdXRhY3Rpdml0eSgpIHtcclxuICAgIGNvbnN0IG9iamVjdDEgPSB7XHJcbiAgICAgIG9wX2NvZGU6IFwiXCJcclxuICAgIH07XHJcbiAgICBjb25zdCBvYmplY3QyID0ge1xyXG4gICAgICBcclxuICAgICAgb3BfY29kZTogIGFwcFNldHRpbmdzLmdldFN0cmluZyhcIm9wLWNvZGVcIilcclxuICAgIH1cclxuICAgIGNvbnN0IHNlYXJjaFBhcmFtcyA9IE9iamVjdC5rZXlzKG9iamVjdDEpLm1hcCgoa2V5KSA9PiB7XHJcbiAgICAgIHJldHVybiBlbmNvZGVVUklDb21wb25lbnQoa2V5KSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudChvYmplY3QyW2tleV0pO1xyXG4gICAgfSkuam9pbignJicpO1xyXG5cclxuICAgIGZldGNoKFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL2xvZ291dFwiLCB7XHJcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiLCBcIkF1dGhvcml6YXRpb25cIjogXCJCZWFyZXIgVTFObmNGZGtkelZUVTJkd1YyUjNObDlKUkRwVFUyZHdWMlIzTlZOVFozQlhaSGMyWDFORlMxSkZWQT09XCIsXHJcbiAgICAgICAgXCJjaGFyc2V0XCI6IFwidXRmLThcIlxyXG4gICAgICB9LFxyXG4gICAgICBib2R5OiBzZWFyY2hQYXJhbXMsXHJcbiAgICB9KS50aGVuKChyKSA9PiByLnRleHQoKSlcclxuICAgICAgLnRoZW4oKHRleHQpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIm1lc3NhZ2VcIiArIHRleHQpO1xyXG4gICAgICAgIGlmICh0ZXh0Lmxlbmd0aCA9PSA0KSB7XHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvb3AtY29kZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiZmFpbGVkIHRvIGxvZ291dFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pLmNhdGNoKChlKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJFcnJvcjogXCIpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGUpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgfVxyXG5cclxufVxyXG4iXX0=