"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs = require("tns-core-modules/ui/dialogs");
var reset_password_service_1 = require("~/app/reset-password/reset-password-service");
var router_1 = require("@angular/router");
var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(myservice, router) {
        this.myservice = myservice;
        this.router = router;
        this.new_password = "";
        this.rnewpassword = "";
        this.validpass = true;
        this.passfield = true;
        this.validrpass = true;
        this.rpassfield = true;
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
    };
    ResetPasswordComponent.prototype.validation = function () {
        this.checkpassnull = this.new_password.match(this.reg);
        this.checkrpassnull = this.new_password.length == 10;
        console.log("newpassword : " + this.new_password + "Re-newpassword :" + this.rnewpassword);
        if (this.new_password != "" && this.rnewpassword != "") {
            if (this.rnewpassword.length <= 10 && this.rnewpassword.match(this.new_password) && this.new_password.length <= 9 && this.new_password.match(this.new_password)) {
                dialogs.alert("Valid Mobile Number ");
            }
            else if (!this.checkpassnull || this.new_password.length != 10) {
                this.validpass = false;
            }
            else if (!this.checkrpassnull || this.rnewpassword.length != 10) {
                this.validrpass = false;
            }
            else {
                this.validpass = false;
                this.validrpass = false;
            }
        }
        else if (this.new_password == "" && this.rnewpassword == "") {
            this.passfield = false;
            this.rpassfield = false;
            console.log("both field empty");
        }
        else if (this.new_password == "") {
            this.passfield = false;
            console.log("New password field is empty");
        }
        else {
            this.rpassfield = false;
            console.log("Re-type password field is empty");
        }
    };
    ResetPasswordComponent.prototype.returnPass = function (args) {
        if (this.passfield == false)
            this.passfield = true;
        if (this.validpass == false)
            this.validpass = true;
    };
    ResetPasswordComponent.prototype.returnRpass = function (args) {
        if (this.rpassfield == false)
            this.rpassfield = true;
        if (this.validrpass == false)
            this.validrpass = true;
    };
    ResetPasswordComponent.prototype.getdetails = function () {
        var _this = this;
        var object1 = {
            op_code: "",
            password: "",
        };
        var object2 = {
            op_code: "O19032823",
            password: "sFPLgG7MmD5tN0vFbXPqMQ==",
        };
        var searchParams = Object.keys(object1).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
        }).join('&');
        this.myservice
            .postData(searchParams)
            .subscribe(function (response) {
            // if(response.status==200){
            //  this.isItemVisible = true;
            // this.message=JSON.stringify(response);
            //this.sid=response.headers.get('Set-cookie');
            console.log(response);
            dialogs.alert("password changed");
            //console.log("cookie "+this.sid);
            _this.router.navigateByUrl['/op-code'];
            //}else{
            //console.log(""+JSON.stringify(response));
            //var toast = Toast.makeText("");
            //  toast.show();
            // }
        }, function (error) {
            console.log(error);
            //if(error.status==400){
            // var toast = Toast.makeText("");
            // toast.show();
            // }else{
            //   var toast = Toast.makeText("");
            //  toast.show();
            //}
        });
    };
    ResetPasswordComponent = __decorate([
        core_1.Component({
            selector: 'ns-reset-password',
            templateUrl: './reset-password.component.html',
            styleUrls: ['./reset-password.component.css'],
            providers: [reset_password_service_1.reset_password_service],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [reset_password_service_1.reset_password_service, router_1.Router])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());
exports.ResetPasswordComponent = ResetPasswordComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHFEQUF1RDtBQUN2RCxzRkFBb0Y7QUFDcEYsMENBQXVDO0FBUXZDO0lBYUMsZ0NBQW9CLFNBQWtDLEVBQWEsTUFBYztRQUE3RCxjQUFTLEdBQVQsU0FBUyxDQUF5QjtRQUFhLFdBQU0sR0FBTixNQUFNLENBQVE7UUFYaEYsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFDM0IsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFHMUIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsZUFBVSxHQUFHLElBQUksQ0FBQztJQUltRSxDQUFDO0lBQ3RGLHlDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsMkNBQVUsR0FBVjtRQUNFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDO1FBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDM0YsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUUsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRTtZQUV0RCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLEVBQUUsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTtnQkFDL0osT0FBTyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLEVBQUUsRUFBRTtnQkFDOUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDeEI7aUJBQ0ksSUFBRyxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLElBQUUsRUFBRSxFQUFDO2dCQUMzRCxJQUFJLENBQUMsVUFBVSxHQUFDLEtBQUssQ0FBQzthQUN2QjtpQkFDRztnQkFDRixJQUFJLENBQUMsU0FBUyxHQUFDLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLFVBQVUsR0FBQyxLQUFLLENBQUM7YUFDdkI7U0FDRjthQUNJLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxFQUFFLEVBQUU7WUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQ2pDO2FBQ0ksSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLEVBQUUsRUFBRTtZQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixDQUFDLENBQUM7U0FDNUM7YUFDSTtZQUNILElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLENBQUMsQ0FBQztTQUNoRDtJQUNILENBQUM7SUFFRCwyQ0FBVSxHQUFWLFVBQVcsSUFBSTtRQUNiLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLO1lBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLO1lBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUFDRCw0Q0FBVyxHQUFYLFVBQVksSUFBSTtRQUNkLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLO1lBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLO1lBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7SUFDRCwyQ0FBVSxHQUFWO1FBQUEsaUJBd0NVO1FBdkNULElBQU0sT0FBTyxHQUFDO1lBQ1osT0FBTyxFQUFDLEVBQUU7WUFDVixRQUFRLEVBQUMsRUFBRTtTQUNWLENBQUM7UUFDSCxJQUFNLE9BQU8sR0FBQztZQUNiLE9BQU8sRUFBQyxXQUFXO1lBQ25CLFFBQVEsRUFBQywwQkFBMEI7U0FDbkMsQ0FBQTtRQUVGLElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNsRCxPQUFPLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4RSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDYixJQUFJLENBQUMsU0FBUzthQUNILFFBQVEsQ0FBRSxZQUFZLENBQUM7YUFDdkIsU0FBUyxDQUFDLFVBQUMsUUFBUTtZQUNuQiw0QkFBNEI7WUFDN0IsOEJBQThCO1lBQzdCLHlDQUF5QztZQUN4Qyw4Q0FBOEM7WUFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QixPQUFPLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDbEMsa0NBQWtDO1lBQ2xDLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1lBQ3ZDLFFBQVE7WUFDTiwyQ0FBMkM7WUFDM0MsaUNBQWlDO1lBQ25DLGlCQUFpQjtZQUNsQixJQUFJO1FBQ0wsQ0FBQyxFQUFDLFVBQUMsS0FBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkIsd0JBQXdCO1lBQ3ZCLGtDQUFrQztZQUNsQyxnQkFBZ0I7WUFFbEIsU0FBUztZQUNULG9DQUFvQztZQUNuQyxpQkFBaUI7WUFDakIsR0FBRztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQXhHRSxzQkFBc0I7UUFQbEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztZQUM3QyxTQUFTLEVBQUMsQ0FBQywrQ0FBc0IsQ0FBQztZQUNsQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzt5Q0FjK0IsK0NBQXNCLEVBQXFCLGVBQU07T0FickUsc0JBQXNCLENBeUdsQztJQUFELDZCQUFDO0NBQUEsQUF6R0QsSUF5R0M7QUF6R1ksd0RBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgcmVzZXRfcGFzc3dvcmRfc2VydmljZX0gZnJvbSAnfi9hcHAvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQtc2VydmljZSc7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLXJlc2V0LXBhc3N3b3JkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3Jlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LmNzcyddLFxuICBwcm92aWRlcnM6W3Jlc2V0X3Bhc3N3b3JkX3NlcnZpY2VdLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBSZXNldFBhc3N3b3JkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBuZXdfcGFzc3dvcmQ6IHN0cmluZyA9IFwiXCI7XG4gcm5ld3Bhc3N3b3JkOiBzdHJpbmcgPSBcIlwiO1xuIHJlZzogYW55IDtcbiBvcHJlZzogYW55IDtcbiB2YWxpZHBhc3MgPSB0cnVlO1xuIHBhc3NmaWVsZCA9IHRydWU7XG4gdmFsaWRycGFzcyA9IHRydWU7XG4gcnBhc3NmaWVsZCA9IHRydWU7XG5cbiBjaGVja3Bhc3NudWxsO1xuIGNoZWNrcnBhc3NudWxsO1xuIGNvbnN0cnVjdG9yKHByaXZhdGUgbXlzZXJ2aWNlIDogcmVzZXRfcGFzc3dvcmRfc2VydmljZSAgICwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikgeyB9XG4gbmdPbkluaXQoKSB7XG4gfVxuXG4gdmFsaWRhdGlvbigpIHtcbiAgIHRoaXMuY2hlY2twYXNzbnVsbCA9IHRoaXMubmV3X3Bhc3N3b3JkLm1hdGNoKHRoaXMucmVnKTtcbiAgIHRoaXMuY2hlY2tycGFzc251bGwgPSB0aGlzLm5ld19wYXNzd29yZC5sZW5ndGggPT0gMTA7XG4gICBjb25zb2xlLmxvZyhcIm5ld3Bhc3N3b3JkIDogXCIgKyB0aGlzLm5ld19wYXNzd29yZCArIFwiUmUtbmV3cGFzc3dvcmQgOlwiICsgdGhpcy5ybmV3cGFzc3dvcmQpO1xuICAgaWYgKHRoaXMubmV3X3Bhc3N3b3JkICE9IFwiXCIgJiYgdGhpcy5ybmV3cGFzc3dvcmQgIT0gXCJcIikge1xuXG4gICAgIGlmICh0aGlzLnJuZXdwYXNzd29yZC5sZW5ndGggPD0gMTAgJiYgdGhpcy5ybmV3cGFzc3dvcmQubWF0Y2godGhpcy5uZXdfcGFzc3dvcmQpICYmIHRoaXMubmV3X3Bhc3N3b3JkLmxlbmd0aCA8PSA5ICYmIHRoaXMubmV3X3Bhc3N3b3JkLm1hdGNoKHRoaXMubmV3X3Bhc3N3b3JkKSkge1xuICAgICAgIGRpYWxvZ3MuYWxlcnQoXCJWYWxpZCBNb2JpbGUgTnVtYmVyIFwiKTtcbiAgICAgfVxuICAgICBlbHNlIGlmICghdGhpcy5jaGVja3Bhc3NudWxsIHx8IHRoaXMubmV3X3Bhc3N3b3JkLmxlbmd0aCAhPSAxMCkge1xuICAgICAgIHRoaXMudmFsaWRwYXNzID0gZmFsc2U7XG4gICAgIH1cbiAgICAgZWxzZSBpZighdGhpcy5jaGVja3JwYXNzbnVsbCB8fCB0aGlzLnJuZXdwYXNzd29yZC5sZW5ndGghPTEwKXtcbiAgICAgICB0aGlzLnZhbGlkcnBhc3M9ZmFsc2U7XG4gICAgIH1cbiAgICAgZWxzZXtcbiAgICAgICB0aGlzLnZhbGlkcGFzcz1mYWxzZTtcbiAgICAgICB0aGlzLnZhbGlkcnBhc3M9ZmFsc2U7XG4gICAgIH1cbiAgIH1cbiAgIGVsc2UgaWYgKHRoaXMubmV3X3Bhc3N3b3JkID09IFwiXCIgJiYgdGhpcy5ybmV3cGFzc3dvcmQgPT0gXCJcIikge1xuICAgICB0aGlzLnBhc3NmaWVsZCA9IGZhbHNlO1xuICAgICB0aGlzLnJwYXNzZmllbGQgPSBmYWxzZTtcbiAgICAgY29uc29sZS5sb2coXCJib3RoIGZpZWxkIGVtcHR5XCIpO1xuICAgfVxuICAgZWxzZSBpZiAodGhpcy5uZXdfcGFzc3dvcmQgPT0gXCJcIikge1xuICAgICB0aGlzLnBhc3NmaWVsZCA9IGZhbHNlO1xuICAgICBjb25zb2xlLmxvZyhcIk5ldyBwYXNzd29yZCBmaWVsZCBpcyBlbXB0eVwiKTtcbiAgIH1cbiAgIGVsc2Uge1xuICAgICB0aGlzLnJwYXNzZmllbGQgPSBmYWxzZTtcbiAgICAgY29uc29sZS5sb2coXCJSZS10eXBlIHBhc3N3b3JkIGZpZWxkIGlzIGVtcHR5XCIpO1xuICAgfVxuIH1cblxuIHJldHVyblBhc3MoYXJncykge1xuICAgaWYgKHRoaXMucGFzc2ZpZWxkID09IGZhbHNlKVxuICAgICB0aGlzLnBhc3NmaWVsZCA9IHRydWU7XG4gICBpZiAodGhpcy52YWxpZHBhc3MgPT0gZmFsc2UpXG4gICAgIHRoaXMudmFsaWRwYXNzID0gdHJ1ZTtcbiB9XG4gcmV0dXJuUnBhc3MoYXJncykge1xuICAgaWYgKHRoaXMucnBhc3NmaWVsZCA9PSBmYWxzZSlcbiAgICAgdGhpcy5ycGFzc2ZpZWxkID0gdHJ1ZTtcbiAgIGlmICh0aGlzLnZhbGlkcnBhc3MgPT0gZmFsc2UpXG4gICAgIHRoaXMudmFsaWRycGFzcyA9IHRydWU7XG4gfVxuIGdldGRldGFpbHMoKXtcbiAgY29uc3Qgb2JqZWN0MT17XG4gICAgb3BfY29kZTpcIlwiLFxuICAgIHBhc3N3b3JkOlwiXCIsXG4gICAgfTtcbiAgIGNvbnN0IG9iamVjdDI9e1xuICAgIG9wX2NvZGU6XCJPMTkwMzI4MjNcIixcbiAgICBwYXNzd29yZDpcInNGUExnRzdNbUQ1dE4wdkZiWFBxTVE9PVwiLFxuICAgfVxuIFxuICBjb25zdCBzZWFyY2hQYXJhbXMgPSBPYmplY3Qua2V5cyhvYmplY3QxKS5tYXAoKGtleSkgPT4ge1xuICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQob2JqZWN0MltrZXldKTtcbiAgfSkuam9pbignJicpO1xuICB0aGlzLm15c2VydmljZVxuICAgICAgICAgICAgLnBvc3REYXRhKCBzZWFyY2hQYXJhbXMpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgIC8vIGlmKHJlc3BvbnNlLnN0YXR1cz09MjAwKXtcbiAgICAgICAgICAgIC8vICB0aGlzLmlzSXRlbVZpc2libGUgPSB0cnVlO1xuICAgICAgICAgICAgIC8vIHRoaXMubWVzc2FnZT1KU09OLnN0cmluZ2lmeShyZXNwb25zZSk7XG4gICAgICAgICAgICAgIC8vdGhpcy5zaWQ9cmVzcG9uc2UuaGVhZGVycy5nZXQoJ1NldC1jb29raWUnKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgZGlhbG9ncy5hbGVydChcInBhc3N3b3JkIGNoYW5nZWRcIik7XG4gICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcImNvb2tpZSBcIit0aGlzLnNpZCk7XG4gICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybFsnL29wLWNvZGUnXVxuICAgICAgICAgICAgICAvL31lbHNle1xuICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJcIitKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xuICAgICAgICAgICAgICAgIC8vdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJcIik7XG4gICAgICAgICAgICAgIC8vICB0b2FzdC5zaG93KCk7XG4gICAgICAgICAgICAgLy8gfVxuICAgICAgICAgICAgfSwoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgICAgICAvL2lmKGVycm9yLnN0YXR1cz09NDAwKXtcbiAgICAgICAgICAgICAgIC8vIHZhciB0b2FzdCA9IFRvYXN0Lm1ha2VUZXh0KFwiXCIpO1xuICAgICAgICAgICAgICAgLy8gdG9hc3Quc2hvdygpO1xuIFxuICAgICAgICAgICAgIC8vIH1lbHNle1xuICAgICAgICAgICAgIC8vICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJcIik7XG4gICAgICAgICAgICAgIC8vICB0b2FzdC5zaG93KCk7XG4gICAgICAgICAgICAgIC8vfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxufVxuIl19