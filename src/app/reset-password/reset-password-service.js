"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var reset_password_service = /** @class */ (function () {
    function reset_password_service(http) {
        this.http = http;
        this.serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/updateNewP";
    }
    reset_password_service.prototype.postData = function (searchParams) {
        var options = this.createRequestOptions();
        return this.http.post(this.serverUrl, searchParams, { headers: options, observe: 'response' });
    };
    reset_password_service.prototype.createRequestOptions = function () {
        var headers = new http_1.HttpHeaders({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA=="
        });
        return headers;
    };
    reset_password_service = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], reset_password_service);
    return reset_password_service;
}());
exports.reset_password_service = reset_password_service;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzZXQtcGFzc3dvcmQtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJlc2V0LXBhc3N3b3JkLXNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBTS9EO0lBS0csZ0NBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFKNUIsY0FBUyxHQUFHLG9EQUFvRCxDQUFDO0lBSWpDLENBQUM7SUFFekMseUNBQVEsR0FBUixVQUFTLFlBQVk7UUFFckIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDdEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFHLFlBQVksRUFBRyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQUM7SUFFcEcsQ0FBQztJQUVPLHFEQUFvQixHQUE1QjtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksa0JBQVcsQ0FBQztZQUMxQixjQUFjLEVBQUUsbUNBQW1DO1lBQ25ELGVBQWUsRUFBQyxxRUFBcUU7U0FDeEYsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQXBCUyxzQkFBc0I7UUFEbEMsaUJBQVUsRUFBRTt5Q0FNZ0IsaUJBQVU7T0FMMUIsc0JBQXNCLENBcUJsQztJQUFELDZCQUFDO0NBQUEsQUFyQkQsSUFxQkM7QUFyQlksd0RBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IENvb2tpZVhTUkZTdHJhdGVneSB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUvb2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQgeyBwYXNzd29yZG1vZGVsfSBmcm9tIFwifi9hcHAvcGFzc3dvcmQvcGFzc3dvcmQtbW9kZWxcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIHJlc2V0X3Bhc3N3b3JkX3NlcnZpY2Uge1xyXG4gICBwcml2YXRlIHNlcnZlclVybCA9IFwiaHR0cHM6Ly9wc2dpbXNyLmFjLmluL2FsdGhvcy9paHNyZXN0L3YxL3VwZGF0ZU5ld1BcIjtcclxuICAgcHJpdmF0ZSBSZXNwb25zZURhdGE6IGFueTtcclxuXHJcblxyXG4gICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxyXG5cclxuICAgcG9zdERhdGEoc2VhcmNoUGFyYW1zKXtcclxuXHJcbiAgIGxldCBvcHRpb25zID0gdGhpcy5jcmVhdGVSZXF1ZXN0T3B0aW9ucygpO1xyXG4gICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHRoaXMuc2VydmVyVXJsLCAgc2VhcmNoUGFyYW1zICwgeyBoZWFkZXJzOiBvcHRpb25zLCBvYnNlcnZlOiAncmVzcG9uc2UnfSk7XHJcblxyXG4gICB9XHJcblxyXG4gICBwcml2YXRlIGNyZWF0ZVJlcXVlc3RPcHRpb25zKCkge1xyXG4gICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsXHJcbiAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6XCJCZWFyZXIgVTFObmNGZGtkelZUVTJkd1YyUjNObDlKUkRwVFUyZHdWMlIzTlZOVFozQlhaSGMyWDFORlMxSkZWQT09XCJcclxuICAgICAgIH0pO1xyXG4gICAgICAgcmV0dXJuIGhlYWRlcnM7XHJcbiAgIH1cclxufSJdfQ==