import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieXSRFStrategy } from "@angular/http";
import { Observable } from "tns-core-modules/data/observable/observable";
import { passwordmodel} from "~/app/password/password-model";

@Injectable()
export class reset_password_service {
   private serverUrl = "https://psgimsr.ac.in/althos/ihsrest/v1/updateNewP";
   private ResponseData: any;


   constructor(private http: HttpClient) { }

   postData(searchParams){

   let options = this.createRequestOptions();
       return this.http.post(this.serverUrl,  searchParams , { headers: options, observe: 'response'});

   }

   private createRequestOptions() {
       let headers = new HttpHeaders({
           "Content-Type": "application/x-www-form-urlencoded",
           "Authorization":"Bearer U1NncFdkdzVTU2dwV2R3Nl9JRDpTU2dwV2R3NVNTZ3BXZHc2X1NFS1JFVA=="
       });
       return headers;
   }
}