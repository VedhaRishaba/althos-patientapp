import { Component, OnInit } from '@angular/core';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { reset_password_service} from '~/app/reset-password/reset-password-service';
import {Router} from '@angular/router';
@Component({
  selector: 'ns-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers:[reset_password_service],
  moduleId: module.id,
})
export class ResetPasswordComponent implements OnInit {

  new_password: string = "";
 rnewpassword: string = "";
 reg: any ;
 opreg: any ;
 validpass = true;
 passfield = true;
 validrpass = true;
 rpassfield = true;

 checkpassnull;
 checkrpassnull;
 constructor(private myservice : reset_password_service   , private router: Router) { }
 ngOnInit() {
 }

 validation() {
   this.checkpassnull = this.new_password.match(this.reg);
   this.checkrpassnull = this.new_password.length == 10;
   console.log("newpassword : " + this.new_password + "Re-newpassword :" + this.rnewpassword);
   if (this.new_password != "" && this.rnewpassword != "") {

     if (this.rnewpassword.length <= 10 && this.rnewpassword.match(this.new_password) && this.new_password.length <= 9 && this.new_password.match(this.new_password)) {
       dialogs.alert("Valid Mobile Number ");
     }
     else if (!this.checkpassnull || this.new_password.length != 10) {
       this.validpass = false;
     }
     else if(!this.checkrpassnull || this.rnewpassword.length!=10){
       this.validrpass=false;
     }
     else{
       this.validpass=false;
       this.validrpass=false;
     }
   }
   else if (this.new_password == "" && this.rnewpassword == "") {
     this.passfield = false;
     this.rpassfield = false;
     console.log("both field empty");
   }
   else if (this.new_password == "") {
     this.passfield = false;
     console.log("New password field is empty");
   }
   else {
     this.rpassfield = false;
     console.log("Re-type password field is empty");
   }
 }

 returnPass(args) {
   if (this.passfield == false)
     this.passfield = true;
   if (this.validpass == false)
     this.validpass = true;
 }
 returnRpass(args) {
   if (this.rpassfield == false)
     this.rpassfield = true;
   if (this.validrpass == false)
     this.validrpass = true;
 }
 getdetails(){
  const object1={
    op_code:"",
    password:"",
    };
   const object2={
    op_code:"O19032823",
    password:"sFPLgG7MmD5tN0vFbXPqMQ==",
   }
 
  const searchParams = Object.keys(object1).map((key) => {
  return encodeURIComponent(key) + '=' + encodeURIComponent(object2[key]);
  }).join('&');
  this.myservice
            .postData( searchParams)
            .subscribe((response) => {
             // if(response.status==200){
            //  this.isItemVisible = true;
             // this.message=JSON.stringify(response);
              //this.sid=response.headers.get('Set-cookie');
                console.log(response);
                dialogs.alert("password changed");
                //console.log("cookie "+this.sid);
                this.router.navigateByUrl['/op-code']
              //}else{
                //console.log(""+JSON.stringify(response));
                //var toast = Toast.makeText("");
              //  toast.show();
             // }
            },(error) => {
              console.log(error);
              //if(error.status==400){
               // var toast = Toast.makeText("");
               // toast.show();
 
             // }else{
             //   var toast = Toast.makeText("");
              //  toast.show();
              //}
            });
          }
}
