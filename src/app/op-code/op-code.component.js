"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var router_1 = require("@angular/router");
var getservice_1 = require("../getservice/getservice");
var Toast = require("nativescript-toast");
var Connectivity = require("tns-core-modules/connectivity");
var appSettings = require("tns-core-modules/application-settings");
var OpCodeComponent = /** @class */ (function () {
    function OpCodeComponent(page, router, myService) {
        this.page = page;
        this.router = router;
        this.myService = myService;
        this.op_code = "";
        this.valid = true;
        this.flag = true;
        this.loading = true;
        this.opreg = /^[O]{1}[0-9]{8}/;
    }
    OpCodeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    OpCodeComponent.prototype.validate = function () {
        if (this.op_code.localeCompare("") != 0) {
            if (this.op_code.toString().length == 9 &&
                this.op_code.match(this.opreg)) {
                console.log("Valid OPCode Number");
                this.loading = false;
                this.extractData();
            }
            else if (this.op_code.toString().length != 9) {
                this.flag = false;
            }
            else {
                this.valid = false;
            }
        }
        else {
            var toast = Toast.makeText("OP Code cannot be blank");
            toast.show();
        }
    };
    OpCodeComponent.prototype.returnPress = function (args) {
        if (this.valid == false) {
            this.valid = true;
        }
        else if (this.flag == false) {
            this.flag = true;
        }
    };
    OpCodeComponent.prototype.extractData = function () {
        var _this = this;
        // console.log(this.op_code);
        this.myService.getData(this.op_code).subscribe(function (response) {
            _this.loading = true;
            if (response.body == null) {
                var toast = Toast.makeText("Please enter a correct OP Code");
                toast.show();
            }
            else {
                _this.loading = true;
                _this.onGetDataSuccess();
            }
        }, function (error) {
            console.log(error);
            _this.connectionType = _this.connectionToString(Connectivity.getConnectionType());
            if (_this.connectionType == "0") {
                _this.loading = true;
                var toast = Toast.makeText("Cannot connect to network. Try again later!");
                toast.show();
            }
            else if (error.status == 200) {
                _this.loading = true;
                var toast = Toast.makeText("OTP has been sent to Mobile");
                toast.show();
                _this.onNewRegistration();
            }
            _this.loading = true;
        });
    };
    OpCodeComponent.prototype.onGetDataSuccess = function () {
        appSettings.setString("op-code", this.op_code);
        this.router.navigateByUrl("/password");
    };
    OpCodeComponent.prototype.onNewRegistration = function () {
        this.router.navigateByUrl("/otp");
    };
    OpCodeComponent.prototype.connectionToString = function (connectionType) {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "0";
            case Connectivity.connectionType.wifi:
                return "1";
            case Connectivity.connectionType.mobile:
                return "1";
            default:
                return "0";
        }
    };
    OpCodeComponent = __decorate([
        core_1.Component({
            selector: "ns-op-code",
            templateUrl: "./op-code.component.html",
            styleUrls: ["./op-code.component.css"],
            providers: [getservice_1.GetService],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [page_1.Page,
            router_1.Router,
            getservice_1.GetService])
    ], OpCodeComponent);
    return OpCodeComponent;
}());
exports.OpCodeComponent = OpCodeComponent;
/*
*  Remaining Work
*  ~~~~~~~~~~~~~~
*
*      Get Mobile Number When New User come in
*/ 
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3AtY29kZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcC1jb2RlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxzREFBcUQ7QUFDckQsMENBQXlDO0FBQ3pDLHVEQUFzRDtBQUN0RCwwQ0FBNEM7QUFDNUMsNERBQThEO0FBQzlELG1FQUFxRTtBQVNyRTtJQU9JLHlCQUNZLElBQVUsRUFDVixNQUFjLEVBQ2QsU0FBcUI7UUFGckIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxjQUFTLEdBQVQsU0FBUyxDQUFZO1FBVGpDLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFDckIsVUFBSyxHQUFHLElBQUksQ0FBQztRQUNiLFNBQUksR0FBRyxJQUFJLENBQUM7UUFHWixZQUFPLEdBQUUsSUFBSSxDQUFDO1FBV2QsVUFBSyxHQUFRLGlCQUFpQixDQUFDO0lBTjVCLENBQUM7SUFFSixrQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ3JDLENBQUM7SUFJRCxrQ0FBUSxHQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDckMsSUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQ2hDO2dCQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRSxLQUFLLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUN0QjtpQkFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDNUMsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7YUFDckI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7YUFDdEI7U0FDSjthQUFNO1lBQ0gsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3RELEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNoQjtJQUNMLENBQUM7SUFFRCxxQ0FBVyxHQUFYLFVBQVksSUFBSTtRQUNaLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLEVBQUU7WUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7U0FDckI7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSyxFQUFFO1lBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1NBRXBCO0lBQ0wsQ0FBQztJQUNELHFDQUFXLEdBQVg7UUFBQSxpQkFtQ0M7UUFsQ0csNkJBQTZCO1FBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQzFDLFVBQUEsUUFBUTtZQUNKLEtBQUksQ0FBQyxPQUFPLEdBQUUsSUFBSSxDQUFDO1lBQ25CLElBQUksUUFBUSxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7Z0JBQ3ZCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQ3RCLGdDQUFnQyxDQUNuQyxDQUFDO2dCQUNGLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNoQjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsT0FBTyxHQUFFLElBQUksQ0FBQztnQkFDbkIsS0FBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7YUFDM0I7UUFDTCxDQUFDLEVBQ0QsVUFBQSxLQUFLO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQixLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FDekMsWUFBWSxDQUFDLGlCQUFpQixFQUFFLENBQ25DLENBQUM7WUFDRixJQUFJLEtBQUksQ0FBQyxjQUFjLElBQUksR0FBRyxFQUFFO2dCQUM1QixLQUFJLENBQUMsT0FBTyxHQUFFLElBQUksQ0FBQztnQkFDbkIsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FDdEIsNkNBQTZDLENBQ2hELENBQUM7Z0JBQ0YsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ2hCO2lCQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7Z0JBQzVCLEtBQUksQ0FBQyxPQUFPLEdBQUUsSUFBSSxDQUFDO2dCQUNuQixJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLDZCQUE2QixDQUFDLENBQUM7Z0JBQzFELEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDYixLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzthQUM1QjtZQUNELEtBQUksQ0FBQyxPQUFPLEdBQUUsSUFBSSxDQUFDO1FBQ3ZCLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUdPLDBDQUFnQixHQUF4QjtRQUNJLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUUvQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU8sMkNBQWlCLEdBQXpCO1FBRUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUNNLDRDQUFrQixHQUF6QixVQUEwQixjQUFzQjtRQUM1QyxRQUFRLGNBQWMsRUFBRTtZQUNwQixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDakMsT0FBTyxHQUFHLENBQUM7WUFDZixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtnQkFDakMsT0FBTyxHQUFHLENBQUM7WUFDZixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsTUFBTTtnQkFDbkMsT0FBTyxHQUFHLENBQUM7WUFDZjtnQkFDSSxPQUFPLEdBQUcsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUExR1EsZUFBZTtRQVAzQixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFlBQVk7WUFDdEIsV0FBVyxFQUFFLDBCQUEwQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztZQUN0QyxTQUFTLEVBQUUsQ0FBQyx1QkFBVSxDQUFDO1lBQ3ZCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUN0QixDQUFDO3lDQVNvQixXQUFJO1lBQ0YsZUFBTTtZQUNILHVCQUFVO09BVnhCLGVBQWUsQ0EyRzNCO0lBQUQsc0JBQUM7Q0FBQSxBQTNHRCxJQTJHQztBQTNHWSwwQ0FBZTtBQStHNUI7Ozs7O0VBS0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZS9wYWdlXCI7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBHZXRTZXJ2aWNlIH0gZnJvbSBcIi4uL2dldHNlcnZpY2UvZ2V0c2VydmljZVwiO1xuaW1wb3J0ICogYXMgVG9hc3QgZnJvbSBcIm5hdGl2ZXNjcmlwdC10b2FzdFwiO1xuaW1wb3J0ICogYXMgQ29ubmVjdGl2aXR5IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2Nvbm5lY3Rpdml0eVwiO1xuaW1wb3J0ICogYXMgYXBwU2V0dGluZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtb3AtY29kZVwiLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vb3AtY29kZS5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogW1wiLi9vcC1jb2RlLmNvbXBvbmVudC5jc3NcIl0sXG4gICAgcHJvdmlkZXJzOiBbR2V0U2VydmljZV0sXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZFxufSlcbmV4cG9ydCBjbGFzcyBPcENvZGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIG9wX2NvZGU6IHN0cmluZyA9IFwiXCI7XG4gICAgdmFsaWQgPSB0cnVlO1xuICAgIGZsYWcgPSB0cnVlO1xuICAgIHI7XG4gICAgcHVibGljIGNvbm5lY3Rpb25UeXBlOiBzdHJpbmc7XG4gICAgbG9hZGluZyA9dHJ1ZTtcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBwYWdlOiBQYWdlLFxuICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgICAgICBwcml2YXRlIG15U2VydmljZTogR2V0U2VydmljZVxuICAgICkge31cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBvcHJlZzogYW55ID0gL15bT117MX1bMC05XXs4fS87XG5cbiAgICB2YWxpZGF0ZSgpIHtcbiAgICAgICAgaWYgKHRoaXMub3BfY29kZS5sb2NhbGVDb21wYXJlKFwiXCIpICE9IDApIHtcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICB0aGlzLm9wX2NvZGUudG9TdHJpbmcoKS5sZW5ndGggPT0gOSAmJlxuICAgICAgICAgICAgICAgIHRoaXMub3BfY29kZS5tYXRjaCh0aGlzLm9wcmVnKVxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJWYWxpZCBPUENvZGUgTnVtYmVyXCIpO1xuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9ZmFsc2U7XG4gICAgICAgICAgICAgICAgdGhpcy5leHRyYWN0RGF0YSgpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLm9wX2NvZGUudG9TdHJpbmcoKS5sZW5ndGggIT0gOSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZmxhZyA9IGZhbHNlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLnZhbGlkID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YXIgdG9hc3QgPSBUb2FzdC5tYWtlVGV4dChcIk9QIENvZGUgY2Fubm90IGJlIGJsYW5rXCIpO1xuICAgICAgICAgICAgdG9hc3Quc2hvdygpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuUHJlc3MoYXJncykge1xuICAgICAgICBpZiAodGhpcy52YWxpZCA9PSBmYWxzZSkge1xuICAgICAgICAgICAgdGhpcy52YWxpZCA9IHRydWU7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5mbGFnID09IGZhbHNlKSB7XG4gICAgICAgICAgICB0aGlzLmZsYWcgPSB0cnVlO1xuXG4gICAgICAgIH1cbiAgICB9XG4gICAgZXh0cmFjdERhdGEoKSB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMub3BfY29kZSk7XG4gICAgICAgIHRoaXMubXlTZXJ2aWNlLmdldERhdGEodGhpcy5vcF9jb2RlKS5zdWJzY3JpYmUoXG4gICAgICAgICAgICByZXNwb25zZSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID10cnVlO1xuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5ID09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXG4gICAgICAgICAgICAgICAgICAgICAgICBcIlBsZWFzZSBlbnRlciBhIGNvcnJlY3QgT1AgQ29kZVwiXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0LnNob3coKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPXRydWU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25HZXREYXRhU3VjY2VzcygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICAgICAgICAgIHRoaXMuY29ubmVjdGlvblR5cGUgPSB0aGlzLmNvbm5lY3Rpb25Ub1N0cmluZyhcbiAgICAgICAgICAgICAgICAgICAgQ29ubmVjdGl2aXR5LmdldENvbm5lY3Rpb25UeXBlKClcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmNvbm5lY3Rpb25UeXBlID09IFwiMFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9dHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXG4gICAgICAgICAgICAgICAgICAgICAgICBcIkNhbm5vdCBjb25uZWN0IHRvIG5ldHdvcmsuIFRyeSBhZ2FpbiBsYXRlciFcIlxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB0b2FzdC5zaG93KCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChlcnJvci5zdGF0dXMgPT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9dHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRvYXN0ID0gVG9hc3QubWFrZVRleHQoXCJPVFAgaGFzIGJlZW4gc2VudCB0byBNb2JpbGVcIik7XG4gICAgICAgICAgICAgICAgICAgIHRvYXN0LnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbk5ld1JlZ2lzdHJhdGlvbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPXRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfVxuXG4gXG4gICAgcHJpdmF0ZSBvbkdldERhdGFTdWNjZXNzKCkge1xuICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJvcC1jb2RlXCIsIHRoaXMub3BfY29kZSk7XG5cbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChcIi9wYXNzd29yZFwiKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uTmV3UmVnaXN0cmF0aW9uKCkge1xuXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoXCIvb3RwXCIpO1xuICAgIH1cbiAgICBwdWJsaWMgY29ubmVjdGlvblRvU3RyaW5nKGNvbm5lY3Rpb25UeXBlOiBudW1iZXIpOiBzdHJpbmcge1xuICAgICAgICBzd2l0Y2ggKGNvbm5lY3Rpb25UeXBlKSB7XG4gICAgICAgICAgICBjYXNlIENvbm5lY3Rpdml0eS5jb25uZWN0aW9uVHlwZS5ub25lOlxuICAgICAgICAgICAgICAgIHJldHVybiBcIjBcIjtcbiAgICAgICAgICAgIGNhc2UgQ29ubmVjdGl2aXR5LmNvbm5lY3Rpb25UeXBlLndpZmk6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiMVwiO1xuICAgICAgICAgICAgY2FzZSBDb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUubW9iaWxlOlxuICAgICAgICAgICAgICAgIHJldHVybiBcIjFcIjtcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiMFwiO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5cblxuLypcbiogIFJlbWFpbmluZyBXb3JrIFxuKiAgfn5+fn5+fn5+fn5+fn5cbiogICAgICAgXG4qICAgICAgR2V0IE1vYmlsZSBOdW1iZXIgV2hlbiBOZXcgVXNlciBjb21lIGluXG4qLyJdfQ==