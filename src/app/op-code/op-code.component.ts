import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { Router } from "@angular/router";
import { GetService } from "../getservice/getservice";
import * as Toast from "nativescript-toast";
import * as Connectivity from "tns-core-modules/connectivity";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "ns-op-code",
    templateUrl: "./op-code.component.html",
    styleUrls: ["./op-code.component.css"],
    providers: [GetService],
    moduleId: module.id
})
export class OpCodeComponent implements OnInit {
    op_code: string = "";
    valid = true;
    flag = true;
    r;
    public connectionType: string;
    loading =true;
    constructor(
        private page: Page,
        private router: Router,
        private myService: GetService
    ) {}

    ngOnInit() {
        this.page.actionBarHidden = true;
    }

    opreg: any = /^[O]{1}[0-9]{8}/;

    validate() {
        if (this.op_code.localeCompare("") != 0) {
            if (
                this.op_code.toString().length == 9 &&
                this.op_code.match(this.opreg)
            ) {
                console.log("Valid OPCode Number");
                this.loading =false;
                this.extractData();
            } else if (this.op_code.toString().length != 9) {
                this.flag = false;
            } else {
                this.valid = false;
            }
        } else {
            var toast = Toast.makeText("OP Code cannot be blank");
            toast.show();
        }
    }

    returnPress(args) {
        if (this.valid == false) {
            this.valid = true;
        } else if (this.flag == false) {
            this.flag = true;

        }
    }
    extractData() {
        // console.log(this.op_code);
        this.myService.getData(this.op_code).subscribe(
            response => {
                this.loading =true;
                if (response.body == null) {
                    var toast = Toast.makeText(
                        "Please enter a correct OP Code"
                    );
                    toast.show();
                } else {
                    this.loading =true;
                    this.onGetDataSuccess();
                }
            },
            error => {
                console.log(error);
                this.connectionType = this.connectionToString(
                    Connectivity.getConnectionType()
                );
                if (this.connectionType == "0") {
                    this.loading =true;
                    var toast = Toast.makeText(
                        "Cannot connect to network. Try again later!"
                    );
                    toast.show();
                } else if (error.status == 200) {
                    this.loading =true;
                    var toast = Toast.makeText("OTP has been sent to Mobile");
                    toast.show();
                    this.onNewRegistration();
                }
                this.loading =true;
            }
        );
    }

 
    private onGetDataSuccess() {
        appSettings.setString("op-code", this.op_code);

        this.router.navigateByUrl("/password");
    }

    private onNewRegistration() {

        this.router.navigateByUrl("/otp");
    }
    public connectionToString(connectionType: number): string {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "0";
            case Connectivity.connectionType.wifi:
                return "1";
            case Connectivity.connectionType.mobile:
                return "1";
            default:
                return "0";
        }
    }
}



/*
*  Remaining Work 
*  ~~~~~~~~~~~~~~
*       
*      Get Mobile Number When New User come in
*/