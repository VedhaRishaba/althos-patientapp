"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_module_1 = require("./app-routing.module");
var http_1 = require("@angular/common/http");
var http_client_1 = require("nativescript-angular/http-client");
var forms_1 = require("nativescript-angular/forms");
var app_component_1 = require("./app.component");
var op_code_component_1 = require("./op-code/op-code.component");
var forgot_op_component_1 = require("./forgot-op/forgot-op.component");
var password_component_1 = require("./password/password.component");
var forgot_password_component_1 = require("./forgot-password/forgot-password.component");
var otp_component_1 = require("./otp/otp.component");
var reset_password_component_1 = require("./reset-password/reset-password.component");
var faq_component_1 = require("./faq/faq.component");
var policies_component_1 = require("./policies/policies.component");
var about_component_1 = require("./about/about.component");
var contact_component_1 = require("./contact/contact.component");
var profile_component_1 = require("./profile/profile.component");
var checkup_plan_component_1 = require("./checkup-plan/checkup-plan.component");
var hp_step1_component_1 = require("./hp-step1/hp-step1.component");
var hp_step2_component_1 = require("./hp-step2/hp-step2.component");
var hp_step3_component_1 = require("./hp-step3/hp-step3.component");
var hp_step4_component_1 = require("./hp-step4/hp-step4.component");
var hp_step5_component_1 = require("./hp-step5/hp-step5.component");
var records_component_1 = require("./records/records.component");
var lab_component_1 = require("./lab/lab.component");
var appointment_component_1 = require("./appointment/appointment.component");
var ap_step1_component_1 = require("./ap-step1/ap-step1.component");
var ap_step2_component_1 = require("./ap-step2/ap-step2.component");
var ap_step3_component_1 = require("./ap-step3/ap-step3.component");
var ap_step4_component_1 = require("./ap-step4/ap-step4.component");
var ap_step5_component_1 = require("./ap-step5/ap-step5.component");
var lab_step1_component_1 = require("./lab-step1/lab-step1.component");
var lab_step2_component_1 = require("./lab-step2/lab-step2.component");
var lab_step3_component_1 = require("./lab-step3/lab-step3.component");
var lab_step4_component_1 = require("./lab-step4/lab-step4.component");
var lab_step5_component_1 = require("./lab-step5/lab-step5.component");
var terms_component_1 = require("./terms/terms.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                http_1.HttpClientModule,
                http_client_1.NativeScriptHttpClientModule
            ],
            declarations: [
                app_component_1.AppComponent,
                op_code_component_1.OpCodeComponent,
                forgot_op_component_1.ForgotOPComponent,
                password_component_1.PasswordComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                otp_component_1.OtpComponent,
                reset_password_component_1.ResetPasswordComponent,
                faq_component_1.FaqComponent,
                policies_component_1.PoliciesComponent,
                about_component_1.AboutComponent,
                contact_component_1.ContactComponent,
                profile_component_1.ProfileComponent,
                checkup_plan_component_1.CheckupPlanComponent,
                hp_step1_component_1.HpStep1Component,
                hp_step2_component_1.HpStep2Component,
                hp_step3_component_1.HpStep3Component,
                hp_step4_component_1.HpStep4Component,
                hp_step5_component_1.HpStep5Component,
                records_component_1.RecordsComponent,
                lab_component_1.LabComponent,
                appointment_component_1.AppointmentComponent,
                ap_step1_component_1.ApStep1Component,
                ap_step2_component_1.ApStep2Component,
                ap_step3_component_1.ApStep3Component,
                ap_step4_component_1.ApStep4Component,
                ap_step5_component_1.ApStep5Component,
                lab_step1_component_1.LabStep1Component,
                lab_step2_component_1.LabStep2Component,
                lab_step3_component_1.LabStep3Component,
                lab_step4_component_1.LabStep4Component,
                lab_step5_component_1.LabStep5Component,
                terms_component_1.TermsComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBRTlFLDJEQUF3RDtBQUN4RCw2Q0FBbUU7QUFDbkUsZ0VBQWdGO0FBQ2hGLG9EQUFvRTtBQUNwRSxpREFBK0M7QUFHL0MsaUVBQThEO0FBQzlELHVFQUFvRTtBQUNwRSxvRUFBa0U7QUFDbEUseUZBQXNGO0FBQ3RGLHFEQUFtRDtBQUNuRCxzRkFBbUY7QUFDbkYscURBQW1EO0FBQ25ELG9FQUFrRTtBQUNsRSwyREFBeUQ7QUFDekQsaUVBQStEO0FBQy9ELGlFQUErRDtBQUMvRCxnRkFBNkU7QUFDN0Usb0VBQWlFO0FBQ2pFLG9FQUFpRTtBQUNqRSxvRUFBaUU7QUFDakUsb0VBQWlFO0FBQ2pFLG9FQUFpRTtBQUNqRSxpRUFBK0Q7QUFDL0QscURBQW1EO0FBQ25ELDZFQUEyRTtBQUMzRSxvRUFBaUU7QUFDakUsb0VBQWlFO0FBQ2pFLG9FQUFpRTtBQUNqRSxvRUFBaUU7QUFDakUsb0VBQWlFO0FBQ2pFLHVFQUFvRTtBQUNwRSx1RUFBb0U7QUFDcEUsdUVBQW9FO0FBQ3BFLHVFQUFvRTtBQUNwRSx1RUFBb0U7QUFDcEUsMkRBQXlEO0FBbUR6RDtJQUFBO0lBQXlCLENBQUM7SUFBYixTQUFTO1FBakRyQixlQUFRLENBQUM7WUFDTixTQUFTLEVBQUU7Z0JBQ1AsNEJBQVk7YUFDZjtZQUNELE9BQU8sRUFBRTtnQkFDTCx3Q0FBa0I7Z0JBQ2xCLHFDQUFnQjtnQkFDaEIsK0JBQXVCO2dCQUN2Qix1QkFBZ0I7Z0JBQ2hCLDBDQUE0QjthQUMvQjtZQUNELFlBQVksRUFBRTtnQkFDViw0QkFBWTtnQkFDWixtQ0FBZTtnQkFDZix1Q0FBaUI7Z0JBQ2pCLHNDQUFpQjtnQkFDakIsbURBQXVCO2dCQUN2Qiw0QkFBWTtnQkFDWixpREFBc0I7Z0JBQ3RCLDRCQUFZO2dCQUNaLHNDQUFpQjtnQkFDakIsZ0NBQWM7Z0JBQ2Qsb0NBQWdCO2dCQUNoQixvQ0FBZ0I7Z0JBQ2hCLDZDQUFvQjtnQkFDcEIscUNBQWdCO2dCQUNoQixxQ0FBZ0I7Z0JBQ2hCLHFDQUFnQjtnQkFDaEIscUNBQWdCO2dCQUNoQixxQ0FBZ0I7Z0JBQ2hCLG9DQUFnQjtnQkFDaEIsNEJBQVk7Z0JBQ1osNENBQW9CO2dCQUNwQixxQ0FBZ0I7Z0JBQ2hCLHFDQUFnQjtnQkFDaEIscUNBQWdCO2dCQUNoQixxQ0FBZ0I7Z0JBQ2hCLHFDQUFnQjtnQkFDaEIsdUNBQWlCO2dCQUNqQix1Q0FBaUI7Z0JBQ2pCLHVDQUFpQjtnQkFDakIsdUNBQWlCO2dCQUNqQix1Q0FBaUI7Z0JBQ2pCLGdDQUFjO2FBQ2pCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxTQUFTLENBQUk7SUFBRCxnQkFBQztDQUFBLEFBQTFCLElBQTBCO0FBQWIsOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBBcHBSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vYXBwLXJvdXRpbmcubW9kdWxlXCI7XHJcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgLEh0dHBDbGllbnR9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2h0dHAtY2xpZW50XCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHsgQXBwQ29tcG9uZW50IH0gZnJvbSBcIi4vYXBwLmNvbXBvbmVudFwiO1xyXG5cclxuXHJcbmltcG9ydCB7IE9wQ29kZUNvbXBvbmVudCB9IGZyb20gJy4vb3AtY29kZS9vcC1jb2RlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcmdvdE9QQ29tcG9uZW50IH0gZnJvbSAnLi9mb3Jnb3Qtb3AvZm9yZ290LW9wLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFBhc3N3b3JkQ29tcG9uZW50IH0gZnJvbSAnLi9wYXNzd29yZC9wYXNzd29yZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3Jnb3RQYXNzd29yZENvbXBvbmVudCB9IGZyb20gJy4vZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBPdHBDb21wb25lbnQgfSBmcm9tICcuL290cC9vdHAuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUmVzZXRQYXNzd29yZENvbXBvbmVudCB9IGZyb20gJy4vcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRmFxQ29tcG9uZW50IH0gZnJvbSAnLi9mYXEvZmFxLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFBvbGljaWVzQ29tcG9uZW50IH0gZnJvbSAnLi9wb2xpY2llcy9wb2xpY2llcy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBYm91dENvbXBvbmVudCB9IGZyb20gJy4vYWJvdXQvYWJvdXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29udGFjdENvbXBvbmVudCB9IGZyb20gJy4vY29udGFjdC9jb250YWN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFByb2ZpbGVDb21wb25lbnQgfSBmcm9tICcuL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDaGVja3VwUGxhbkNvbXBvbmVudCB9IGZyb20gJy4vY2hlY2t1cC1wbGFuL2NoZWNrdXAtcGxhbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBIcFN0ZXAxQ29tcG9uZW50IH0gZnJvbSAnLi9ocC1zdGVwMS9ocC1zdGVwMS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBIcFN0ZXAyQ29tcG9uZW50IH0gZnJvbSAnLi9ocC1zdGVwMi9ocC1zdGVwMi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBIcFN0ZXAzQ29tcG9uZW50IH0gZnJvbSAnLi9ocC1zdGVwMy9ocC1zdGVwMy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBIcFN0ZXA0Q29tcG9uZW50IH0gZnJvbSAnLi9ocC1zdGVwNC9ocC1zdGVwNC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBIcFN0ZXA1Q29tcG9uZW50IH0gZnJvbSAnLi9ocC1zdGVwNS9ocC1zdGVwNS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBSZWNvcmRzQ29tcG9uZW50IH0gZnJvbSAnLi9yZWNvcmRzL3JlY29yZHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTGFiQ29tcG9uZW50IH0gZnJvbSAnLi9sYWIvbGFiLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFwcG9pbnRtZW50Q29tcG9uZW50IH0gZnJvbSAnLi9hcHBvaW50bWVudC9hcHBvaW50bWVudC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcFN0ZXAxQ29tcG9uZW50IH0gZnJvbSAnLi9hcC1zdGVwMS9hcC1zdGVwMS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcFN0ZXAyQ29tcG9uZW50IH0gZnJvbSAnLi9hcC1zdGVwMi9hcC1zdGVwMi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcFN0ZXAzQ29tcG9uZW50IH0gZnJvbSAnLi9hcC1zdGVwMy9hcC1zdGVwMy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcFN0ZXA0Q29tcG9uZW50IH0gZnJvbSAnLi9hcC1zdGVwNC9hcC1zdGVwNC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcFN0ZXA1Q29tcG9uZW50IH0gZnJvbSAnLi9hcC1zdGVwNS9hcC1zdGVwNS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMYWJTdGVwMUNvbXBvbmVudCB9IGZyb20gJy4vbGFiLXN0ZXAxL2xhYi1zdGVwMS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMYWJTdGVwMkNvbXBvbmVudCB9IGZyb20gJy4vbGFiLXN0ZXAyL2xhYi1zdGVwMi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMYWJTdGVwM0NvbXBvbmVudCB9IGZyb20gJy4vbGFiLXN0ZXAzL2xhYi1zdGVwMy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMYWJTdGVwNENvbXBvbmVudCB9IGZyb20gJy4vbGFiLXN0ZXA0L2xhYi1zdGVwNC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMYWJTdGVwNUNvbXBvbmVudCB9IGZyb20gJy4vbGFiLXN0ZXA1L2xhYi1zdGVwNS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUZXJtc0NvbXBvbmVudCB9IGZyb20gJy4vdGVybXMvdGVybXMuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBib290c3RyYXA6IFtcclxuICAgICAgICBBcHBDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxyXG4gICAgICAgIEFwcFJvdXRpbmdNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXHJcbiAgICAgICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgICAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgQXBwQ29tcG9uZW50LFxyXG4gICAgICAgIE9wQ29kZUNvbXBvbmVudCxcclxuICAgICAgICBGb3Jnb3RPUENvbXBvbmVudCxcclxuICAgICAgICBQYXNzd29yZENvbXBvbmVudCxcclxuICAgICAgICBGb3Jnb3RQYXNzd29yZENvbXBvbmVudCxcclxuICAgICAgICBPdHBDb21wb25lbnQsXHJcbiAgICAgICAgUmVzZXRQYXNzd29yZENvbXBvbmVudCxcclxuICAgICAgICBGYXFDb21wb25lbnQsXHJcbiAgICAgICAgUG9saWNpZXNDb21wb25lbnQsXHJcbiAgICAgICAgQWJvdXRDb21wb25lbnQsXHJcbiAgICAgICAgQ29udGFjdENvbXBvbmVudCxcclxuICAgICAgICBQcm9maWxlQ29tcG9uZW50LFxyXG4gICAgICAgIENoZWNrdXBQbGFuQ29tcG9uZW50LFxyXG4gICAgICAgIEhwU3RlcDFDb21wb25lbnQsXHJcbiAgICAgICAgSHBTdGVwMkNvbXBvbmVudCxcclxuICAgICAgICBIcFN0ZXAzQ29tcG9uZW50LFxyXG4gICAgICAgIEhwU3RlcDRDb21wb25lbnQsXHJcbiAgICAgICAgSHBTdGVwNUNvbXBvbmVudCxcclxuICAgICAgICBSZWNvcmRzQ29tcG9uZW50LFxyXG4gICAgICAgIExhYkNvbXBvbmVudCxcclxuICAgICAgICBBcHBvaW50bWVudENvbXBvbmVudCxcclxuICAgICAgICBBcFN0ZXAxQ29tcG9uZW50LFxyXG4gICAgICAgIEFwU3RlcDJDb21wb25lbnQsXHJcbiAgICAgICAgQXBTdGVwM0NvbXBvbmVudCxcclxuICAgICAgICBBcFN0ZXA0Q29tcG9uZW50LFxyXG4gICAgICAgIEFwU3RlcDVDb21wb25lbnQsXHJcbiAgICAgICAgTGFiU3RlcDFDb21wb25lbnQsXHJcbiAgICAgICAgTGFiU3RlcDJDb21wb25lbnQsXHJcbiAgICAgICAgTGFiU3RlcDNDb21wb25lbnQsXHJcbiAgICAgICAgTGFiU3RlcDRDb21wb25lbnQsXHJcbiAgICAgICAgTGFiU3RlcDVDb21wb25lbnQsXHJcbiAgICAgICAgVGVybXNDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBzY2hlbWFzOiBbXHJcbiAgICAgICAgTk9fRVJST1JTX1NDSEVNQVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxyXG4iXX0=